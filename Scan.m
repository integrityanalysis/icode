//
//  Scan.m
//  SquareCam
//
//  Created by Zeke Chan on 29/10/18.
//
#import <Foundation/Foundation.h>
#import "Scan.h"


@interface Scan() 
@end

@implementation Scan

+ (NSString *)dynamoDBTableName {
  return @"Scans1";
}

+ (NSString *)hashKeyAttribute {
  return @"UserId";
}
//+ (NSNumber *)hashKeyAttribute {
//  return @"TimeStamp";
//}


@end
