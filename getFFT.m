//
//  getFFT.m
//  SquareCam 
//
//  Created by Zeke Chan on 5/12/16.
//
//

#import <Foundation/Foundation.h>

//#include "engine.h"
#include "Constants.h"
#include "ic_engine.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Accelerate/Accelerate.h>

//#define	max(a, b)	((a) < (b) ? (b) : (a))
float _signal1[SQPX], _edge[SQPX], _absF1[SQPX];
void getFilteredFFT(float *Signal, float *absF2){
  if(db_useOldFilter){
    getFilteredFFT_old(Signal, absF2);
    return;
  }
  
  int r,c,i,j,k;
  int displayOn = 0;
  
  // Disk-donut Kernal (instead of Gaussian)
  float B[9]={
    0.0368 ,   0.2132 ,   0.0368,
    0.2132 ,        0 ,   0.2132,
    0.0368 ,   0.2132 ,   0.0368
  };
  vDSP_f3x3(Signal, CF, RF, B, _signal1);
  
  // Edge detection kernel
  float E[9]={
    -1 ,   -1  ,  -1,
    -1 ,    8  ,  -1,
    -1 ,   -1  ,  -1
  };
  vDSP_f3x3(Signal, CF, RF, E, _edge);
  
  if(displayOn){
    printf("Conv Signal\n");
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF; ++c){
        printf("%3.0f ", _signal1[r*CF+c]);
      }
      printf("\n");
    }
  }
  
  // Alternative 3 - Adaptive binary based on Flatness
  float bthres   = 0.1; // minimum pixel threshold
  float seg[3]   = { 0.0 , 10.0  , 100.0   };
  float sgain[3] = { 0.025,  0.15,   0.0125 };
  for(i=0;i<NF;i++){
    float Id  = Signal[i] - _signal1[i]; // difference
    float Id1 = +(Id>bthres)  -(Id<-bthres);   // binarize

    // Adaptive gain based on Edge
    float absEdge = fabs(_edge[i]);
    float gain;
    if(absEdge<seg[1])       gain=sgain[0]; // low edge: high noise content
    else if (absEdge<seg[2]) gain=sgain[1]; // med edge: best signal
    else                     gain=sgain[2]; // hi edge: possibly false

    _signal1[i] = Id1 * gain;
  }
  
  
  // Alternative 2 - exponentially increase near pixel 0 and 255.
  // Plus edge removal.
  // The lower ethres, the more noise removed. 
//  float ethres = 50; // matlab use 100. Here 80 a stronger filter for clearer image
//  for(i=0;i<NF;i++){
//    if(_signal1[i]!=0 & fabs(_edge[i])<ethres){
//      float Id  = Signal[i] - _signal1[i]; // difference
//      float den = 10 + (_signal1[i]<128) *_signal1[i]
//                     + (_signal1[i]>=128)*(255-_signal1[i]);
//      _signal1[i] = Id  / den; // edge removal
//    }else{
//      _signal1[i] = 0;
//    }
//  }
  
  // Alternative 1 - binarize. Good result too!
  // (f-f'), binarize, edge removal+scaling
//  float bthres = 2;
//  float ethres = 80;
//  for(i=0;i<NF;i++){
//    float Id  = Signal[i] - _signal1[i]; // difference
//    float Id1 = +(Id>bthres)  -(Id<-bthres);   // binarize
//    _signal1[i] = 0.1 * Id1 * (fabs(_edge[i])<ethres); // edge removal
//  }
  
  // Original plus edge detector. Better but in text situation, code not strong.
  // f/f' - 1. if f' is zero, don't do the division. Keep it to zero.
//  for(i=0;i<NF;i++){
//    if(_signal1[i]!=0){
//      _signal1[i] = (Signal[i] / _signal1[i] - 1)*(fabs(_edge[i])<ethres);
//    }
//  }
  
  if(displayOn){
    printf("Normalized Convolved Signal\n");
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF; ++c){
        printf("%7.3f ", _signal1[r*CF+c]);
      }
      printf("\n");
    }
  }
  
  // Zero-out area outside circle
  for(i=0;i<LK.nOutCircle;i++) _signal1[ LK.outCircle[i] ] = 0;
  
  // unfortunately, can't see if the circle-zero is working!
//  printimagef(_signal1,256,256,5,5,1,1);

  
  //  float *absF;
  getFFT(_signal1,_absF1);
  int C1 = CF/2+1;

  if(displayOn){
    printf("Abs F (not shifted)\n");
    k = 0;
    for (r = 0; r < RF; ++r){
      for (c = 0; c < C1; ++c){
        printf("%7.3f ", _absF1[k++]);
      }
      printf("\n");
    }
  }
  
  // Perform FFT Shift
  memcpy(absF2,&_absF1[(RF/2)*C1],4*(RF/2)*C1); // float=4x word.
  memcpy(&absF2[(RF/2)*C1],_absF1,4*(RF/2+1)*C1);
  // printf("RF %d C1 %d NF %d\n",RF,C1,NF); //512,257

  if(displayOn){
    printf("\n\nShuffled Abs F\n");
    k = 0;
    for (r = 0; r < RF+1; ++r){
      for (c = 0; c < C1; ++c){
        printf("%7.3f ", absF2[k++]);
      }
      printf("\n");
    }
  }
}

// Original Filter FFT
void getFilteredFFT_old(float *Signal, float *absF2){
  
  int r,c,i,j,k;
  int displayOn = NO;
  
  // Gaussian Kernal
  float B[9]={
    0.0113 ,   0.0838  ,  0.0113,
    0.0838 ,   0.6193  ,  0.0838,
    0.0113 ,   0.0838  ,  0.0113
  };
  vDSP_f3x3(Signal, CF, RF, B, _signal1);
  
  if(displayOn){
    printf("Conv Signal\n");
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF; ++c){
        printf("%3.0f ", _signal1[r*CF+c]);
      }
      printf("\n");
    }
  }
  
  // f/f' - 1. if f' is zero, don't do the division. Keep it to zero.
  for(i=0;i<NF;i++){
    if(_signal1[i]!=0){
      _signal1[i] = Signal[i] / _signal1[i] - 1;
    }
  }
  
  if(displayOn){
    printf("Normalized Convolved Signal\n");
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF; ++c){
        printf("%7.3f ", _signal1[r*CF+c]);
      }
      printf("\n");
    }
  }
  
  //  float *absF;
  getFFT(_signal1,_absF1);
  int C1 = CF/2+1;
  
  if(displayOn){
    printf("Abs F (not shifted)\n");
    k = 0;
    for (r = 0; r < RF; ++r){
      for (c = 0; c < C1; ++c){
        printf("%7.3f ", _absF1[k++]);
      }
      printf("\n");
    }
  }
  
  // Perform FFT Shift
  memcpy(absF2,&_absF1[(RF/2)*C1],4*(RF/2)*C1); // float=4x word.
  memcpy(&absF2[(RF/2)*C1],_absF1,4*(RF/2+1)*C1);
  
  if(displayOn){
    printf("\n\nShuffled Abs F\n");
    k = 0;
    for (r = 0; r < RF+1; ++r){
      for (c = 0; c < C1; ++c){
        printf("%7.3f ", absF2[k++]);
      }
      printf("\n");
    }
  }
}

// ###########################################################################
// ###########################################################################
float _magFFT[NF/2+CF+RF];
void getFFT(float *Signal, float *mag1){
  BOOL displayOn = NO;
  // ===== 1. Input Variables
  /*	Define a stride for the array be passed to the FFT.  In many
   applications, the stride is one and is passed to the vDSP
   routine as a constant.
   */
  const vDSP_Stride Stride = 1;
  
  // Define variables for loop iterators.
  vDSP_Length r, c;
  if(displayOn) printf("\n\tTwo-dimensional real FFT of %lu*%lu elements.\n",
                       (unsigned long) RF, (unsigned long) CF);
  
  // Allocate memory for the arrays.
  float *ObservedMemory = malloc(NF * sizeof *ObservedMemory);
  if (ObservedMemory == NULL || Signal == NULL) {
    fprintf(stderr, "Error, failed to allocate memory.\n");
    exit(EXIT_FAILURE);
  }
  
  // Assign half of ObservedMemory to reals and half to imaginaries.
  DSPSplitComplex Observed = { ObservedMemory, ObservedMemory + NF/2 };
  
  // ===== 2. Perform get Absolute FFT
  if(displayOn) printf("Begin %s.\n", __func__);
  
  /*	Initialize data for the FFT routines.  Note that the longest
   length we will use, in either dimension, is passed to the
   setup.
   */
  FFTSetup Setup = vDSP_create_fftsetup(MAX(Log2R, Log2C), FFT_RADIX2);
  if (Setup == NULL) {
    fprintf(stderr, "Error, vDSP_create_fftsetup failed.\n");
    exit (EXIT_FAILURE);
  }
  
  //  for(int counter=0;counter<1000;counter++){
  //    printf("Counter %d\n",counter);
  
  
  /*	Reinterpret the real signal as an interleaved-data complex
   vector and use vDSP_ctoz to move the data to a separated-data
   complex vector.
   
   Note that we pass vDSP_ctoz two times Signal's normal stride,
   because ctoz skips through a complex vector from real to real,
   skipping imaginary elements.  Considering this as a stride of
   two real-sized elements rather than one complex element is a
   legacy use.
   
   Also, we assume here the rows of Signal are contiguous, that
   there is no additional stride between rows.  When this is the
   case, we can use a single call to vDSP_ctoz to copy the whole
   array.  If it were not the case, we would need to use a
   separate vDSP_ctoz call for each row.
   
   In the destination array, a stride of one is used regardless of
   the source stride.  Since the destination is a buffer allocated
   just for this purpose, there is no point in replicating the
   source stride.
   */
  vDSP_ctoz((DSPComplex *) Signal, 2*Stride, &Observed, 1, NF/2);
  
  /*	Here and elsewhere we treat Signal and Observed as a
   one-dimensional array, even though they contain a
   two-dimensional signal.  That is fine because they are
   laid out in memory as consecutive elements, so they are
   one-dimensional arrays for purposes of copying data,
   which do not care about the interpretation of the
   data.  When we want to act on the two-dimensional
   signal, we do the necessary subscript arithmetic to
   convert indices in the two-dimensional signal to an
   index in the one-dimensional array.
   
   It is also possible to cast the one-dimensional array
   (or a pointer to it) to a two-dimensional array (or a
   pointer to it).  The resulting behavior is not defined
   by the CF standard.  Many compilers produce code that
   behaves in the obvious way, but it is possible to get
   undesirable behavior, particularly when optimization is
   involved.
   */
  
  // Perform a real-to-complex FFT. (In Place Real)
  vDSP_fft2d_zrip(Setup, &Observed, 1, 0, Log2C, Log2R, FFT_FORWARD);
  
  /*	Observe that zero is passed as the row stride.
   Normally the row stride is number of elements from the
   start of one row to the start of the next.  Zero is a
   special value that means to use the number of columns.
   This is the normal case when an array is not embedded
   in a larger array.  If, for example, you were taking
   the DFT of a 16*16 array embedded inside a 1024*1024
   array, you would pass 1024 as the row stride, because
   the rows of the 16*16 array begin 1024 elements apart
   in memory.
   */
  
  
  // ----- a. Compute magnitude
//  float mag[NF/2+CF+RF]; //,mag1[NF/2 + C1];
  vDSP_zvabs(&Observed, 1, _magFFT, 1, NF/2);
  
  if(displayOn){
    // Display FFT complex output
    printf("\n\n----- FFT Output %dx%d\n",RF,CF/2);
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF/2; ++c){
        printf("%7.2f + i%7.2f ,",Observed.realp[r*CF/2 + c],Observed.imagp[r*CF/2 + c]);
      }
      printf("\n");
    }
    
    printf("----- abs FFT Output %dx%d\n",RF,CF/2);
    for (r = 0; r < RF; ++r){
      for (c = 0; c < CF/2; ++c){
        printf("%7.3f  ",_magFFT[r*CF/2 + c]);
      }
      printf("\n");
    }
    
  }
  
  
  
  // C1 is final column number: half the column number + 1
  // Copy mag into unpacked format mag1
  int  C1 = CF/2 + 1;
  int i,j,k;
  
  // ----- b. Image Packing with extra column for Convolution
  j = 0;
  i = 0;
  for (r = 0; r < RF; r++){
    j = r*C1  ;
    for (c = 0; c < CF/2; c++){
      mag1[j++] = _magFFT[i++];
    }
  }
  
  // ----- c. Unpacking - must see this guide
  // https://developer.apple.com/library/mac/documentation/Performance/Conceptual/vDSP_Programming_Guide/UsingFourierTransforms/UsingFourierTransforms.html
  // Obtain 1st and last Column magnitude
  c  = 0;
  for (r = 1; r < (RF/2); ++r){
    // Column 1.
    int k1 = (2*r  )*CF/2;
    int k2 = (2*r+1)*CF/2;
//    printf("r %d obs %f(%d) %f(%d)\n",r,Observed.realp[k1],k1,Observed.realp[k2],k2);
    mag1[r*C1     + 0] = NORM2(Observed.realp[k1],Observed.realp[k2]) ;
    mag1[(r+1)*C1 - 1] = NORM2(Observed.imagp[k1],Observed.imagp[k2]) ;
  }
  
  // Copy mag into unpacked format mag1
  mag1[0]             = fabs(Observed.realp[0]);
  mag1[C1-1]          = fabs(Observed.imagp[0]);
  mag1[(RF/2)*C1]     = fabs(Observed.realp[CF/2]);
  mag1[(RF/2+1)*C1-1] = fabs(Observed.imagp[CF/2]);
  
  // Copy Symmetric values in the first and last column
  for (r = 1; r < CF/2; ++r){
    mag1[(RF-r)*C1   + 0] = mag1[r*C1     + 0];
    mag1[(RF-r+1)*C1 - 1] = mag1[(r+1)*C1 - 1];
  }
  
  // Display
  if(displayOn){
    printf("\n\n----- Unpacked abs Output (mag1) %dx%d\n",RF,CF/2);
    k=0;
    for (r = 0; r < RF; r++){
      for (c = 0; c < C1; c++){
        printf("%7.3f  ",mag1[k++]);
      }
      printf("\n");
    }
  }
  
  
  //}
  
  
  // ----- d. Done. Clean up.
  free(ObservedMemory); // Very important to free up memory
  vDSP_destroy_fftsetup(Setup);
  
  if(displayOn) printf("\nEnd %s.\n\n\n", __func__);
  
}





void test01(){
  
  int r,c,i,j,k;
  
  
  
  
  // Allocate memory for the arrays.
  const vDSP_Stride Stride = 1;
  float *Signal  = malloc(NF * Stride * sizeof Signal);
  float *absF    = malloc(NF * Stride * sizeof Signal);
  float *absF2   = malloc(NF * Stride * sizeof Signal);
  float *Signal1 = malloc(NF * Stride * sizeof Signal);
  float *Signal2 = malloc(NF * Stride * sizeof Signal);
  //  float *ObservedMemory = malloc(NF * sizeof *ObservedMemory);
  
  /*	Generate an input signal.  In a real application, data would of
   course be provided from an image file, sensors, or other
   source.
   */
  
  for (r = 0; r < RF; ++r)
    for (c = 0; c < CF; ++c)
      Signal[(r*CF + c) * Stride] = (99*(r*r+c*c+c))%256;
  
  printf("Input Signal\n");
  for (r = 0; r < RF; ++r){
    for (c = 0; c < CF; ++c){
      printf("%7.1.0f ", Signal[r*CF+c]);
    }
    printf("\n");
  }
  
  float B[9]={
    0.0113 ,   0.0838  ,  0.0113,
    0.0838 ,   0.6193  ,  0.0838,
    0.0113 ,   0.0838  ,  0.0113
  };
  vDSP_f3x3(Signal, CF, RF, B, Signal1);
  
  printf("Conv Signal\n");
  for (r = 0; r < RF; ++r){
    for (c = 0; c < CF; ++c){
      printf("%3.0f ", Signal1[r*CF+c]);
    }
    printf("\n");
  }
  
  // f/f' - 1. if f' is zero, don't do the division. Keep it to zero.
  for(i=0;i<NF;i++){
    if(Signal1[i]!=0){
      Signal1[i] = Signal[i] / Signal1[i] - 1;
    }
  }
  
  printf("Normalized Signal\n");
  for (r = 0; r < RF; ++r){
    for (c = 0; c < CF; ++c){
      printf("%7.3f ", Signal1[r*CF+c]);
    }
    printf("\n");
  }
  
  //  float *absF;
  getFFT(Signal1,absF);
  
  printf("Abs F (not shifted)\n");
  k = 0;
  int C1 = CF/2+1;
  for (r = 0; r < RF; ++r){
    for (c = 0; c < C1; ++c){
      printf("%7.3f ", absF[k++]);
    }
    printf("\n");
  }
  
  // Perform FFT Shift
  memcpy(absF2,&absF[(RF/2)*C1],4*(RF/2)*C1); // float=4x word.
  memcpy(&absF2[(RF/2)*C1],absF,4*(RF/2+1)*C1);
  
  
  //  for (c = 0; c < C1; c++){
  //    absF2[(RF)*C1 + c] = absF2[ c ];
  //  }
  
  //  // Copy column 3 into column 1, inversed.
  //  for (r = 0; r < RF+1; ++r){
  //    absF2[r*C1 ] = absF2[ (RF-r)*C1 + 2];
  //  }
  
  
  printf("\n\nShuffled Abs F\n");
  k = 0;
  for (r = 0; r < RF+1; ++r){
    for (c = 0; c < C1; ++c){
      printf("%7.3f ", absF2[k++]);
    }
    printf("\n");
  }
  
  
  
  
  printf("\n");
}

