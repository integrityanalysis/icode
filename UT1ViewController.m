//
//  UT1ViewController.m
//  SquareCam 
//
//  Created by Zeke Chan on 26/11/16.
//
//

#import "UT1ViewController.h"
#import "plotView.h"
#include <stdint.h> // int 64

#include "nr.h"
#include "nrutil.h"
#include "Math.h"
#import "ic_engine.h"
#include <Accelerate/Accelerate.h>
#import <AWSCore/AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "Code.h"
#import "Scan.h"
#import "ICG.h"
#import "Code.h"
#import "ICWebViewController.h"
#import "UIImage+Resize.h"
#import <QuartzCore/QuartzCore.h>
#import <ZXingObjC/ZXingObjC.h>
#import <Crashlytics/Crashlytics.h>
#import "Reachability.h"
@interface UT1ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *iv01;
@property (strong, nonatomic) IBOutlet plotView *vwPlot;
@property (strong, nonatomic) IBOutlet UIImageView *ivSquare;

@end

@implementation UT1ViewController


#define ndata 384
float lf[SQW],X[2][5000],Mt[90000],tmp[3000];


void test_fitEllipse(){
  printf("\n***** Start test fitEllipse *****\n");
  // from tmp7. fprintf('%d,',[f.col; f.row])
  float data[384*2]={266,266,266,266,266,266,266,267,267,267,267,267,278,278,279,279,279,279,279,279,279,284,284,284,285,285,285,285,285,285,285,290,290,291,291,290,291,291,291,291,312,313,312,314,313,314,313,314,313,314,313,314,313,314,313,314,318,319,318,319,319,319,328,329,328,329,328,329,328,331,329,330,329,329,333,334,334,333,333,335,334,335,336,337,337,338,339,337,338,339,340,336,338,339,340,336,338,336,339,340,342,343,344,342,343,344,342,343,344,345,343,343,345,347,346,347,346,348,347,346,348,347,346,347,347,349,350,352,351,352,351,348,352,351,350,353,352,351,352,351,359,361,360,362,361,360,362,364,361,363,362,364,361,363,362,362,363,366,367,368,369,367,368,369,367,369,367,368,367,368,369,370,371,369,370,371,369,372,370,371,370,373,371,369,371,372,373,371,372,373,371,374,372,373,374,372,375,373,372,374,375,374,375,376,376,374,375,375,369,372,375,377,376,375,379,378,377,379,378,380,379,378,376,376,375,374,377,376,378,377,376,379,378,377,380,379,381,369,368,368,370,368,372,369,371,370,372,375,367,365,367,366,365,366,368,367,369,368,369,361,366,364,362,363,361,362,365,363,366,367,366,366,357,360,354,362,355,351,349,350,351,349,352,353,350,351,352,353,340,341,342,343,341,345,344,341,344,343,338,332,333,332,331,332,335,334,333,335,327,334,327,328,328,327,329,326,331,330,327,331,328,330,331,330,320,322,317,313,312,313,314,311,312,314,312,313,301,303,301,302,303,301,302,303,301,302,300,301,298,296,296,295,296,297,295,296,297,295,296,297,294,295,296,295,294,290,290,291,290,291,289,290,289,290,289,290,289,286,284,285,263,263,263,262,131,132,133,134,135,136,137,134,135,136,137,138,134,135,134,135,136,137,138,139,140,134,135,136,135,136,137,138,139,140,141,136,137,136,137,141,138,139,140,142,146,145,147,144,146,145,147,146,148,147,149,148,150,149,151,150,149,148,152,151,153,154,156,155,157,156,159,158,160,156,159,158,160,161,160,159,160,162,163,161,164,164,164,163,164,164,163,166,165,164,163,168,166,165,164,169,167,170,167,170,168,168,167,170,169,168,171,170,169,168,171,172,172,172,173,173,174,173,174,175,174,175,176,176,177,177,177,176,177,177,178,181,178,179,180,178,179,180,180,181,193,192,193,192,193,194,193,192,194,193,194,193,195,194,195,196,196,203,203,203,203,204,204,204,205,205,206,206,208,208,208,208,208,209,209,209,210,209,210,210,211,210,211,214,214,214,214,215,215,215,216,215,216,216,216,217,216,217,219,219,219,220,220,220,221,222,222,223,252,253,253,273,273,273,274,274,274,275,275,276,276,277,278,279,279,279,280,280,281,281,281,282,282,282,283,284,293,300,301,302,303,303,305,304,305,305,306,308,306,307,308,308,308,309,310,310,311,311,312,311,314,313,312,313,312,313,315,314,316,317,317,318,316,319,316,323,319,333,332,333,334,333,336,337,335,336,337,338,333,341,342,344,342,347,346,343,347,346,352,349,351,350,349,351,355,354,353,356,346,355,351,353,354,353,356,352,359,358,354,360,356,359,361,360,358,368,363,364,363,365,367,362,364,368,365,367,367,373,368,371,374,369,372,375,370,373,368,371,372,370,371,369,372,375,370,373,376,371,374,377,369,372,375,373,370,372,373,377,374,378,371,375,372,376,373,377,374,374,378,385,369,379,381,369};
  
  float ellipseParam[3];
  for(int i=0;i<384;i++){
    data[i] -= 256.5;
    data[384+i] -= 256.5;
    printf("%d  %f  %f \n",i,data[i] ,data[384+i]);
  }
  fitEllipse(&data[0],&data[384], 384, ellipseParam);
  printf("\n***** End test fitEllipse *****\n\n");
}


void test_readPeaks(){
  printf("\n***** Start test fitEllipse *****\n");
  // Read CSV file from bundle
  int i,j,k;
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"lf3" ofType:@"csv"];
  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
  if (myData) {
    // do something useful
    NSLog(@"file found");
    
    j = 0;
    //    NSString *file     = [[NSString alloc] initWithContentsOfFile:filePath];
    
    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* line in allLines) {
      //      NSLog(@"%@", line);
      NSArray *elements = [line componentsSeparatedByString:@","];
      int ne = [elements count];
      //      printf("count %d \n",ne);
      if(ne==257){
        for(i=0;i<ne;i++)
          lf[j++] = [elements[i] floatValue];
      }
    }
    
    // 512x257 - half square.
    for (i=0;i<20;i++) printf("half sq %d %f \n",i,lf[i]);
  }
  
  float ellipseParam[3],conf[63];
  ellipseParam[0]  = 122.7611;
  ellipseParam[1]  = 120.9657;
  ellipseParam[2]  = -0.7824;
  
  readPeaks(lf, ellipseParam, conf);
  
  randomDecode(conf, 100);
  
  printf("\n***** End test readPeaks *****\n\n");
}

void test_decode01(){
  // #####################################################################
  // ---------- Decode
  // #####################################################################
  printf("\n***** test Decode *****\n\n");
  
  
  // bchdec_mex(C.m, C.k, C.t, seqRx);
  // m:6, n:63, t:6, k:30, seqRx=[1x63] binary.
  unsigned char rx[63]={0,1,0,1,1,1,0,0,0,1,1,0,1,1,1,1,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,0,0,0,1,0,1,0,1,1,0,0,1,0,1,1,1,0,0,0,0,0}; // dec should be 3330383200116716256
  
  rx[3]  = !rx[3];
  rx[1]  = !rx[1];
  rx[5]  = !rx[5];
//  rx[15] = !rx[15];
//  rx[11] = !rx[11];
//  rx[10] = !rx[10];
  //  rx[12] = !rx[12];
  
  unsigned char ucRx[63];
  
  int nCorrected;
  int flag = myDecoder(rx,ucRx,&nCorrected);
  printf("Decoder flag %d (corrected %d)\n",flag,nCorrected);
  
  
  if(!(flag==0 || flag==1)){
    printf("Decode error %d\n",flag);
    return;
  }
  
  // Show original binary
  printf("\nucRx\n");
  for(int i=0;i<63;i++){
    printf("%d",ucRx[i]);
  }
  printf("\n");
  
  // Binary to Decimal conversion
  //  unsigned long long int decDecoded= 0;//0x0000444400004444LL;
  //  for(int i=0;i<63;i++){
  //    if(ucRx[i]==1){
  //      decDecoded |= 1ULL << i;
  //    }
  //  }
  
  // 3330383200116716256
  unsigned long long int decDecoded   = b2d63(ucRx);
  unsigned long long int decDecodedMx = decMax63(decDecoded);
  
  int found = -1;
  printf(" decDecoded %llu   decDecodedMx %llu  \n#### found = %d ####\n",decDecoded,decDecodedMx,found);
 
  
  // Show decoded binary/decimal.
//  if(1){
//    printf(" decDecoded %llu    \n",decDecoded);
//    for(int i=0;i<63;i++){
//      int bit = (decDecoded >> i) & 1;
//      printf("%d",bit);
//    }
//    printf("\n");
//  }
  printf("\n***** End test Decode *****\n\n");
}


void test_randomDecode(){
  float p[63] = {0.010000,0.990000,0.010000,0.990000,0.990000,0.010000,0.990000,0.990000,0.990000,0.010000,0.990000,0.990000,0.990000,0.010000,0.010000,0.010000,0.010000,0.990000,0.010000,0.010000,0.937500,0.937500,0.990000,0.843750,0.010000,0.656250,0.990000,0.990000,0.010000,0.010000,0.010000,0.010000,0.990000,0.010000,0.010000,0.990000,0.010000,0.010000,0.010000,0.010000,0.990000,0.010000,0.010000,0.990000,0.010000,0.010000,0.562500,0.990000,0.990000,0.990000,0.010000,0.990000,0.010000,0.656250,0.750000,0.562500,0.010000,0.010000,0.990000,0.937500,0.010000,0.010000,0.468750};
  
  int nDecodeRun = 3600;
  double clockTime0 = CACurrentMediaTime();
  randomDecode(p, nDecodeRun);
  printf("Time taken for %d decode run: %f \n",nDecodeRun,CACurrentMediaTime()-clockTime0);
  
  nDecodeRun = 500;
  clockTime0 = CACurrentMediaTime();
  randomDecode(p, nDecodeRun);
  printf("Time taken for %d decode run: %f \n",nDecodeRun,CACurrentMediaTime()-clockTime0);

}



// Unit Test on FFT and filtered FFT. Adjust Log2R/Log2C and display
// For testing.
// Not much time saving by taking FFT Setup out.
float signalx[NF],absFx[NF];
void test_FFT(){
  int r,c,i,j,k;
  for (r = 0; r < RF; ++r)
    for (c = 0; c < CF; ++c)
      signalx[(r*CF + c) ] = (99*(r*r+c*c+c))%256;
  
//  printf("***** Original signal \n");
//  for (r = 0; r < RF; ++r){
//    for (c = 0; c < CF; ++c)
//      printf("%7.3f ", signalx[r*CF + c]);
//    printf("\n");
//  }
  
  double clockTime0 = CACurrentMediaTime();
  for(i=0;i<1000;i++){
    printf("%d FFT\n",i);
    // Very little time difference whether FFT setup was done once
    // or over every loop.
//    getFFT(signalx,absFx);
    
    // 24 ms on iPhone 5.
    getFilteredFFT(signalx, absFx);
  }
  printf("Time taken %f \n",CACurrentMediaTime()-clockTime0);

}

float _lf3[SQPX];
void test_getMeanRadius(){
  printf("\n***** Start test Get Mean Radius *****\n");
  // Read CSV file from bundle
  int i,j,k;
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"lf3" ofType:@"csv"];
  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
  if (myData) {
    // do something useful
    NSLog(@"file found");
    
    j = 0;
    //    NSString *file     = [[NSString alloc] initWithContentsOfFile:filePath];
    
    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* line in allLines) {
      //      NSLog(@"%@", line);
      NSArray *elements = [line componentsSeparatedByString:@","];
      int ne = [elements count];
      //      printf("count %d \n",ne);
      if(ne==257){
        for(i=0;i<ne;i++)
          _lf3[j++] = [elements[i] floatValue];
      }
    }
    
  }
  
  printf("NF %d RF %d CF %d NE %d\n",NF,RF,CF,NE);
  
  for(i=0;i<20;i++) printf("%7.3f ",_lf3[i]);
  
  // Get mean radius - output is stored in structure "gp"
  getMeanRadius(_lf3);
  
  // Display results
  printf("i r c v rad use \n");
  for(i=0;i<gp.n;i++){
    printf("%d %f %f %f %f %d\n",i,gp.r[i],gp.c[i],gp.v[i],gp.rad[i],gp.use[i]);
  }
  
}

float _tmpIm[NF],_absF[NF],_dataRing1[NE*50],_dataRing2[NE*50];;
void test_Image(){
  
  
  printf("\n***** Start test import and Process Image *****\n");
  // Read CSV file from bundle
  int i,j,k;
//  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Im512" ofType:@"csv"];
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Im256_tower" ofType:@"csv"];
  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
  if (myData) {
    // do something useful
    NSLog(@"file found");
    
    j = 0;
    //    NSString *file     = [[NSString alloc] initWithContentsOfFile:filePath];
    
    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* line in allLines) {
      //      NSLog(@"%@", line);
      NSArray *elements = [line componentsSeparatedByString:@","];
      int ne = [elements count];
      //      printf("count %d \n",ne);
      if(ne==SQW){
        for(i=0;i<ne;i++)
          _tmpIm[j++] = [elements[i] floatValue];
      }
    }
    
  }
  
  // -------------------------------
  LOGD("Load Matlab Data");
  unsigned char *im = loadMatlabData();
  for(int i=0;i<256*256;i++){
    _tmpIm[i] = im[i];
  }
  // -------------------------------
  
  float *Image=_tmpIm;
  
  double clockTime0 = CACurrentMediaTime();
  for(int counter=0;counter<1;counter++){
    printf("----- ----- counter %d\n");
    
//    unsigned long long int dec= myEngine(_tmpIm);
  
    // ----- 1. Get filtered FFT
    clockTime0 = CACurrentMediaTime();
    getFilteredFFT(Image, _absF);
    printf("Time FFT %f \n",CACurrentMediaTime()-clockTime0);
    
    printReset;
    for(int i=0;i<256*268;i+=700){
      printAppend("%4.2f,",_absF[i]);
    }
    printOut;
    
    // ----- 2. Get mean Radius
    // Get mean radius - output is stored in structure "gp"
    clockTime0 = CACurrentMediaTime();
    getMeanRadius(_absF);
    printf("Time mean Radius %f \n",CACurrentMediaTime()-clockTime0);
  
    
    // ----- 3. Fit into Ellipse
    clockTime0 = CACurrentMediaTime();
    float ellipseParam[3],conf[63];
    k = 0;
    for(i=0;i<gp.n;i++){
      if(gp.use[i]==1){
        _dataRing1[k] = gp.c[i];
        _dataRing2[k] = gp.r[i];
        LOGD("%d  %f  %f ",k,_dataRing1[k] ,_dataRing2[k]);
        k++;
      }
    }
    
//    return;
    // Column (0 to 256), row (from -256 to +256]
    fitEllipse(_dataRing1,_dataRing2, k, ellipseParam);
    printf("Time Ellipse %f \n",CACurrentMediaTime()-clockTime0);
    
    // ----- 4. Read Peaks
    clockTime0 = CACurrentMediaTime();
    readPeaks(_absF, ellipseParam, conf);
    printf("Time Read Peak %f \n",CACurrentMediaTime()-clockTime0);
    
    // ----- 5. Random decode
    clockTime0 = CACurrentMediaTime();
    unsigned long long int dec = randomDecode(conf,100);
    unsigned long long int decDecodedMx = decMax63(dec);
   
    int found = -1;
//    int found = searchBase(decDecodedMx);
    printf(" decDecoded %llu   decDecodedMx %llu  found= %d\n",dec,decDecodedMx,found);

    
    
  printf("Time Decode %f \n",CACurrentMediaTime()-clockTime0);
    
  }


  printf("Time taken %f \n",CACurrentMediaTime()-clockTime0);
}


void test_Image2(){
  
  
  printf("\n***** Start test import and Process Image *****\n");
  // Read CSV file from bundle
  int i,j,k;
  NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Im512" ofType:@"csv"];
  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
  if (myData) {
    // do something useful
    NSLog(@"file found");
    
    j = 0;
    //    NSString *file     = [[NSString alloc] initWithContentsOfFile:filePath];
    
    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* line in allLines) {
      //      NSLog(@"%@", line);
      NSArray *elements = [line componentsSeparatedByString:@","];
      int ne = [elements count];
      //      printf("count %d \n",ne);
      if(ne==SQW){
        for(i=0;i<ne;i++)
          _tmpIm[j++] = [elements[i] floatValue];
      }
    }
    
  }
  
  NSMutableArray *arr = [NSMutableArray array];
  float *Image=_tmpIm;
  
  double clockTime0 = CACurrentMediaTime();
  int found=0;
  for(int counter=0;counter<10000;counter++){
    if(counter%100==0) printf("----- ----- counter %d\n",counter);
    unsigned long long int dec = myEngine(Image);
//    found = searchBase(dec);
    
    
    NSDictionary *dict = @{@"address":@"https://www.swingprofile.com",@"title":@"Swing Profile"};
    for(int i=0;i<10;i++){
      [arr addObject:dict];
    }
    
  }
  
  
  printf("Time taken %f \n",CACurrentMediaTime()-clockTime0);
}



void test_Image3(){

  
  // -------------------------------
  LOGD("Load Matlab Data");
  unsigned char *im = loadMatlabData();
  for(int i=0;i<256*256;i++){
    _tmpIm[i] = im[i];
  }
  // -------------------------------
  
  float *Image=_tmpIm;
  
  double clockTime0 = CACurrentMediaTime();

    
    //    unsigned long long int dec= myEngine(_tmpIm);
  if(0){
    // ----- 1. Get filtered FFT
    clockTime0 = CACurrentMediaTime();
    getFilteredFFT(Image, _absF);
    printf("Time FFT %f \n",CACurrentMediaTime()-clockTime0);
    
    printReset;
    for(int i=0;i<256*268;i+=700){
      printAppend("%4.2f,",_absF[i]);
    }
    printOut;
  }
  
//  myEnginex(_absF);
  myEngine(Image);

}



- (void)viewDidLoad {
    [super viewDidLoad];
  
    // Do any additional setup after loading the view.
  initializeLookup();
//  test_fitEllipse();
//
  //
//  test_readPeaks();
//
  test_randomDecode();
//

  printf("NF %d RF %d CF %d\n",NF,RF,CF);
  test_Image3();
  
  
    test_decode01();
//  [self buttonUT02:nil];
  
//  test_FFT();
//  test_Image2();
//  test_getMeanRadius();
  
//  unsigned long long int dec   = 6924533684978347979;
//  unsigned long long int decMx = dec;
//  for(int i=0;i<62;i++){
//    int lsb = dec & 1ULL;
//    dec   = (dec>>1) + lsb*4611686018427387904;
//    decMx = MAX(dec,decMx);
//    
//    printf("%d  %lld %lld %d\n",i,dec,decMx,lsb);
//    
//  }
  
//  NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//  dict[@"x"]    = @[@(-2),@(2),@(3),@(4),@(5)];
//  dict[@"y"]    = @[@(10),@(20),@(30),@(40),@(50)];
//  dict[@"xlim"] = @[@(0),@(10)];
//  dict[@"ylim"] = @[@(0),@(50)];
//  dict[@"grid"] = @[@(2),@(5)];
//  _vwPlot.d = dict;
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)buttonUT22:(id)sender {
  // Toggle show plot
  [ICG sharedICG].showPlot = abs([ICG sharedICG].showPlot-1);
  NSLog(@"show plot %d",[ICG sharedICG].showPlot );
}


// Fetch (load) item from table
- (IBAction)buttonUT01:(id)sender {

  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
  // Load the item using the hash key.
  [[objMapper load:[Code class] hashKey:@123456789 rangeKey:nil] continueWithBlock:^id _Nullable(AWSTask * _Nonnull task) {
    if (task.error) {
      NSLog(@"The request failed. Error: [%@]", task.error);
    }
    
    if (task.result) {
      Code *code = task.result;
      //Do something with the result.
      NSLog(@"%@", code.Desc);
      
    }
    return nil;
  }];
}
- (IBAction)UT23:(id)sender {
  [ICG sharedICG].noForceFocus = ![ICG sharedICG].noForceFocus ? 1 : 0;
}


- (IBAction)UT31:(id)sender {
    [ICG sharedICG].testMode = abs([ICG sharedICG].testMode-1);
  //  NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:[ICG sharedICG].arrayCode];
  //  BOOL success = [data2 writeToFile:[ICG sharedICG].fScan atomically:YES];
  //  NSAssert(success, @"writeToFile failed");
  //
  //  NSData *data3 = [[NSData alloc] initWithContentsOfFile:[ICG sharedICG].fScan];
  //  NSArray *arr2 = [NSKeyedUnarchiver unarchiveObjectWithData:data3];
  //  NSAssert(arr2, @"arrayWithContentsOfFile failed");
  //  NSLog(@"original array %@",[ICG sharedICG].arrayCode);
  //  NSLog(@"loaded array %@",arr2);
  
  //  NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:[ICG sharedICG].arrayCode];
  //  BOOL success = [[ICG sharedICG].arrayCode writeToFile:[ICG sharedICG].fScan atomically:YES];
  //  NSAssert(success, @"writeToFile failed");
  //
  //  NSArray *data3 = [NSArray arrayWithContentsOfFile:[ICG sharedICG].fScan];
  ////  NSArray *arr2 = [NSKeyedUnarchiver unarchiveObjectWithData:data3];
  //  NSAssert(data3, @"arrayWithContentsOfFile failed");
  //  NSLog(@"original array %@",[ICG sharedICG].arrayCode);
  //  NSLog(@"loaded array %@",data3);
  
  
  
//  NSDictionary *d = @{@"key1" : [ICG sharedICG].arrayCode,
//                      @"key2" : @"Hops",
//                      @"key3" : @"Malt",
//                      @"key4" : @"Yeast" };
//  [d writeToFile: [ICG sharedICG].fScan atomically:YES];
//  NSDictionary *e = [NSDictionary dictionaryWithContentsOfFile:[ICG sharedICG].fScan];
//  NSLog(@"original dict %@",d);
//  NSLog(@"loaded dict %@",e);
//  NSLog(@"original key1 %@",d[@"key1"]);
//  NSLog(@"loaded key1 %@",e[@"key1"]);
  
  //  NSArray *myarray = @[@"ola",@"alo",@"hello",@"hola"];
  //  NSArray *myarray = [[NSArray alloc] initWithArray: [ICG sharedICG].arrayCode];
  //  BOOL success = [myarray writeToFile:[ICG sharedICG].fScan atomically:YES];
  //  NSAssert(success, @"writeToFile failed");
  //
  //
  //  NSArray *array2 = [NSArray arrayWithContentsOfFile:[ICG sharedICG].fScan];
  //  NSAssert(array2, @"arrayWithContentsOfFile failed");
  
  
  //  NSMutableDictionary *e = [[NSMutableDictionary alloc] initWithContentsOfFile:[ICG sharedICG].fScan];
  //  NSLog(@"original d %@",[ICG sharedICG].arrayCode);
  //
  //  NSLog(@"loaded e %@",arr2);
  
  
  //  if ([[NSFileManager defaultManager] fileExistsAtPath:_fInfoFile]){
  //    NSData *data    = [NSData dataWithContentsOfFile:_fInfoFile];
  //    NSDictionary *dictVideoInfo  = [[CJSONDeserializer deserializer] deserializeAsDictionary:data error:nil];
  
  
  //    // Copy dictionary element by element instead of passing ptr.
  //    // This ensures all default values are present.
  //    NSMutableDictionary *vi = dictVideoInfo[@"videoInfo"];
  //    for(NSString *key in vi){
  //      [_videoInfo setObject:[vi objectForKey:key] forKey:key];
  //    }
  //    NSMutableDictionary *pi = dictVideoInfo[@"playerInfo"];
  //    for(NSString *key in pi){
  //      [_playerInfo setObject:[pi objectForKey:key] forKey:key];
  //    }
  //    NSMutableDictionary *si = dictVideoInfo[@"senderInfo"];
  //    for(NSString *key in si){
  //      [_senderInfo setObject:[si objectForKey:key] forKey:key];
  //    }
  //
  //    // Re-initialize in case of empty field: due to json conversion
  //    NSMutableArray *tmp;
  //    tmp = [_videoInfo objectForKey:@"points"];
  //    if(tmp.count==0)  [_videoInfo setObject:[[NSMutableArray alloc] init] forKey:@"points"];
  
  //  }else{
  //    NSLog(@"**** No videoInfo.json file at %@",_fInfoFile);
  //  }
  

}
- (IBAction)buttonUT32:(id)sender {
  
  // https://github.com/TheLevelUp/ZXingObjC
  UIImage *im = [UIImage imageNamed:@"barcode-13.png"];
  CGImageRef imageToDecode = im.CGImage;  // Given a CGImage in which we are looking for barcodes
  ZXLuminanceSource *source = [[ZXCGImageLuminanceSource alloc] initWithCGImage:imageToDecode];
  ZXBinaryBitmap *bitmap = [ZXBinaryBitmap binaryBitmapWithBinarizer:[ZXHybridBinarizer binarizerWithSource:source]];

  NSError *error = nil;

  // There are a number of hints we can give to the reader, including
  // possible formats, allowed lengths, and the string encoding.
  ZXDecodeHints *hints = [ZXDecodeHints hints];

  ZXMultiFormatReader *reader = [ZXMultiFormatReader reader];
  ZXResult *result = [reader decode:bitmap
                              hints:hints
                              error:&error];
  if (result) {
    // The coded result as a string. The raw data can be accessed with
    // result.rawBytes and result.length.
    NSString *contents = result.text;

    // The barcode format, such as a QR code or UPC-A
    ZXBarcodeFormat format = result.barcodeFormat;
    NSLog(@"Zxing format %d \nContents %@", format, contents);
  } else {
    // Use error to determine why we didn't get a result, such as a barcode
    // not being found, an invalid checksum, or a format inconsistency.
    NSLog(@"Zxing error %@",error);
  }
}
- (IBAction)buttonUT11:(id)sender {
  // http://docs.aws.amazon.com/mobile/sdkforios/developerguide/dynamodb-object-mapper.html
  
}
- (IBAction)buttonUT12:(id)sender {
  // Help Page - re-use
  Code *code    = [[Code alloc] init];
  code.Code     = 0;
  code.ClientId = @"";
  code.Desc     = @"";
  code.Url      = @"https://www.integrityanalysis.com";
  
  ICWebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
  wvc.code      = code;
  wvc.noAnimate = YES;
  
  // Crop Image into Square, then save to JPEG.
  // Name file as picXXX.jpg where XXX is code.ScannedAt
//  UIImage *imSq = [latestCaptureImage thumbnailImage:256
//                                   transparentBorder:0
//                                        cornerRadius:10
//                                interpolationQuality:kCGInterpolationHigh];
//  
//  NSString *f = [docDir stringByAppendingPathComponent:ptf(@"pic%@.jpg",code.ScannedAt)];
//  [UIImageJPEGRepresentation(imSq,0.9f) writeToFile:f atomically:YES];
  
  //      wvc.thumbnail = imSq;//latestCaptureImage;
  wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
  [self.navigationController pushViewController:wvc animated:YES];
  
//  [self presentViewController:wvc animated:YES completion:nil];
}

// Scan for items from table - Not working yet
- (IBAction)buttonUT02:(id)sender {
 
  [[ICG sharedICG] downloadCodesFromCloud];
  
  
//  // Test code
//  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
//  
//  AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
//    scanExpression.limit = @1000;// how many table items are examined, not results returned.
//
//  
//  // Need to put filter the table using a filter expression
//  scanExpression.filterExpression = @"UpdatedAt > :date AND ExpiredAt = :expire";
//  scanExpression.expressionAttributeValues = @{@":date":@1,@":expire":@5};
//  
//  
//  [[objMapper scan:[Code class]
//                   expression:scanExpression]
//   continueWithBlock:^id(AWSTask *task) {
//     if (task.error) {
//       NSLog(@"The request failed. Error: [%@]", task.error);
//     }
//     if (task.exception) {
//       NSLog(@"The request failed. Exception: [%@]", task.exception);
//     }
//     if (task.result) {
//       AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
//       int i=0;
//       for (Code *code in paginatedOutput.items) {
//         //Do something with book.
//
//         printf("qry[%d] \n",i);
//        NSLog(@"%@", code);
//         i++;
//       }
//     }
//     return nil;
//   }];
  
//  // Test code
//  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
//  
//  AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
//  scanExpression.limit = @1000; // how many table items are examined, not results returned.
//  
//  // Need to put filter the table using a filter expression
//  scanExpression.filterExpression = @"UpdatedAt > :date AND ExpiredAt = :expire";
//  scanExpression.expressionAttributeValues = @{@":date":@1,@":expire":@5};
//  
//  
//  [[objMapper scan:[Code class]
//        expression:scanExpression]
//   continueWithBlock:^id(AWSTask *task) {
//     if (task.error) {
//       NSLog(@"The request failed. Error: [%@]", task.error);
//     }
//     if (task.exception) {
//       NSLog(@"The request failed. Exception: [%@]", task.exception);
//     }
//     if (task.result) {
//       AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
//       int i=0;
//      
//       for (Code *code in paginatedOutput.items) {
//         // Do something with book.
//         // Add code to array, update the largest update date
//         
////         int updatedAt = code.UpdatedAt.intValue;
//         if( code.UpdatedAt.intValue > [ICG sharedICG].latestUpdatedAt ) {
//           [ICG sharedICG].latestUpdatedAt = code.UpdatedAt.intValue;
//         }
//         
//         printf("qry[%d] \n",i);
//         NSLog(@"%@", code);
//         NSLog(@"Latest Updated at %d", [ICG sharedICG].latestUpdatedAt);
//         i++;
//       }
//     }
//     return nil;
//   }];

 // [[ICG sharedICG] resetUserDefaults];
 // [[ICG sharedICG] saveUserDefaults];
  
  
  
//  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
//  AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
//  
//  queryExpression.indexName = @"IcRegion-UpdatedAt-index";
//  queryExpression.keyConditionExpression    = @"IcRegion = :reg AND UpdatedAt > :val";
//  queryExpression.expressionAttributeValues = @{@":reg":@"NZL",
//                                                @":val":@([ICG sharedICG].latestUpdatedAt)};
//    printf("Before call: latest Updated At %f\n",[ICG sharedICG].latestUpdatedAt);
////  queryExpression.keyConditionExpression = @"IcRegion = :reg ";
////  queryExpression.expressionAttributeValues = @{@":reg":@"NZL"};
////  queryExpression.expressionAttributeNames = @{@"#reg":@"Region"};
//  [[objMapper query:[Code class]
//                    expression:queryExpression]
//   continueWithBlock:^id(AWSTask *task) {
//    
//     if (task.error) {
//       NSLog(@"The request failed. Error: [%@]", task.error);
//     }
//     if (task.exception) {
//       NSLog(@"The request failed. Exception: [%@]", task.exception);
//     }
//     if (task.result) {
//       AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
//       for (Code *code in paginatedOutput.items) {
//         //Do something with book.
//         NSLog(@"q Code %@", code);
//         
//          [[ICG sharedICG] updateArrayCode:code];
//         
//         if( code.UpdatedAt.doubleValue > [ICG sharedICG].latestUpdatedAt ) {
//           [ICG sharedICG].latestUpdatedAt = code.UpdatedAt.doubleValue;
//         }
//         
//        
//
//         
//         
//       }
//       printf("After call:latest Updated At %f\n",[ICG sharedICG].latestUpdatedAt);
//     }
//     return nil;
//   }];
  
  
}

- (IBAction)buttonUT05:(id)sender {
  [[ICG sharedICG] updateLookupCode];
  
//  // Search for expired code and delete - looping backward for delete
//  double unixNow = [[NSDate date] timeIntervalSince1970];
//  for(int i=[ICG sharedICG].arrayCode.count-1;i>=0;i--){
//    Code *c = [ICG sharedICG].arrayCode[i];
//    
//    printf("%d Created at %f / now %f \n",i,c.CreatedAt.doubleValue,unixNow);
//    NSLog(@"Created At String %@",c.CreatedAtS);
//    
//    // Check expiry - c.ExpireIn is in DAYS.
//    BOOL expired = unixNow > (c.CreatedAt.doubleValue + c.ExpireIn.doubleValue*3600*24);
//    if(expired){
//      printf("%d *** deleted!! \n",i);
//      [[ICG sharedICG].arrayCode removeObjectAtIndex:i];
//    }
//  }
//  
//  
//  // Add Code from arrayCode to LK.code - much faster search.
//  LK.nCode = [ICG sharedICG].arrayCode.count;
//  for(int i=0;i<LK.nCode;i++){
//    Code *c = [ICG sharedICG].arrayCode[i];
//    LK.code[i] = [c.Code unsignedLongLongValue];
//    
//    printf("%d LK.code %llu / %d\n",i,LK.code[i],LK.nCode);
//  }
  
  
}


- (IBAction)buttonUT06:(id)sender {
  
  // Test binary search
  for(int i=0;i<1000000;i++){
    LK.code[i] = i*6;
  }
  LK.nCode = 1000000;
  
  for(int j=3000;j<3020;j++){
    double clockTime0 = CACurrentMediaTime();
    int index;
    for(int k=0;k<100;k++) index = myBinarySearch(j);
    printf("c Time taken 100 runs %f  \n",CACurrentMediaTime()-clockTime0);
    printf("Index = %d \n", index );
    if(index>0) printf(" LK.code %llu\n", LK.code[index]);
  }

}

- (IBAction)buttonUT35:(id)sender {
  // Toggle show plot
  [ICG sharedICG].scaleDown = abs([ICG sharedICG].scaleDown-1);
  NSLog(@"Scale Down %d",[ICG sharedICG].scaleDown );
}
- (IBAction)buttonUT36:(id)sender {
  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
//    AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
  
  // NSTimeInterval is defined as double
  NSTimeInterval timeStamp = [[NSDate date] timeIntervalSince1970];
  
  Scan *myScan = [Scan new];
//  myScan.TimeStamp = [NSNumber numberWithDouble: timeStamp];
  myScan.UserId = @"Test User";
  myScan.Note   = @"Create timestamp of now as NSNumber in objective-c";
  [[objMapper save:myScan] continueWithBlock:^id(AWSTask *task) {
     if (task.error) {
       NSLog(@"The request failed. Error: [%@]", task.error);
     } else {
       //Do something with task.result or perform other operations.
       NSLog(@"Upload completed! [%@]", task.error);
     }
     return nil;
   }];
  
//  2018-10-29 18:50:11.806698+1300 ICode[2874:133743] AWSiOSSDK v2.5.2 [Debug] AWSURLResponseSerialization.m line:63 | -[AWSJSONResponseSerializer responseObjectForResponse:originalRequest:currentRequest:data:error:] | Response body:
//  {"__type":"com.amazon.coral.service#AccessDeniedException","Message":"User: arn:aws:sts::367618052723:assumed-role/Cognito_ICodeUnauth_Role/CognitoIdentityCredentials is not authorized to perform: dynamodb:UpdateItem on resource: arn:aws:dynamodb:ap-southeast-2:367618052723:table/Scans1"}
//  2018-10-29 18:50:18.009109+1300 ICode[2874:133743] The request failed. Error: [Error Domain=com.amazonaws.AWSServiceErrorDomain Code=6 "(null)" UserInfo={__type=com.amazon.coral.service#AccessDeniedException, Message=User: arn:aws:sts::367618052723:assumed-role/Cognito_ICodeUnauth_Role/CognitoIdentityCredentials is not authorized to perform: dynamodb:UpdateItem on resource: arn:aws:dynamodb:ap-southeast-2:367618052723:table/Scans1}]
 
}

int _arr[1000000];
- (IBAction)buttonUT04:(id)sender {
  initializeLookup();
  test_Image3();
  
//  [ICG sharedICG].latestUpdatedAt=0;
//  
//  // Array matching speed
//  int c = 1000000;
//  // Test search speed
//  NSLog(@" ----- Search Speed Test ------");
//  NSMutableArray *a = [NSMutableArray array];
//  NSMutableArray *d = [NSMutableArray array];
//  for(int i=0;i<c;i++){
//    a[i] = @(i+30);
//    d[i] = @{@"code":@(i+30),@"others":@"something"};
//    _arr[i] = i+30;
//  }
//  
//  // For number kind of values:
//  double clockTime0 = CACurrentMediaTime();
//  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF = %@", @(100)];
//  NSArray *results = [a filteredArrayUsingPredicate:predicate];
//  printf("a Time taken %f \n",CACurrentMediaTime()-clockTime0);
//  NSLog(@"a results %@",results);
//  
//
//  clockTime0 = CACurrentMediaTime();
//  NSPredicate *predicated = [NSPredicate predicateWithFormat:@"code = %@", @(100)];
//  NSArray *resultsb = [d filteredArrayUsingPredicate:predicated];
//  printf("d Time taken %f \n",CACurrentMediaTime()-clockTime0);
//  NSLog(@"d results %@",resultsb);
//  
//  clockTime0 = CACurrentMediaTime();
//  int resultc=0;
//  for(int i =0;i<1000000;i++){
//    if(d[i][@"code"] == @(100)) resultc = i;
//  }
//  printf("c Time taken %f \n",CACurrentMediaTime()-clockTime0);
//
//  
//  
//  clockTime0 = CACurrentMediaTime();
//  int result=0;
//  for(int i =0;i<1000000;i++){
//    if(_arr[i] == 100) result = i;
//  }
// printf("e Time taken %f \n",CACurrentMediaTime()-clockTime0);
//  NSLog(@"e results %d",result);
//  
//  
//  NSLog(@"\n\n##### Binary");
//    clockTime0 = CACurrentMediaTime();
//  NSArray *sortedArray = a;
//  id searchObject = @(100);
//  NSRange searchRange = NSMakeRange(0, [sortedArray count]);
//  NSUInteger findIndex = [sortedArray indexOfObject:searchObject
//                                      inSortedRange:searchRange
//                                            options:NSBinarySearchingInsertionIndex
//                                    usingComparator:^(id obj1, id obj2)
//                          {
//                            return [obj1 compare:obj2];
//                          }];
//  int match = 0;
//  if([sortedArray[findIndex] isEqual:searchObject]) match =1;
//  NSLog(@"e results %tu match %d",findIndex,match);
//  printf("e Time taken %f \n",CACurrentMediaTime()-clockTime0);
//  
//  
//  
//
//
//  
//   NSLog(@" ----- Search Speed Test ends ------");
//  
//  
//  
//
//  if(0){
//  [[ICG sharedICG] setup];
//
//  
//  NSString *tmp = [[ICG sharedICG] getNowDateString];
//  NSDictionary *dict = @{@"address":@"https://www.swingprofile.com",@"title":tmp};
////  [[ICG sharedICG].arrayScan insertObject:dict atIndex:0];
//  [[ICG sharedICG].arrayScan addObject:dict ];
//  
//  NSLog(@"arrayScan %@",[ICG sharedICG].arrayScan);
//  NSLog(@"latestUpdatedAt %f",[ICG sharedICG].latestUpdatedAt);
//  
//    [[ICG sharedICG] saveUserDefaults : @"scan,code"];
//  }
}
- (IBAction)buttonUT07:(id)sender {
  [[ICG sharedICG] resetUserDefaults];
  NSLog(@"arrayCode: %@",[ICG sharedICG].arrayCode);
}
- (IBAction)buttonUT08:(id)sender {
  [[ICG sharedICG] loadUserDefaults];
  NSLog(@"arrayCode: %@",[ICG sharedICG].arrayCode);
}
- (IBAction)buttonUT09:(id)sender {
  [[ICG sharedICG] saveUserDefaults : @"scan,code"];
  NSLog(@"arrayCode: %@",[ICG sharedICG].arrayCode);
}
- (IBAction)buttonUT10:(id)sender {
  for(int k=0;k<LK.nCode;k++){
    unsigned long long int decMax = LK.code[k];
    int foundIndex = myBinarySearch(decMax);
    printf("Code %llu found at index %d \n",decMax,foundIndex);
    for(int i=0;i<LK.nCode;i++){
      printf("(%d) %llu\n",i,LK.code[i]);
    }
    
    // base64 image conversion to uiimage.
    for(int i=0;i<LK.nCode;i++){
      Code *code = [ICG sharedICG].arrayCode[i];
      if(code.Thumbnail){
        printf("============ code %d\n",i);
        UIImage *thumbnail = nil;
        NSData *decoded = [[NSData alloc] initWithBase64EncodedData:code.Thumbnail options:NSDataBase64DecodingIgnoreUnknownCharacters];
        if(decoded.length>0){
          thumbnail = [UIImage imageWithData:decoded];
        }
        printf("");
       }
    }
  }
}

  
- (IBAction)buttonUT20:(id)sender {
  // ----- Visit Webpage -
  ICWebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
//  wvc.websiteAddress  = code.Url;
  wvc.websiteAddress  = @"http://www.nzherald.co.nz";
  
  wvc.websiteTitle    = @"code desc";
  wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
  [self.navigationController pushViewController:wvc animated:YES];
}

- (IBAction)buttonUT21:(id)sender {
  UIImage *image      = [UIImage imageNamed:@"test.jpg"];
  
  UIImage *mediumImage       = [image thumbnailImage:280 transparentBorder:0 cornerRadius:0 interpolationQuality:kCGInterpolationHigh];
  
  _iv01.image = mediumImage;
}

- (IBAction)button30:(id)sender {
  NSString *url = @"https://4oyj.app.link/bKZGo2L0OA";
  
  if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:url]]) {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(openURL:options:completionHandler:)]) {
      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url] options:@{} completionHandler:nil];
    } else {
      [[UIApplication sharedApplication] openURL:url];
    }
    
  } else {
    NSLog(@"Cannot open URL: %@", url);
  }
}

- (IBAction)buttonUT33:(id)sender {
  // Create heatmap
  float Im2[10000];
  int k=0;
  for(int i=0;i<100;i++){
    for(int j=0;j<100;j++){
      Im2[k] = (i%10>0) * (j%10>0) * (i+j);
      k++;
    }
  }
  
  UIImage *im = [ICG heatmap:Im2 setWidth:100 setHeight:100 setMinValue:0 setMaxValue:100];
  
  [_iv01 setImage:im];
  
}


- (IBAction)buttonUT34:(id)sender {
  // Reachability test
  if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) {
    NSLog(@"NotReachable");
  } else {
    NSLog(@"Reachable");
  }
}





@end
