//
//  ICG.h
//  SquareCam 
//
//  Created by Eden Choi on 14/09/16.
//
//
#import "Code.h"
@interface ICG : NSObject

@property (strong, nonatomic) NSMutableArray *arrayScan;
@property (strong, nonatomic) NSMutableArray *arrayCode;
@property (strong, nonatomic) NSMutableArray *arrayAttrib;
@property (assign, nonatomic) double latestUpdatedAt;
@property (strong, nonatomic) NSString *clientId;
@property (assign, nonatomic) int showPlot;
@property (assign, nonatomic) int testMode;
@property (assign, nonatomic) int noForceFocus;
@property (assign, nonatomic) int scaleDown;
@property (strong, nonatomic) NSString *fScan;
@property (strong, nonatomic) NSString *fCode;
@property (strong, nonatomic) NSString *fSettings;
@property (strong, nonatomic) NSDictionary *settings;

// TC
@property (assign, nonatomic) int tc_code;
+ (ICG *)sharedICG;

- (void) loadData;
- (void) saveData;
- (void) setup;
- (void) loadUserDefaults;
- (void) saveUserDefaults : (NSString *) option;
- (void) updateArrayCode : (Code *) code ;
- (void) resetUserDefaults ;
- (void) updateLookupCode;
- (void) downloadCodesFromCloud ;
- (NSString *) getNowDateString;

+ (UIImage *) heatmap : (float *) src
             setWidth : (int) width
            setHeight : (int) height
          setMinValue : (float) minValue
          setMaxValue : (float) maxValue;
+ (UIImage *) processFrameToImage : (unsigned char*) baseAddress
                         setWidth : (int) width
                        setHeight : (int) height
                   setBytesPerRow : (int) bytesPerRow
                    setImageColor : (BOOL) imageColor;
@end
