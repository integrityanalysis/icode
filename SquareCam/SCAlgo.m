//
//  SCAlgo.m
//  SquareCam 
//
//  Created by Zeke Chan on 27/05/16.
//
//

#import "SCAlgo.h"
#import <Endian.h>
//#include "engine.h"
#include "libyuv.h"
#include "Constants.h"
#include "ic_engine.h"
#include "tc_engine.h"
#include "TCAlgo.h"
#include "ICG.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Accelerate/Accelerate.h>
#import <ZXingObjC/ZXingObjC.h>

#define ibs 31961096 // imageByteSize
#define sigmoid(x,a,c) 1.0f/(1.0f+exp(-a*(x-c)))
#define clamp(a) (a>255?255:(a<0?0:a))

@interface SCAlgo ()
//@property (strong, nonatomic) SPOverlay *overlay;
//@property (assign, nonatomic) int numAutoCaptureInSession;
@end

@implementation SCAlgo

// ###########################################################################
// ###########################################################################
unsigned char im0[SQPX], im1[SQPX];


- (void) getFrameRate{
  static int counter = 0;
  static double lastClockTime = 0, camframerate = 0;
  double currentClockTime;
  
  currentClockTime = CACurrentMediaTime();
  
  if((currentClockTime - lastClockTime)>2){
    camframerate  = counter / (currentClockTime - lastClockTime);
    
    counter =  0;
    lastClockTime = currentClockTime;
    printf("======== Camera frame rate %f \n",camframerate);
  }
  
  counter++;
}

// ********************************************************************************
// * Process Frame in uint8 array into a UIImage format
// ********************************************************************************
- (UIImage *) processFrameToImage : (unsigned char*) baseAddress
                         setWidth : (int) width
                        setHeight : (int) height
                   setBytesPerRow : (int) bytesPerRow
                    setImageColor : (BOOL) imageColor
{
  CGColorSpaceRef colorSpace;
  CGBitmapInfo bitmapInfo;
  
  if (imageColor == YES){
    colorSpace = CGColorSpaceCreateDeviceRGB();
    bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
  } else {
    colorSpace = CGColorSpaceCreateDeviceGray();
    bitmapInfo = kCGImageAlphaNone;
  }
  
  /*Create a CGImageRef from the CVImageBufferRef*/
  CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, bitmapInfo);
  CGImageRef newImage = CGBitmapContextCreateImage(newContext);
  
  /*We release some components*/
  CGContextRelease(newContext);
  CGColorSpaceRelease(colorSpace);
  
  
  /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly).
   Same thing as for the CALayer we are not in the main thread so ...*/
  UIImage *image = [UIImage imageWithCGImage:newImage
                                       scale:1.0f
                                 orientation:[SCAlgo exifOrientationToiOSOrientation:_exifOrientation]];
  
//  UIImage *image = [UIImage imageWithCGImage:newImage scale:0.5f orientation:[SCAlgo exifOrientationToiOSOrientation:UIImageOrientationUp]];
  //self.imagePtr = image;
  
//  UIImage *image = [UIImage imageWithCGImage:newImage scale:1.0f orientation:UIImageOrientationUp];

  
  /*We relase the CGImageRef*/
  CGImageRelease(newImage);
  return image;
}



// Convert an EXIF image orientation to an iOS one.
// reference see here: http://sylvana.net/jpegcrop/exif_orientation.html

+ (UIImageOrientation) exifOrientationToiOSOrientation:(int)exifOrientation {
  UIImageOrientation o = UIImageOrientationUp;
  switch (exifOrientation) {
    case 1: o = UIImageOrientationUp; break;
    case 3: o = UIImageOrientationDown; break;
    case 8: o = UIImageOrientationLeft; break;
    case 6: o = UIImageOrientationRight; break;
    case 2: o = UIImageOrientationUpMirrored; break;
    case 4: o = UIImageOrientationDownMirrored; break;
    case 5: o = UIImageOrientationLeftMirrored; break;
    case 7: o = UIImageOrientationRightMirrored; break; default: break;
  }
  return o;
}

- (UIImage *)makeUIImageFromCIImage:(CIImage *)ciImage {
//    	UIDeviceOrientation curDeviceOrientation  = [[UIDevice currentDevice] orientation];
  CIContext *context = [CIContext contextWithOptions:nil];
  CGImageRef cgImage = [context createCGImage:ciImage fromRect:[ciImage extent]];
  
//  UIImage* uiImage = [UIImage imageWithCGImage:cgImage];
//  UIImage* uiImage = [UIImage imageWithCGImage:cgImage scale:1.0 orientation:UIImageOrientationRight];
  UIImage* uiImage = [UIImage imageWithCGImage:cgImage
                                         scale:1.0f
                                   orientation:[SCAlgo exifOrientationToiOSOrientation:_exifOrientation]];
  CGImageRelease(cgImage);
  
  return uiImage;
}


#pragma mark - =============================
#pragma mark -   Scoring Mechanisms
#pragma mark - =============================


// Still image higher resolution than video list
// https://developer.apple.com/library/ios/technotes/tn2409/_index.html

double param[100];
unsigned char Imgray0[ibs],Im1[1280*720],
Im2[SQPX],Im3t[ibs],Img[ibs],ImgLast[ibs],ImgS[ibs],Img2[ibs],Img3[ibs],
ImaLast[ibs],Im2a[720*720];
float absFq[SQPX],Imf[SQPX],Imf2[SQPX];
unsigned char _tcImg1[SQ512],_tcImg2[SQ512];

- (int) captureOutput : (AVCaptureOutput *)captureOutput
didOutputSampleBuffer : (CMSampleBufferRef)sampleBuffer
       fromConnection : (AVCaptureConnection *)connection
             features : (NSArray *) features {
  int i,j,k,check;
  static unsigned int counter = 0;
  check = 1;
  
  // Initialization
  if(counter==0){
    initializeLookup();
  }
  [self getFrameRate];
  
  // ====================
  // Lock the image buffer, get address and info
  // https://stackoverflow.com/questions/8838481/kcvpixelformattype-420ypcbcr8biplanarfullrange-frame-to-uiimage-conversion
  CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
  CVPixelBufferLockBaseAddress(imageBuffer,0);
  
  size_t width = CVPixelBufferGetWidth(imageBuffer);
  size_t height = CVPixelBufferGetHeight(imageBuffer);
  
  uint8_t *baseAddress = CVPixelBufferGetBaseAddress(imageBuffer);
  CVPlanarPixelBufferInfo_YCbCrBiPlanar *bufferInfo = (CVPlanarPixelBufferInfo_YCbCrBiPlanar *)baseAddress;
  
  NSUInteger yOffset = EndianU32_BtoN(bufferInfo->componentInfoY.offset);
  NSUInteger yPitch  = EndianU32_BtoN(bufferInfo->componentInfoY.rowBytes);
  
  NSUInteger cbCrOffset = EndianU32_BtoN(bufferInfo->componentInfoCbCr.offset);
  NSUInteger cbCrPitch  = EndianU32_BtoN(bufferInfo->componentInfoCbCr.rowBytes);
  
  if(counter%300==0){
    // YCbCr720: width 1280, height 720, yOffset 64, yPitch 1280, cbCrOffset 921664, cbCrPitch 1280
    printf("[%d] width %d, height %d, yOffset %d, yPitch %d, cbCrOffset %d, cbCrPitch %d \n",counter,width,height,yOffset,yPitch,cbCrOffset,cbCrPitch);
  }

  // Address of each Plane
  uint8_t *yBuffer    = baseAddress + yOffset;
  uint8_t *cbCrBuffer = baseAddress + cbCrOffset;
 
  int ystart = (int)(height-2*SQW)/2.0;
  int yend   = ystart + 2*SQW;
  int xstart = (int)(width-2*SQW)/2.0;
  int xend   = xstart + 2*SQW;
  int z=0;
  int imageMode = 0;// counter%2; //0-color,1-gray
  if(THINCODE==1) imageMode = 5;
  
  // ----- Color Mode
  if(imageMode==0){
    for(int y = ystart; y < yend; y+=2){
      uint8_t *cbCrBufferLine = &cbCrBuffer[(y >> 1) * cbCrPitch];
      for(int x = xstart; x < xend; x+=2){
        // from ITU-R BT.601, rounded to integers
        float cb = cbCrBufferLine[x & ~1] ; // was uint8 cb - seems quant.error
        float cr = cbCrBufferLine[x |  1] ;
        Imf[z] = .7071*(cb+cr-256.0); // removed mean
//        Imf[z] = .7071*(cb+cr); // removed mean

        if(check){
          if(y==height/2) printf("%d,%d,%d, ycbcr %d %2.0f %2.0f imf %2.0f\n",x,x & ~1,x | 1,y1,cb,cr,(cb+cr-256.0));
          Im1[z] = (uint8)(cb*.5 + cr*.5); // for plotting
        }
        z++;
      }
    }
//    if(check){
//      UIImage *uiImg1 = [self processFrameToImage:Im1 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
//    }
    
    if([ICG sharedICG].scaleDown){
      // A quick and dirty way to reduce 512 image to 256, then put 256 in the middle of blank 512 image.
      // Properly done, it should scale down 720 to 360, then put 360 in blank 512.
      // Image copying is not optimized either.
      // To check correct transfer, copy unsigned image rather than float image, then preview UIImage
      for(i=0;i<SQW/2;i++){
        for(j=0;j<SQW/2;j++){
          Imf2[i*SQW+j] = Imf[2*(i*SQW+j)]; // reduce by jumping every 4 pixels, copy to top quarter of Imf2
        }
      }
      for(i=0;i<SQW*SQW;i++) Imf[i]=0; // zero out Imf
      for(i=0;i<SQW/2;i++){
        int i2 = (SQW/4 + i)*SQW+SQW/4;
        for(j=0;j<SQW/2;j++){
          Imf[i2+j] = Imf2[i*SQW+j]; // copy 256 to centre of zero-out Imf
//          Im1[i*SQW+j] = Im2[i*SQW+j];
        }
      }
      
    }
//    UIImage *uiImg2 = [self processFrameToImage:Im1 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
//    NSLog(@"happy");
    
//    int ystart = (int)(height/2-SQW)/2.0;
//    int yend   = ystart + SQW;
//    int xstart = (int)(width/2-SQW)/2.0;
//    int xend   = xstart + SQW;
//
//    for(int y = ystart; y < yend; y++){
//      uint8_t *cbCrBufferLine = &cbCrBuffer[y * cbCrPitch];
//      for(int x = xstart; x < xend; x++){
//        // from ITU-R BT.601, rounded to integers
//        float cb = cbCrBufferLine[x*2] ; // was uint8 cb - seems quant.error
//        float cr = cbCrBufferLine[x*2+1] ;
//        Imf[z] = .7071*(cb+cr-256.0); // removed mean
//
//
//        if(check){
//          if(y==height/2) printf("%d,%d,%d, ycbcr %d %2.0f %2.0f imf %2.0f\n",x,x & ~1,x | 1,y1,cb,cr,(cb+cr-256.0));
//          Im1[z] = (uint8)(cb*.5 + cr*.5); // for plotting
//        }
//        z++;
//      }
//    }
//          UIImage *uiImg1 = [self processFrameToImage:Im1 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
//    printf("hello");
    
  }// imageMode==0 - color
  
  // ----- Gray Mode
  // Crop 512x512 from 1280x720, then scale down to 256x256
  if(imageMode==1){

    // a. Crop y-plane
    z = 0;
    for(int y = ystart; y < yend; y++){
      uint8_t *yBufferLine = &yBuffer[y * yPitch];
      for(int x = xstart; x < xend; x++) Img2[z++]=yBufferLine[x];
    }
    
    // b. Scale - not sure if optimal or not - may need improvement
    vImage_Buffer src  = { Img2, 2*SQW, 2*SQW, 2*SQW }; // original
    vImage_Buffer dest = { Im2,    SQW,   SQW,   SQW };
    vImage_Error *err  = vImageScale_Planar8(&src, &dest, Im3t, kvImageNoError);
    if(err){
      NSLog(@"vImage downscale error");
      return NO;
    }
    
    // c. Copy to Float
    for(i=0;i<SQW*SQW;i++) Imf[i] = Im2[i]; // Slow - needs improvement
    
    
    if(check){
      UIImage *uiImg2 = [self processFrameToImage:Im2 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
    }
  }
  
  if(imageMode==5){
    
//    int ystart = (int)(height-2*SQW)/2.0;
//    int yend   = ystart + 2*SQW;
//    int xstart = (int)(width-2*SQW)/2.0;
//    int xend   = xstart + 2*SQW;

    // a. Crop y-plane - 512x512
    z = 0;
    for(int y = ystart; y < yend; y++){
      uint8_t *yBufferLine = &yBuffer[y * yPitch];
      for(int x = xstart; x < xend; x++) Img3[z++]=yBufferLine[x];
    }
    
    // Image resizing if necessary
//    vImage_Buffer src  = { _g1, IMW, IMW, IMW };
//    vImage_Buffer dest = { _g2, IMW, IMW, IMW };
//    vImage_Error err = vImageScale_Planar8(&src, &dest, nil, kvImageHighQualityResampling);
    
    // Pixel repositioning to Rotate - efficient - must use 2 different buffers, otherwise overwrite.
    vImage_Buffer src  = { Img3, IMW, IMW, IMW };
    vImage_Buffer dest = { Img2, IMW, IMW, IMW };
    vImage_Error err = vImageRotate90_Planar8(&src, &dest, 3, 0, kvImageNoError); // 1-rotate 90, 3-rotate 270

    
    // b. Scale - not sure if optimal or not - may need improvement
//    vImage_Buffer src  = { Img2, 2*SQW, 2*SQW, 2*SQW }; // original
//    vImage_Buffer dest = { Im2,    SQW,   SQW,   SQW };
//    vImage_Error *err  = vImageScale_Planar8(&src, &dest, Im3t, kvImageNoError);
//    if(err){
//      NSLog(@"vImage downscale error");
//      return NO;
//    }
    
    // c. Copy to Float
//    for(i=0;i<2*2*SQW*SQW;i++) Imf[i] = Im2[i]; // Slow - needs improvement
    if(check){
      // Image is auto-rotated to be portrait always - n
      UIImage *uiImg2 = [self processFrameToImage:Img2 setWidth:2*SQW setHeight:2*SQW setBytesPerRow:2*SQW setImageColor:NO];
      NSLog(@"Check Image");
//      _latestCaptureImage = uiImg2;
    }
  }
  
//  // Unlock
//  CVPixelBufferUnlockBaseAddress( imageBuffer, 0 );
  // ----- iCode Engine -----
  int found = -1;
  if(1){
    if(THINCODE==0){
      // iCode Engine
      found = myEngine(Imf);
    }else{
        
      
      // Thin Code Engine - for now return 1 if found
      found = tc_engine0(Img2,_tcImg1,_tcImg2);
      if(found>0){
        [ICG sharedICG].tc_code = found;
//        found = 1; // for now, won't cra
      }
    }
  }
//  found=-1;
  
  /*
  
  uint8_t *baseAddress0 = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
  int bytesPerRow    = (int) CVPixelBufferGetBytesPerRow(imageBuffer);
  int height         = (int) CVPixelBufferGetHeight(imageBuffer);
  int imageByteSize  = (int) CVPixelBufferGetDataSize(imageBuffer);
  int width          = (int) CVPixelBufferGetWidth(imageBuffer);
  
  // ====================
  
  if(counter%300==0){
    printf("[%d] bytesPerRow %d, height %d, imageByteSize %d, width %d\n",counter,bytesPerRow,height,imageByteSize,width);
  }
  
  // Use this to get Y plane address
  //  unsigned char *Ybase = CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);
  //  int widthVid = 640, heightVid = 480;
  //  void CopyPlane(const uint8* src_y, int src_stride_y,
  //                 uint8* dst_y, int dst_stride_y,
  //                 int width, int height);
  //  CopyPlane(Ybase, 4, Im1, 1, 1280*4,720);
  //  for(i=0;i<1280*720;i++){
  //    Im1[i] = Ybase[i*4];
  //  }
  
  // ----- 1. Extract 512 Square image from the center of the 1280x720 image.
  // Use vImage to extract single channel, then place in float image.
  // gray image, 0,1,2 works. not 3.
  // 0 is blue channel! 1 is Green. 2 is Red;
  int chan = 1; // was 1. 2 also works. 3 doesn't work at all. I think it is 32BGRA so channel 0
  vImage_Buffer src  = { baseAddress0, height, width, bytesPerRow }; // original
  vImage_Buffer dest = { Im1, height, width, width };
  vImage_Error *err  = vImageExtractChannel_ARGB8888(&src, &dest, chan, kvImageNoError);
  if(err){
    NSLog(@"vImage error");
    return NO;
  }
  
  // Cut the square out from the middle
  k = width*(height-SQW)/2;
  for(i=0;i<SQW;i++){
    // uint8 to Float conversion
    vDSP_vfltu8(&Im1[k + i*width + (width-SQW)/2],1,&Imf[i*SQW],1,SQW);
  }
  
  // Unlock
  CVPixelBufferUnlockBaseAddress( imageBuffer, 0 );
  
  // For checking the input floating point image is correct
  //  for(i=0;i<SQPX;i++) Im2[i] = Imf[i]; // copy back to uint8
  //  UIImage *uiImg1 = [self processFrameToImage:Im2 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
  
  
  //  UIImage *uiImg1 = [self processFrameToImage:Im2 setWidth:SQW setHeight:SQW setBytesPerRow:SQW setImageColor:NO];
  
  //  printimagef(Imf, SQW, SQW, 20, 10, 1, 128);
  
  
  //  printf("hello\n");
  
  //  getFFT(Imf,absFq);
  
  //  getFilteredFFT(Imf,absF);
  //  test01();
  //  test_Image();
  
  // Old search code method
  //  int found=0;
  //  for(i=0;i<1;i++){
  //  unsigned long long int dec = myEngine(Imf);
  //  _code = dec;
  //    if(db_testA){
  //      found = dec>0;
  //    }else{
  //      found = searchBase(dec);
  //    }
  //  }
  
  // ----- iCode -----
  int found = -1;
  if(1){
    found = myEngine(Imf);
  }
  
  // ----- Bar code -----
  if(counter%5==0){
    // Cut the square out from the middle
    // Row dominant - next pixel is left then down)
    // height=720, width=1280
    k = (width-height)/2;
    for(i=0;i<height;i++){
      // uint8 to Float conversion
      memcpy(&Im2a[i*height],&Im1[k + i*width],height);
    }
    
    // https://github.com/TheLevelUp/ZXingObjC
    UIImage *im = [self processFrameToImage:Im2a setWidth:height setHeight:height setBytesPerRow:height setImageColor:NO];
    UIImage *imRot = [self rotateUIImage:im clockwise:1]; // rotate clockwise to upright for line bar code
    //    UIImage *im = [UIImage imageNamed:@"barcode-13.png"];
    
    CGImageRef imageToDecode  = imRot.CGImage;  // Given a CGImage in which we are looking for barcodes
    ZXLuminanceSource *source = [[ZXCGImageLuminanceSource alloc] initWithCGImage:imageToDecode];
    ZXBinaryBitmap *bitmap    = [ZXBinaryBitmap binaryBitmapWithBinarizer:[ZXHybridBinarizer binarizerWithSource:source]];
    
    NSError *error = nil;
    
    // There are a number of hints we can give to the reader, including
    // possible formats, allowed lengths, and the string encoding.
    ZXDecodeHints *hints = [ZXDecodeHints hints];
    
    ZXMultiFormatReader *reader = [ZXMultiFormatReader reader];
    ZXResult *result = [reader decode:bitmap
                                hints:hints
                                error:&error];
    if (result) {
      // The coded result as a string. The raw data can be accessed with
      // result.rawBytes and result.length.
      NSString *contents = result.text;
      
      // The barcode format, such as a QR code or UPC-A
      ZXBarcodeFormat format = result.barcodeFormat;
      
      _latestBarcode = @{@"format":@(result.barcodeFormat),@"contents":result.text};
      
      NSLog(@"Zxing format %d \nContents %@", format, contents);
      found = -2;
    } else {
      // Use error to determine why we didn't get a result, such as a barcode
      // not being found, an invalid checksum, or a format inconsistency.
      NSLog(@"Zxing error %@",error);
    }
  }
  
  
  // If valid code, save captured UIImage
  if(found>=0 || found==-2){
    // Extract UIImage from baseAddress0
    _latestCaptureImage = [self processFrameToImage:baseAddress0
                                           setWidth:width
                                          setHeight:height
                                     setBytesPerRow:bytesPerRow
                                      setImageColor:YES];
  }
  
  */
  
  // If valid code, save captured UIImage
  // Speicial YUV to RGB then to UIImage 512x512
  // https://stackoverflow.com/questions/8838481/kcvpixelformattype-420ypcbcr8biplanarfullrange-frame-to-uiimage-conversion
  if(found>=0 || found==-2){
    int bytesPerPixel = 4;
    uint8_t *rgbBuffer = malloc(4*SQW*SQW*bytesPerPixel);
    z=0;
    for(int y = ystart; y < yend; y++) {
      uint8_t *yBufferLine = &yBuffer[y * yPitch];
      uint8_t *cbCrBufferLine = &cbCrBuffer[(y >> 1) * cbCrPitch];
      
      for(int x = xstart; x < xend; x++) {
        int16_t y = yBufferLine[x];
        int16_t cb = cbCrBufferLine[x & ~1] - 128;
        int16_t cr = cbCrBufferLine[x | 1] - 128;
        
        int16_t r = (int16_t)roundf( y + cr *  1.4 );
        int16_t g = (int16_t)roundf( y + cb * -0.343 + cr * -0.711 );
        int16_t b = (int16_t)roundf( y + cb *  1.765);
        
        rgbBuffer[z++] = 0xff;
        rgbBuffer[z++] = clamp(b);
        rgbBuffer[z++] = clamp(g);
        rgbBuffer[z++] = clamp(r);
      }
    }
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(rgbBuffer, 2*SQW, 2*SQW, 8, 2*SQW * bytesPerPixel, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipLast);
    CGImageRef quartzImage = CGBitmapContextCreateImage(context);
    _latestCaptureImage = [UIImage imageWithCGImage:quartzImage
                                         scale:1.0f
                                   orientation:[SCAlgo exifOrientationToiOSOrientation:_exifOrientation]];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    CGImageRelease(quartzImage);
    free(rgbBuffer);
  }
  
  CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
  
  
  
  counter++;
  return found;

}

- (UIImage*)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise
{
  CGSize size = sourceImage.size;
  UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
  [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0 orientation:clockwise ? UIImageOrientationRight : UIImageOrientationLeft] drawInRect:CGRectMake(0,0,size.height ,size.width)];
  UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  return newImage;
}



@end
