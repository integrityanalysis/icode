//
//  TCAlgo.m
//  SquareCam
//
//  Created by Zeke Chan on 9/01/20.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#include <Accelerate/Accelerate.h>

#import "TCAlgo.h"
#import "Constants.h"

#include "tc_engine.h"
#include "ic_engine.h"
#include "fast.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "nr.h"
#include "nrutil.h"
#include "math.h"
//#include <Accelerate/Accelerate.h>

//#define IMW 512  // Image width
//#define SQ512 512*512 // Square 512


@interface TCAlgo()

@property (assign, nonatomic) int exifOrientation;

@end

@implementation TCAlgo


// ********************************************************************************
// * Process Frame in uint8 array into a UIImage format
// ********************************************************************************
+ (UIImage *) processFrameToImage : (unsigned char*) baseAddress
                         setWidth : (int) width
                        setHeight : (int) height
                   setBytesPerRow : (int) bytesPerRow
                    setImageColor : (BOOL) imageColor
{
  CGColorSpaceRef colorSpace;
  CGBitmapInfo bitmapInfo;
  
  if (imageColor == YES){
    colorSpace = CGColorSpaceCreateDeviceRGB();
    bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
  } else {
    colorSpace = CGColorSpaceCreateDeviceGray();
    bitmapInfo = kCGImageAlphaNone;
  }
  
  /*Create a CGImageRef from the CVImageBufferRef*/
  CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, bitmapInfo);
  CGImageRef newImage = CGBitmapContextCreateImage(newContext);
  
  /*We release some components*/
  CGContextRelease(newContext);
  CGColorSpaceRelease(colorSpace);
  
  
  /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly).
   Same thing as for the CALayer we are not in the main thread so ...*/
  UIImage *image = [UIImage imageWithCGImage:newImage
                                       scale:1.0f
                                 orientation:nil];// for now orientation not important [TCAlgo exifOrientationToiOSOrientation:_exifOrientation]];
  
  /*We relase the CGImageRef*/
  CGImageRelease(newImage);
  return image;
}


void loadCSVFile(char filename[], float tmpf[]){
  // Read CSV file from bundle
  int i,j;
  NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithUTF8String:filename] ofType:@"csv"];
  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
  if (myData) {
    // Load CSV file elements, line by line
    j = 0;
    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for (NSString* line in allLines) {
      NSArray *elements = [line componentsSeparatedByString:@","];
      int ne = [elements count];
      for(i=0;i<ne;i++)  tmpf[j++] = [elements[i] floatValue];
    }
    NSLog(@"Load CSV: file found. (%d) elements loaded.",j);
  }
}





//void find_components() {
//  int component = 0;
//  for (int i = 0; i < row_count; ++i)
//    for (int j = 0; j < col_count; ++j)
//      if (!label[i+j*IMW] && m[i+j*IMW]) dfs(i, j, ++component);
//}
// -----------------------------------------------------
void imageConv(unsigned char g1[],unsigned char bin[]){
  //  float *ellipseParam = (float *)malloc(3 *sizeof(float));
  //   free(ellipseParam);
  int i;
  float *t1 = malloc(SQ512 * sizeof(float));
  float *t2 = malloc(SQ512 * sizeof(float));
  float *t3 = malloc(SQ512 * sizeof(float));
  
  for(i=0;i<SQ512;i++) t1[i]=g1[i];
//  float *buf2 = (float *)malloc(3 *sizeof(SQ512));
//    free(ellipseParam);
  // ======================================================================
  // Image Convolutions
  // 2 Important parameters: (1)edge detetion threshold, (2)fattening 3x3 or 5x5
  int thres_edge = -70; // edge detection parameter
  int nfat       = 5;
  
  // Combined 3x3 Gaussian and Edge Detection
  float B[25]={
     -0.011344,-0.095163,-0.106507,-0.095163,-0.011344,
     -0.095163,-0.696236,-0.139117,-0.696236,-0.095163,
     -0.106507,-0.139117,4.574123,-0.139117,-0.106507,
     -0.095163,-0.696236,-0.139117,-0.696236,-0.095163,
     -0.011344,-0.095163,-0.106507,-0.095163,-0.011344
   };
   vDSP_f5x5(t1, IMW, IMW, B, t2);
  
  // Thresholding for edges
  for(i=0;i<SQ512;i++) t2[i]=(t2[i]<thres_edge);
  
  // Fattening
  if(nfat==5){
    // Fattens by 5x5 disk
    float D[25]={
      0.000000,0.017016,0.038115,0.017016,0.000000,
      0.017016,0.078381,0.079577,0.078381,0.017016,
      0.038115,0.079577,0.079577,0.079577,0.038115,
      0.017016,0.078381,0.079577,0.078381,0.017016,
      0.000000,0.017016,0.038115,0.017016,0.000000
    };
    vDSP_f5x5(t2, IMW, IMW, D, t3);
  }else{
    // Fatten by 3x3 Disk
    float D[9]={
      0.025079,0.145344,0.025079,
      0.145344,0.318310,0.145344,
      0.025079,0.145344,0.025079,
    };
    vDSP_f3x3(t2, IMW, IMW, D, t3);
  }
  
   
  // Binarize
  for(i=0;i<SQ512;i++) bin[i]=128*(t3[i]>0);
  
  free(t1);
  free(t2);
  free(t3);
  
}

// From YACCLAB - labeling_distefano_1999
// Large arrays must declare outside so not crash
//#define UPPER_BOUND_8_CONNECTIVITY ((size_t)((img_.rows + 1) / 2) * (size_t)((img_.cols + 1) / 2) + 1)
#define UPPER_BOUND_8_CONNECTIVITY (IMW+1)/2 * ((IMW+1)/2 +1)
int a_class[UPPER_BOUND_8_CONNECTIVITY];
bool a_single[UPPER_BOUND_8_CONNECTIVITY];
int a_renum[UPPER_BOUND_8_CONNECTIVITY];
int img_labels[SQ512];
void conl_DiStefano(unsigned char g[], float c[][3],int *count, int thres_min,int thres_max){

//    img_labels_ = cv::Mat1i(img_.size());
//  int img_labels[SQ512];
//    int i_new_label(0);
  int i_new_label;

    // p q r      p
    // s x      q x
    // lp,lq,lx: labels assigned to p,q,x

    // First scan
//    int *a_class = new int[UPPER_BOUND_8_CONNECTIVITY];
//    bool *a_single = new bool[UPPER_BOUND_8_CONNECTIVITY];
//    int *a_renum = new int[UPPER_BOUND_8_CONNECTIVITY];
  
//  int a_class[UPPER_BOUND_8_CONNECTIVITY];
//  bool a_single[UPPER_BOUND_8_CONNECTIVITY];
//  int a_renum[UPPER_BOUND_8_CONNECTIVITY];
  
  for (int y = 0; y < IMW; y++) {

      // Get rows pointer
//        const unsigned char* const img_row = img_.ptr<unsigned char>(y);
//        unsigned int* const img_labels_row = img_labels_.ptr<unsigned int>(y);
//        unsigned int* const img_labels_row_prev = (unsigned int *)(((char *)img_labels_row) - img_labels_.step.p[0]);
    unsigned char *img_row = &g[y*IMW];
    int *img_labels_row = &img_labels[y*IMW];
    int *img_labels_row_prev = &img_labels[y*IMW-IMW];
    
      for (int x = 0; x < IMW; x++) {
          if (img_row[x]) {

//                int lp(0), lq(0), lr(0), ls(0), lx(0); // lMin(INT_MAX);
              int lp, lq, lr, ls, lx; // lMin(INT_MAX);
              if (y > 0) {
                  if (x > 0)
                      lp = img_labels_row_prev[x - 1];
                  lq = img_labels_row_prev[x];
                  if (x < IMW - 1)
                      lr = img_labels_row_prev[x + 1];
              }
              if (x > 0)
                  ls = img_labels_row[x - 1];

              // If everything around is background
              if (lp == 0 && lq == 0 && lr == 0 && ls == 0) {
                  lx = ++i_new_label;
                  a_class[lx] = lx;
                  a_single[lx] = true;
              }
              else {
                  // p
                  lx = lp;
                  // q
                  if (lx == 0)
                      lx = lq;
                  // r
                  if (lx > 0) {
                      if (lr > 0 && a_class[lx] != a_class[lr]) {
                          if (a_single[a_class[lx]]) {
                              a_class[lx] = a_class[lr];
                              a_single[a_class[lr]] = false;
                          }
                          else if (a_single[a_class[lr]]) {
                              a_class[lr] = a_class[lx];
                              a_single[a_class[lx]] = false;
                          }
                          else {
                              int i_class = a_class[lr];
                              for (int k = 1; k <= i_new_label; k++) {
                                  if (a_class[k] == i_class) {
                                      a_class[k] = a_class[lx];
                                  }
                              }
                          }
                      }
                  }
                  else
                      lx = lr;
                  // s
                  if (lx > 0) {
                      if (ls > 0 && a_class[lx] != a_class[ls]) {
                          if (a_single[a_class[lx]]) {
                              a_class[lx] = a_class[ls];
                              a_single[a_class[ls]] = false;
                          }
                          else if (a_single[a_class[ls]]) {
                              a_class[ls] = a_class[lx];
                              a_single[a_class[lx]] = false;
                          }
                          else {
                              int i_class = a_class[ls];
                              for (int k = 1; k <= i_new_label; k++) {
                                  if (a_class[k] == i_class) {
                                      a_class[k] = a_class[lx];
                                  }
                              }
                          }
                      }
                  }
                  else
                      lx = ls;
              }

              img_labels_row[x] = lx;
          }
          else
              img_labels_row[x] = 0;
      }
  }

  // Renumbering of labels
  int n_labels_ = 0;
  for (int k = 1; k <= i_new_label; k++) {
      if (a_class[k] == k) {
          n_labels_++;
          a_renum[k] = n_labels_;
      }
  }
  for (int k = 1; k <= i_new_label; k++)
      a_class[k] = a_renum[a_class[k]];

  // Second scan
  for (int y = 0; y < IMW; y++) {

      // Get rows pointer
//        unsigned int* const img_labels_row = img_labels_.ptr<unsigned int>(y);
      int *img_labels_row = &img_labels[y*IMW];
      for (int x = 0; x < IMW; x++) {
          int iLabel = img_labels_row[x];
          if (iLabel > 0)
              img_labels_row[x] = a_class[iLabel];
      }
  }

  n_labels_++; // To count also background
  
  
  // For text drawing of labels - check only
  if(0){
    int s=10;
    for (int y = 0; y < IMW; y+=s) {
      for (int x = 0; x < IMW; x+=s) {
        printf("%3d ",img_labels[y*IMW+x]);
      }
      printf("\n");
    }
  }

//    delete[] a_class;
//    delete[] a_single;
//    delete[] a_renum;
  // =======================================================
   // Count size of each cluster
    for (int i = 0; i < n_labels_; i++) { // does not need to go sq512 loop
      _labelx[i]     = 0;
      _labely[i]     = 0;
      _labelCount[i] = 0; // initialize
    }
  
    for (int y = 0; y < IMW; y++)
      for (int x = 0; x < IMW; x++){
        int j = img_labels[x+y*IMW]; // linear index

        _labelx[j] += x;
        _labely[j] += y;
        _labelCount[j]++;
      }

    int ncentre = 0;
  //  float centrex[1000],centrey[1000];
    for (int i = 0; i < n_labels_; i++){
      if(_labelCount[i]>=thres_min && _labelCount[i]<=thres_max ){
        c[ncentre][0] = _labelx[i]/_labelCount[i];
        c[ncentre][1] = _labely[i]/_labelCount[i];
        c[ncentre][2] = _labelCount[i];

  //      printf("%d xy %f %f \n",ncentre,centrex[ncentre],centrey[ncentre]);
        ncentre++;

      }
    }
    *count = ncentre;
  
}


int tc_connected(unsigned char g1[],unsigned char g2[],unsigned char g3[]){

//  int *g2   = malloc(SQ512 * sizeof(int));   // too large for device must use malloc - cannot use label[SQ512]
  
  int i,j,k,m,n;
  int foundIndex = -1;

  double clockTime0 = CACurrentMediaTime();
  
  // Original gray image (float[]), convolved image (float[] not used), binarized image;
  imageConv(g1,g2);
  
  printf("Convolutions: Time taken %f \n",CACurrentMediaTime()-clockTime0);

  // ===================================================================
  // **** Connected Component labelling (Working, but Inefficient! 4.4ms)
  clockTime0 = CACurrentMediaTime();

  int thres_size_min = 20;
  int thres_size_max = 1000;
  float centre[1000][3]; // maximum 10000 centres
  int ncentre = 0;
//  test(g2, centre, &ncentre, thres_size_min,thres_size_max);
  conl_DiStefano(g2, centre, &ncentre, thres_size_min,thres_size_max);
   
  printf("conl_DiStefano ConnectedLabelling: Time taken %f \n",CACurrentMediaTime()-clockTime0);
  
  
  ncentre = MIN(ncentre,200); // no more than 200
  if(ncentre<5) return foundIndex;
  
  if(1){//}(TC_VERBOSE){
    for(i=0;i<ncentre;i++){
      // Draw white dot at centres on image
      int x = centre[i][0]; // rounded down to int
      int y = centre[i][1]; // rounded down to int
      g2[x  + y   *IMW] = 255;
      g2[x+1+ y   *IMW] = 255;
      g2[x+1+(y+1)*IMW] = 255;
      g2[x  +(y+1)*IMW] = 255;
      
//      printf("---- Centre %d/%d (x,y,n): %2.1f %2.1f %2.1f\n",i,ncentre,centre[i][0],centre[i][1],centre[i][2]);
    }
  }
  return 0;
}

// ===================================================================
// **** Connected Component labelling (Working, but Inefficient! 4.4ms)
// Simple but inefficient 8-Connected Component Labelling
// https://stackoverflow.com/questions/14465297/connected-component-labeling-implementation
// direction vectors
const int _dx[] = {+1, 0, -1, 0, +1, +1, -1, -1};
const int _dy[] = {0, +1, 0, -1, -1, +1, -1, +1};
int _label[SQ512],_labelCount[SQ512];
float _labelx[SQ512],_labely[SQ512];
float _centre[1000][3];
//int _ncentre;
unsigned char __g1[SQ512];

void doUnion(int a, int b){
    // get the root component of a and b, and set the one's parent to the other
    while (_label[a] != a)
        a = _label[a];
    while (_label[b] != b)
        b = _label[b];
    _label[b] = a;
}

void unionCoords(int x, int y, int x2, int y2){
//    if (y2 < IMW && x2 < IMW && __g1[x+y*IMW] && __g1[x2+y2*IMW])
//        doUnion(x + y*IMW, x2 + y2*IMW);
  if (y2>=0 && x2>=0 && y2 < IMW && x2 < IMW && __g1[x+y*IMW] && __g1[x2+y2*IMW]){
    int a=x + y*IMW;
    int b=x2 + y2*IMW;
    // get the root component of a and b, and set the one's parent to the other
    while (_label[a] != a)
        a = _label[a];
    while (_label[b] != b)
        b = _label[b];
    _label[b] = a;
  }

}

int unionFind(){
  int i,j;

  
  // Union find
  for (int i = 0; i < SQ512; i++) _label[i] = i; // initialize each pixel its own cluster
  for (int x = 0; x < IMW; x++)
    for (int y = 0; y < IMW; y++) {
      unionCoords(x, y, x+1, y);
      unionCoords(x, y, x,   y+1);
      unionCoords(x, y, x+1, y+1);
      unionCoords(x, y, x-1, y  );
      unionCoords(x, y, x,   y-1);
      unionCoords(x, y, x-1, y-1);
      unionCoords(x, y, x+1, y-1);
      unionCoords(x, y, x-1, y+1);
    }
  
  // Count size of each cluster
  for (int i = 0; i < SQ512; i++) {
    _labelx[i]     = 0;
    _labely[i]     = 0;
    _labelCount[i] = 0; // initialize
  }
  for (int y = 0; y < IMW; y++)
    for (int x = 0; x < IMW; x++){
      j = _label[x+y*IMW]; // linear index

      _labelx[j] += x;
      _labely[j] += y;
      _labelCount[j]++;
    }

  int ncentre = 0;
  float centrex[1000],centrey[1000];
  for (i = 0; i < SQ512; i++){
    if(_labelCount[i]>=9){
      centrex[ncentre] = _labelx[i]/_labelCount[i];
      centrey[ncentre] = _labely[i]/_labelCount[i];

//      printf("%d xy %f %f \n",ncentre,centrex[ncentre],centrey[ncentre]);
      ncentre++;

    }
  }
  
  
//  for(i=200;i<300;i++){
//    for(j=200;j<300;j++){
//      printf("%d ", _label[j+i*IMW]);
////      printf("%d ", __g1[j+i*IMW]);
//    }
//    printf("\n");
//  }
  

  
  
  printf("hello\n");

//    // print the array
//    for (int x = 0; x < IMW; x++)
//    {
//        for (int y = 0; y < IMW; y++)
//        {
//            if (!__g1[x+y*IMW] == 0)
//            {
//              printf(" ");
//                continue;
//            }else{
//              printf("%d",_label[x+y*IMW]);
//            }
//        }
//        printf("\n");
//    }
  return 0;
}
void dfs2(int x, int y, int current_label) {
  // Important that for every new image, last_label is initialized
  static int last_label = -99;
  if(current_label==-1) last_label= -99;
  static int npix = 0;
  // This could lead to array OB
//  if (x < 0 || x == IMW) return; // out of bounds
//  if (y < 0 || y == IMW) return; // out of bounds
  
  // Safer boundary
  if (x <= 0 || x >= IMW-1) return; // out of bounds
  if (y <= 0 || y >= IMW-1) return; // out of bounds
  if (_label[x+y*IMW] || !__g1[x+y*IMW]) return; // already labeled or not marked with 1 in m
//  printf("npix %d current_label==last_label %d \n",npix,current_label==last_label);
//  if (current_label==last_label && npix>10000) return;

  // mark the current cell
  _label[x+y*IMW] = current_label;

  // --------- Zeke - summarize centre info
  if(current_label!=last_label){
    _centre[current_label][0] = x;
    _centre[current_label][1] = y;
    _centre[current_label][2] = 1;
    npix = 1;
  }else{
    _centre[current_label][0] += x;
    _centre[current_label][1] += y;
    _centre[current_label][2] += 1;
    npix++;
  }
  last_label = current_label;
  // ----------

  // recursively mark the neighbors
  for (int direction = 0; direction < 8; ++direction)
    dfs2(x + _dx[direction], y + _dy[direction], current_label);
}
//void dfs(int x, int y, int current_label, unsigned char m[], int label[], float centre[][3]) {
//  // Important that for every new image, last_label is initialized
//  static int last_label = -99;
//  if(current_label==-1) last_label= -99;
//  // This could lead to array OB
////  if (x < 0 || x == IMW) return; // out of bounds
////  if (y < 0 || y == IMW) return; // out of bounds
//
//  // Safer boundary
//  if (x <= 0 || x >= IMW-1) return; // out of bounds
//  if (y <= 0 || y >= IMW-1) return; // out of bounds
//  if (label[x+y*IMW] || !m[x+y*IMW]) return; // already labeled or not marked with 1 in m
//
//  // mark the current cell
//  label[x+y*IMW] = current_label;
//
//  // --------- Zeke - summarize centre info
//  if(current_label!=last_label){
//    centre[current_label][0] = x;
//    centre[current_label][1] = y;
//    centre[current_label][2] = 1;
//  }else{
//    centre[current_label][0] += x;
//    centre[current_label][1] += y;
//    centre[current_label][2] += 1;
//  }
//  last_label = current_label;
//  // ----------
//
//  // recursively mark the neighbors
//  for (int direction = 0; direction < 8; ++direction)
//    dfs(x + _dx[direction], y + _dy[direction], current_label,m,label,centre);
//}

void test(unsigned char in[], float c[][3],int *count, int thres_min,int thres_max){
}

void connectedLabelling(unsigned char g[], float c[][3],int *count, int thres_min,int thres_max){
  int i,j;
  for(i=0;i<SQ512;i++)  __g1[i]=g[i];
    
  // Union find
  for (int i = 0; i < SQ512; i++) _label[i] = i; // initialize each pixel its own cluster
  for (int x = 0; x < IMW; x++)
    for (int y = 0; y < IMW; y++) {
      unionCoords(x, y, x+1, y);
      unionCoords(x, y, x,   y+1);
      unionCoords(x, y, x+1, y+1);
      unionCoords(x, y, x-1, y  );
      unionCoords(x, y, x,   y-1);
      unionCoords(x, y, x-1, y-1);
      unionCoords(x, y, x+1, y-1);
      unionCoords(x, y, x-1, y+1);
    }
  
  // Count size of each cluster
  for (int i = 0; i < SQ512; i++) {
    _labelx[i]     = 0;
    _labely[i]     = 0;
    _labelCount[i] = 0; // initialize
  }
  for (int y = 0; y < IMW; y++)
    for (int x = 0; x < IMW; x++){
      j = _label[x+y*IMW]; // linear index

      _labelx[j] += x;
      _labely[j] += y;
      _labelCount[j]++;
    }

  int ncentre = 0;
//  float centrex[1000],centrey[1000];
  for (i = 0; i < SQ512; i++){
    if(_labelCount[i]>=thres_min && _labelCount[i]<=thres_max ){
      c[ncentre][0] = _labelx[i]/_labelCount[i];
      c[ncentre][1] = _labely[i]/_labelCount[i];
      c[ncentre][2] = _labelCount[i];

//      printf("%d xy %f %f \n",ncentre,centrex[ncentre],centrey[ncentre]);
      ncentre++;

    }
  }
  *count = ncentre;
}


void connectedLabelling_old(unsigned char g[], float c[][3],int *count, int thres_min,int thres_max){
  int i,j;
  float centre[1000][3];
  
  // too large for device must use malloc - cannot use label[SQ512]
  int *label = (int *) malloc(SQ512 * sizeof(int));
  for(i=0;i<SQ512;i++) label[i]=0; // must reset all points
  
  // Easy print bit map
//  for(j=0;j<IMW;j+=10){
//    for(i=0;i<IMW;i+=10){
//      printf("%s",g[i+j*IMW]==0?".":"#");
//    }
//    printf("\n");
//  }
    for(i=0;i<SQ512;i++) _label[i]=0; // must reset all points
  for(i=0;i<SQ512;i++)  __g1[i]=g[i];
  // 1.Get original centres - run dfs
//  dfs2(0, 0, -1); // Reset static variable last_label: set current_Label=-1
  int component = 0;
//  for (i = 0; i < IMW; ++i)
//    for (j = 0; j < IMW; ++j)
//      if (!_label[j+i*IMW] && g[j+i*IMW]) dfs2(j,i, ++component);
  unionFind();
//  return;

  if(component>=1000) printf("***** Problem! Num component more than 1000\n");

  // 2. Filter centres to only certain size
  // Centre array: start from [1] and ends on [component], following variable [component]. No [0].
  int nc = 0;
  for(i=1;i<=component;i++){
    _centre[i][0]/=_centre[i][2]; // Get mean x
    _centre[i][1]/=_centre[i][2]; // Get mean y

//    printf("Component %d (x,y,n): %f %f %f\n",i,centre[i][0],centre[i][1],centre[i][2]);
    
    // Extract centres exceeding threshold size
    // Should also remove large ones >1000 i think
    if(_centre[i][2]>=thres_min & _centre[i][2]<=thres_max){
      c[nc][0] = _centre[i][0];
      c[nc][1] = _centre[i][1];
      c[nc][2] = _centre[i][2];
      nc++;
    }
  }
  
  // Output number of eligible centres (nc) and centres (c)
  *count = nc;
  
  free(label);
}

// https://www.geeksforgeeks.org/represent-given-set-points-best-possible-straight-line/
// function to calculate m and c that best fit points
// represented by x[] and y[]
void linefit(float x[], float y[], int n, float *m, float *c)
{
    int i;
    float sum_x = 0, sum_y = 0, sum_xy = 0, sum_x2 = 0;
    for (i = 0; i < n; i++) {
        sum_x += x[i];
        sum_y += y[i];
        sum_xy += x[i] * y[i];
        sum_x2 += (x[i] * x[i]);
    }
  
    *m = (n * sum_xy - sum_x * sum_y) / (n * sum_x2 - (sum_x * sum_x));
    *c = (sum_y - *m * sum_x) / n;
}

void collinearSearch(float c[][3],int nc, int thres_proj, long M[], long CP2[], int *nCP2, int iCP2[], int jCP2[]){
  long CP[1000];
  int iCP[1000],jCP[1000];
  unsigned long rank[1000];
  
  int i,j,k;
  int nCP = 0;
  for(i=0;i<nc;i++){
    for(j=i+1;j<nc;j++){
      // Pre-calc n1 and n2 for speed up of about 50%
      M[i*nc + j]=-2;
      float n1 = (c[i][1]-c[j][1]);
      float n2 = (c[i][0]-c[j][0]);
      for(k=0;k<nc;k++){
  //        int d = (c[i][1]-c[j][1])*(c[i][0]-c[k][0]) - (c[i][1]-c[k][1])*(c[i][0]-c[j][0]); // original
        float d = n1*(c[i][0]-c[k][0]) - n2*(c[i][1]-c[k][1]); // use pre-calc n1 and n2 - faster
        if(fabs(d)<thres_proj) M[i*nc + j] += 1; //  fastest with fabs
  //        M[i][j] += ((d>0) ? d:-d)<thres_proj; // works ok but slower
      }
      if(M[i*nc + j] > 4){
        CP[ nCP ]  = M[i*nc + j];
        iCP[ nCP ] = i;
        jCP[ nCP ] = j;
        nCP++;
        
        nCP = MIN(nCP,900); // cause of crash
      }
    }// for j
  } // for i
  
  if(nCP<5) return;
  
  // ------------------------------------
  // Rank high collinear points
  // iindexx - unit offset so must change to zero offset by array-1
  // index is from [1,....len] so must do a[i]-1 to change to [0,...len-1]
  iindexx(nCP, CP-1, rank-1);
  
  // Copy sorted Collinear points
  for(int m=0;m<nCP;m++){
    // Get highest collinear
    long n  = rank[nCP-m-1] -1; // changed to [nCP-1 to 0]+cvhange sort idx to zero offset by -1.
    CP2[m]  = CP[n];
    iCP2[m] = iCP[n];
    jCP2[m] = jCP[n];
  }
  *nCP2 = nCP;
}

void collinearLineParam(float c[][3],int nc,int i,int j,int thres_proj,
                             float *mx2,float *my2,float *yint2, float *deg){
  float uy[1000],ux[1000]; // temp variables for holding collinear centres
  
  // ==================================
  // 1. Get collinear points with i, j into ux,uy,npt
  // Pre-calc n1 and n2 for speed up of about 50%
  int npt = 0;
  float n1 = (c[i][1]-c[j][1]);
  float n2 = (c[i][0]-c[j][0]);
  for(int k=0;k<nc;k++){
    float d = n1*(c[i][0]-c[k][0]) - n2*(c[i][1]-c[k][1]);
    if(fabs(d)<thres_proj){
      // printf("Collinear pts %f %f\n",c[k][0],c[k][1]);
      uy[npt] = c[k][1];
      ux[npt] = c[k][0];
      npt += 1; //  fastest with fabs
      
      if(npt>=1000) printf("********** Problem - too many npt");
    }
  }

  // --------------------------
  // 2. Line fitting
  // given gradient m, for unit vector
  // dx=1/sqrt(1+m^2), dy=1/sqrt(1+1/m^2)
  float m2,c2,mx,my;
  linefit(ux,uy,npt,&m2,&c2);
  mx = 1.0/sqrt(1.0+m2*m2);      // x for unit vector
  my = m2 /sqrt(1.0+m2*m2);  // y for unit vector

//  printf("Line fit- params: npt = %d, m2=%f c2=%f , mx,my: %f,%f \n",npt,m2,c2,mx,my);
//  printf("\n");
  
  *mx2   = mx;
  *my2   = my;
  *yint2 = c2;
  *deg   = atan(my/mx)*(180/3.141492654);
}

void collinearGradientPoints(unsigned char g2[],float mx, float my,float c2,
float px[],float py[],int pb[],int *np2){
  // --------------------------
  // 3. Generate gradient
  //      float py[1000],px[1000];
  //      int pb[1000]; // bitmap intensity
  int np=0;
  int marg = 20;
  for(int i=0;i<3000;i++){
    // check that y is inbound as x is already bounded
    float x = mx*i;
    float y = my*i + c2;
    if(y>marg && y<=IMW-marg && x>marg && x<=IMW-marg){
      px[np] = x;
      py[np] = y;
      pb[np] = g2[(int)roundf(x) + (int)roundf(y)*IMW];

      // draw line for check
      if(TC_VERBOSE) {
        g2[(int)roundf(x) + (int)roundf(y)*IMW] = 220;
      }
      
      
      if(np>=1000) printf("********** Problem");
      if(((int)roundf(x)+ (int)roundf(y)*IMW)>=SQ512) printf("********** Problem");
  //
  ////        _g2[(int)roundf(i + 100*IMW)] = 255;
//      printf("gradient: %d %f %f %d\n", np,px[np],py[np],pb[np]);
      np++;
      np = MIN(np,900);
    }else continue;
    
  }
  
  *np2 = np;
  
}

void findLongSegments(int pb[], int np, int seg[][2],int *nseg,
                      int thres_gap, int thres_len,int thres_den,int thres_lbreak){
  int ilast1 = 0, count0 = 0, istart,iend,len;
  int i,j;
  int ns=0;
  // Main loop
  for(i=0;i<np;i++){
    if(pb[i]==0) count0++;
    else{
      // '1' is detected
      if(count0>thres_gap || i==np-1){
        printf("*** Long break detected :");
        istart = ilast1;   // first occurrence of 1 after long break
        iend   = i-count0; // last occurrence of 1 after long break
        len    = iend - istart ;

        // Get longest minor breaks of 0
        int lbreak =0, c0=0, n1=0;
        for(j=istart;j<iend;j++){
          if(pb[j]==0) c0++;
          else{
            lbreak = MAX(c0,lbreak);
            c0 = 0;
            n1++;
          }
        }

        // Density
        float den = len>0? (float)n1/len:0;
        printf("len=%d, den=%f,longest break=%d\n",len,den,lbreak);
        // *****************************************
        if(len>thres_len && den>thres_den && lbreak<thres_lbreak){
          seg[ns][0] = istart;
          seg[ns][1] = iend;
          ns++;
          
          ns = MIN(ns,900);
        }
        

        // *****************************************
        // first occurrnece of 1 after a long break
         ilast1 = i;
      }
      count0 = 0;
    }

  } //np
  *nseg = ns;
  
}

void getIntercepts(unsigned char g1[], float px[],float py[],int istart, int iend,float gap[][2],int *ngap){
  // --------------------------
  // 4. Loop through each long segment
  int dqv[1000];
  int i,j,k;
//  i=0;
//  int istart=seg[i][0]-5,iend=seg[i][1]+5; // add a bit of margin
  
  // --------------------------
  // A. Sample from a thick Projections
  int w_prj=5;
  int ndq=0;
  for(i=istart;i<iend-1;i++){
    
    // Unit Projection vector
    float ux = py[i+1]-py[i];
    float uy = px[i+1]-px[i];
    
    dqv[ndq]=255;
    for(j=-w_prj;j<=w_prj;j++){
      // Sample projection point - should be automatically bounded by margin
      float kx = px[i] + ux*j;
      float ky = py[i] + uy*j;
      int kb = g1[(int)roundf(kx) + (int)roundf(ky)*IMW]; // original pixel value
      dqv[ndq] = MIN(kb, dqv[ndq]);
    } // for j
    ndq++;
  } // for i
  
//    printf("============= Inverted Bitmap Plot ===============\n");
//    for(i=0;i<npt;i++){
//      for(j=0;j<(255-dqv[i])/10;j++) printf("-");
//      printf("%d\n",dqv[i]);
//    }
  // --------------------------
  // B. Smooth by 7-pt convolution - without performing on the first and last 3 points
  const float mask[] = {.01, .02, .07, .8, .07, .02, .01};
  float dq[1000];
  int dqi[1000]; // just a flag to indicate intercept - for check only
  for(i=0;i<3;i++) dq[i]=dqv[i];
  for(i=ndq-3;i<ndq;i++) dq[i]=dqv[i];
  for(i=3;i<ndq-3;i++){
    dq[i]=0;
    for(j=0;j<7;j++) dq[i]+=dqv[i+j-3]*mask[j];
  }
  
//  printf("============= Inverted Bitmap Plot ===============\n");
//  for(i=0;i<ndq;i++){
//    for(j=0;j<(255-dq[i])/10;j++) printf("-");
//    printf("%3.0f\n",dq[i]);
//  }
  
  
  // --------------------------
  // C. Peak Detection
  int pk[1000][3];
  int nk   = 0;
  int isPeakLast = 99;
  int thres_dqv  = 5; // *** min.distance between peak and trough
  int peakLast = 0;
  int wp0 = 2;
//  int pk[1000][3];
  for(k=wp0; k<ndq-wp0; k++){
    // Use Score to check whether a point is peak or trough amongst neighbors
    int score=0; // isPeak - score=wp0*2, isTrough - score=0
    for(j=k-wp0; j<=k+wp0; j++) score += dq[k]>dq[j];
    
    int isPeak=0;
    if(score==wp0*2) isPeak=1;
    else if(score==0) isPeak=-1;
    
//    printf("%d score %d isPeak %d \n",k,score,isPeak);
    
    // Determine nearby duplicate peak
    // 1. 2 peaks must distance by min. thres_dqv
    // 2. 2 peaks must be opposite
    if(isPeak!=0){
      if(isPeakLast==99 || (isPeak!=isPeakLast && isPeak*(dq[k]-peakLast)>thres_dqv)){
        // a.add new peak
        pk[nk][0]  = k;
        pk[nk][1]  = dq[k];
        pk[nk][2]  = isPeak;
        isPeakLast = isPeak;
        peakLast   = dq[k];
        nk++;
      }else if(isPeak==isPeakLast && isPeak*dqv[k] > isPeak*peakLast){
        // b.overwrite the last.Last option is no change
        // if stronger peak than the last, then replace the last
        pk[nk-1][0]  = k;
        pk[nk-1][1]  = dq[k];
        pk[nk-1][2]  = isPeak;
        isPeakLast = isPeak;
        peakLast   = dq[k];
      }
    }
  } // for k
  
  // -------------------------------
  // Scale to 0-255 and get intercepts
  float ymax=255;
  float yint=255/2;
  for(i=1;i<nk;i++){
    float c,g;
    if(pk[i][2]<0){
      // trough: pk(k-1) scale to 255, p(k) to 0
      c = dq[pk[i][0]];
      g = ymax/ (dq[pk[i-1][0]] -c);
    }else{
      // peak: pk(k) scale to 255, p(k-1) to 0
      c = dq[pk[i-1][0]];
      g = ymax/ (dq[pk[i][0]] -c);
    }
    
    // Scale all dqv within peak and trough with same linear eq
    // ds - dqv scaled to [0,255]
//    ix     = [pka(bcEnd-1):pka(bcEnd)];
//    ds(ix) = grad*(dqv(ix) - const);
//    k=0;
    for(j=pk[i-1][0];j<pk[i][0];j++){
      dq[j] = g*(dq[j] - c);   // Scale to 0-255
    }
  }
  
  // Get intercepts
  float intc[1000][3];
  int nintc=0;
  for(j=1;j<ndq;j++){
    dqi[j] = 0;
    if((dq[j]>yint && dq[j-1]<=yint) || (dq[j-1]>yint && dq[j]<=yint)){
      float grad = dq[j]-dq[j-1];
      float xinc = (yint -dq[j-1])/grad;
      intc[nintc][0] = (j-1) + xinc ; // x intercept location
      intc[nintc][1] = pk[i][2] ; // isPeak
      nintc++;
      
      dqi[j-1] =1;
    }
  }
     
  // Transform Intercepts to Gaps
  int ng=0;
  for(i=1;i<nintc;i++){
    gap[ng][0] = intc[i][0] - intc[i-1][0];
    gap[ng][1] = intc[i][1]; // can use either i-1 or i
    ng++;
    
    ng = MIN(ng,900);
  }
  *ngap = ng;

  if(TC_VERBOSE){
    printf("============= Inverted Bitmap Plot ===============\n");
    for(i=0;i<ndq;i++){
      printf("%3d ",i);
      char *s= dqi[i]==1?"-":".";
      for(j=0;j<(275-dq[i])/10;j++) printf("%s",s);
      printf(" %3.0f\n", dq[i]);
    }
    
    printf("============= x intercepts and isPeak ===============\n");
    for(i=0;i<nintc;i++){
  //    printf("[%d] intc %4.1f %2.0f\n",i,intc[i][0],intc[i][1]);
      printf("%d %4.1f %2.0f\n",i,intc[i][0],intc[i][1]);
    }
    
    printf("============= dqi ===============\n");
    for(i=0;i<ndq;i++){
      printf(" %5.4f,",i,dq[i]);
    }
    printf("\n");
  }
}



void tc_decode(float gap[][2],int ngap,int *pass2,int dec[]){
  int i,j,k;
  int pass=0;
  dec[0]=-1;
  
  // --------------------------------
  // Decode
  int ntest = 0;
  for(int bcEnd=ngap-1; bcEnd>=46; bcEnd--){
    ntest++;
    if(ntest>5) break; // do not allow testing too many from the right - should be where code ends
    
    int istart = bcEnd-46;
    int iend   = bcEnd;
//      printf("[%d] istart iend %d %d\n",bcEnd,istart,iend);
    
    // Copy to gp and gps start at index 0
    float gp[47]; // gap starting at index 0
    int gps[47];  // gap sign start at index 0
    int bc[47],bw[47];   // coded message 1,3,-1,-3
    j=0;
    for(i=istart;i<=iend;i++){
      gp[j]  = gap[i][0];
      gps[j] = gap[i][1];
      j++;
    }
    
    // Find the gap width that provides 17 wides
    for(k=0;k<47;k++){
      float ptMaxSep = gp[k] - .0001;
      int nw=0;
      for(i=0;i<47;i++) nw+=gp[i]>ptMaxSep?1:0;
      
      // Separation point found to give 17 wides
      if(nw==17){
        for(i=0;i<47;i++){
          bc[i] = gp[i]>ptMaxSep ? gps[i]*3:gps[i];
          bw[i] = gp[i]>ptMaxSep ? 1:0; // wide or narrow
//            printf("nw==17 [%d] bc %d gp %f ptMaxSep %f\n",i,bc[i],gp[i],ptMaxSep);
        }
        break;
      }
//        printf("k %d ptMaxSep %f\n",k,ptMaxSep);
    }
    
//      char message[100];
//      strcpy(message,"");
//      message = "";
    pass=1;

    // Check Opening and Starting
    codechk((bw[0]==0 && bw[1]==0 && bw[2]==0 && bw[3]==0),"Start 0000");
    codechk((bw[44]==1 && bw[45]==0 && bw[46]==0),"End 100");
    

    // Check 2W for each code, and decode
    const int _wt[] = {1,2,4,7,0};
//    int dec[8];
    for(i=0;i<8;i+=2){
      j = 4+5*i;
      int nw=0;
      for(k=0;k<5;k++) nw+= bw[j+k*2];
      codechk(nw==2,"2Ws1");
      
      nw=0;
      for(k=0;k<5;k++) nw+= bw[j+k*2+1];
      codechk(nw==2,"2Ws2");
      
      // Decoding now
      dec[i]  =0;
      for(k=0;k<5;k++) dec[i]   +=bw[j+k*2  ]*_wt[k];
      dec[i+1]=0;
      for(k=0;k<5;k++) dec[i+1] +=bw[j+k*2+1]*_wt[k];
    }
    
    if(pass==1){
      printf("######## Successfully Decoded = [");
      for(i=0;i<8;i++) printf("%d",dec[i]);
      printf("]\n");
      break;
    }
    
    
    // goto exit point from codechk
    decodeNext: continue;
    
//      for(i=0;i<47;i++) printf("%+d(%3.2f) ",bc[i],gp[i]);
//      printf("\n");
//      for(i=0;i<47;i++) printf("%d ",bw[i]);
//      printf("\n");
    
    
//      printf("Message: %s\n", message);

    

  }
  *pass2 = pass;

//  return pass;
}


int tc_fast(unsigned char g1[],unsigned char g2[],unsigned char g3[]){

//  int *g2   = malloc(SQ512 * sizeof(int));   // too large for device must use malloc - cannot use label[SQ512]
  
  int i,j,k,m,n;
  int foundIndex = -1;

  double clockTime0 = CACurrentMediaTime();
  
  // ===================================================================
  // Original gray image (float[]), convolved image (float[] not used), binarized image;
  imageConv(g1,g2);
  
  printf("Convolutions: Time taken %f \n",CACurrentMediaTime()-clockTime0);

  // ===================================================================
  // **** Connected Component labelling (Working, but Inefficient! 4.4ms)
  clockTime0 = CACurrentMediaTime();

  int thres_size_min = 20;
  int thres_size_max = 1000;
  float centre[1000][3]; // maximum 10000 centres
  int ncentre = 0;
//  test(g2, centre, &ncentre, thres_size_min,thres_size_max);
  connectedLabelling(g2, centre, &ncentre, thres_size_min,thres_size_max);
   
  printf("ConnectedLabelling: Time taken %f \n",CACurrentMediaTime()-clockTime0);
  
  
  
  // ===================================================================
  // Fast9 - 3ms only!
  int numcorners;
  int threshold=60, xsize = 512,ysize=512,stride=512;
//  struct xy p[1000];
  clockTime0 = CACurrentMediaTime();
  xy *p;
  p = fast9_detect_nonmax(g1,  xsize,  ysize,  stride,  threshold, &numcorners);
  
  // Plot corners
  if(1){
    for(int i=0;i<numcorners;i++){
      printf("fast x,y = %d,%d\n",p[i].x,p[i].y);
     
      int x = p[i].x; // rounded down to int
      int y = p[i].y; // rounded down to int
      g2[x  + y   *IMW] = 255;
      g2[x+1+ y   *IMW] = 255;
      g2[x+1+(y+1)*IMW] = 255;
      g2[x  +(y+1)*IMW] = 255;
    }
  }
  
  free(p);
  printf("fast corners=%d\n",numcorners);
  printf("FAST9: Time taken %f \n",CACurrentMediaTime()-clockTime0);
  
  ncentre = MIN(ncentre,200); // no more than 200
  if(ncentre<5) return foundIndex;
  
  if(TC_VERBOSE){
    for(i=0;i<ncentre;i++){
      // Draw white dot at centres on image
      int x = centre[i][0]; // rounded down to int
      int y = centre[i][1]; // rounded down to int
      g2[x  + y   *IMW] = 255;
      g2[x+1+ y   *IMW] = 255;
      g2[x+1+(y+1)*IMW] = 255;
      g2[x  +(y+1)*IMW] = 255;
      
      printf("---- Centre %d/%d (x,y,n): %f %f %f\n",i,ncentre,centre[i][0],centre[i][1],centre[i][2]);
    }
  }
  return 1;
}




int tc_engine0(unsigned char g1[],unsigned char g2[],unsigned char g3[]){

//  int *g2   = malloc(SQ512 * sizeof(int));   // too large for device must use malloc - cannot use label[SQ512]
  
  int i,j,k,m,n;
  int foundIndex = -1;

  double clockTime0 = CACurrentMediaTime();
  
  // Original gray image (float[]), convolved image (float[] not used), binarized image;
  imageConv(g1,g2);
  
  printf("Convolutions: Time taken %f \n",CACurrentMediaTime()-clockTime0);

  // ===================================================================
  // **** Connected Component labelling (Working, but Inefficient! 4.4ms)
  clockTime0 = CACurrentMediaTime();

  int thres_size_min = 20;
  int thres_size_max = 1000;
  float centre[1000][3]; // maximum 10000 centres
  int ncentre = 0;
//  test(g2, centre, &ncentre, thres_size_min,thres_size_max);
  // Old method
//  connectedLabelling(g2, centre, &ncentre, thres_size_min,thres_size_max);
  
  // Di Stefano Labelling
  conl_DiStefano(g2, centre, &ncentre, thres_size_min,thres_size_max);
   
  printf("ConnectedLabelling: Time [%f] \n",CACurrentMediaTime()-clockTime0);
  
  
  ncentre = MIN(ncentre,200); // no more than 200
  if(ncentre<5) return foundIndex;
  
  if(TC_VERBOSE){
    for(i=0;i<ncentre;i++){
      // Draw white dot at centres on image
      int x = centre[i][0]; // rounded down to int
      int y = centre[i][1]; // rounded down to int
      g2[x  + y   *IMW] = 255;
      g2[x+1+ y   *IMW] = 255;
      g2[x+1+(y+1)*IMW] = 255;
      g2[x  +(y+1)*IMW] = 255;
      
      printf("---- Centre %d/%d (x,y,n): %2.1f %2.1f %2.1f\n",i,ncentre,centre[i][0],centre[i][1],centre[i][2]);
    }
  }

//  return 0;
  // ===================================================================
  // **** Collinear Point Search
  // Should not include small centres (<5)
  // Can we speed up by using integer for centre[][]?
  int thres_proj = 10;
  long M[40000]; // if 1000x1000, crashes

  long CP[1000];
  int iCP[1000],jCP[1000]; // M count, idx number
//  unsigned long a[200*200];
    
    // http://www.bowdoin.edu/~ltoma/teaching/cs3250-CompGeom/spring17/Lectures/ex-collineartriplets.pdf
    // Much faster! Compute slopes, then sort slopes, then look for consecutive points with same slope
  //  clockTime0 = CACurrentMediaTime();
  //  float slope[200];
  //  for(i=0;i<ncentre;i++){
  //    for(j=0;j<ncentre;j++){
  //      slope[j] = (centre[i][1]-centre[j][1]) / (centre[i][0]-centre[j][0]);
  //    }
  //  indexx(ncentre, slope, a);
  //  }
  //  printf("Sort method: Time taken %f \n",CACurrentMediaTime()-clockTime0);
    
    
  clockTime0 = CACurrentMediaTime();
  
  int nCP=0;
  collinearSearch(centre,ncentre,thres_proj,M,CP,&nCP,iCP,jCP);
  
  printf("CollinearSearch: Time [%f] \n",CACurrentMediaTime()-clockTime0);
  

  
  if(TC_VERBOSE){
    printf("---------- Projection distance matrix\n");
    for(i=0;i<ncentre;i++){
      for(j=0;j<ncentre;j++){
        if(M[i*ncentre + j]<0) printf("n ");
        else if (M[i*ncentre + j]==0) printf(". ");
        else printf("%ld ",M[i*ncentre + j]);
      }
      printf("\n");
    }
  }

  // Record previous degree and y intercept so not to repeat
  float history[1000][3];
  int nhistory=0;
  clockTime0 = CACurrentMediaTime();
  // Loop over high collinear points, starting from best prospect
  for(m=0;m<nCP;m++){
//    printf("\n--------------------- Looping over (%d/%d) Collinear points\n",m,nCP);
//    if(m!=3) continue;

    // Get line parameter
    float mx,my,yint,deg;
    collinearLineParam( centre, ncentre, iCP[m], jCP[m], thres_proj, &mx, &my, &yint, &deg);
    
    if(TC_VERBOSE) printf("Line degree=%f\n",deg);
    if(fabs(deg)>70){
      if(TC_VERBOSE) printf("Line gradient too steep. Skip.");
      continue;
    }
    
    // Check similar line - if found, skip.
    float thres_deg   = .5;
    float thres_yint  = 5;
    int isSimilarLine = 0;
    for(i=0;i<nhistory;i++){
      if(fabs(deg-history[i][0])<thres_deg && fabs(yint-history[i][1])<thres_yint){
        if(TC_VERBOSE) printf("Similar line was found. Current line skipped.");
        history[i][2]++;
        isSimilarLine = 1;
        continue;
      }
    }
    if(isSimilarLine) continue;
    else{
      history[nhistory][0] = deg;  // line gradient in deg
      history[nhistory][1] = yint; // y intercept
      history[nhistory][2] = 1;    // number of points similar
      nhistory++;
    }
    
    if(TC_VERBOSE){
      printf("----------- history \n");
      for(i=0;i<nhistory;i++){
        printf("[%d/%d] degree %f yint %f count %f\n",i,nhistory,history[i][0],history[i][1],history[i][2]);
      }
    }
    
    // Sample points on line, from g2
    float py[1000],px[1000]; // point xy
    int pb[1000]; // bitmap intensity
    int np=0; // num points
    collinearGradientPoints(g2, mx,my,yint, px, py, pb, &np);
    
    if(np<100) continue; // too short

    
    // --------------------------
    // 3. Detect long unbroken segments - into array seg (start and end)
    // Initialization
    pb[0]    =128; // easier on algo - help initialize ilast1
    if(TC_VERBOSE) printf("[%d/%d Collinear lines] points on this line %d\n",m,nCP,np);

    // np can be 0!
    if(np>1){
      pb[np-1] =128; // one last check on last bit - this one causes crash if np=0
    }
    
    int seg[1000][2];
    int nseg;
    int thres_gap=50, thres_len=50, thres_den=0.6, thres_lbreak=50;
    findLongSegments(pb, np, seg, &nseg,
                     thres_gap,  thres_len, thres_den, thres_lbreak);
    
    // Display Extracted Segment with double line
    if(TC_VERBOSE){
      for(i=0;i<nseg;i++){
        printf("[%d] Long seg %d %d \n",m, seg[i][0],seg[i][1]);
        
        // Draw to check
        for(j=seg[i][0];j<=seg[i][1];j++){
          int h=10;
          g2[(int)roundf(px[j]) + (int)roundf(py[j]-h)*IMW] = 220;
          g2[(int)roundf(px[j]) + (int)roundf(py[j]+h)*IMW] = 220;
        }
      }
    }
    
    
//    int pk[1000][3];
//    int npk=0;
    float gap[1000][2];
    int ngap=0;
    i=0;
    int istart=seg[i][0]-5,iend=seg[i][1]+5; // add a bit of margin
    getIntercepts(g1,px,py,istart,iend,gap,&ngap);

    if(TC_VERBOSE){
      printf("============= Gaps ===============\n");
      for(i=0;i<ngap;i++){
        printf("[%d] gap %4.2f %2.1f\n",i,gap[i][0],gap[i][1]);
      }
    }
    
    if(ngap<47) continue;
    
    
    // --------------------------------
    // Decode
    int pass=0;
    int dec[8];
    tc_decode(gap,ngap,&pass,dec);
//    return -1;
    if(pass){

      // Combine into a single integer
      int decoded = 0;
      for(i=0;i<8;i++) decoded += dec[i]*pow(10,(8-i-1));
      
      
      printf("[%d/%d] ^^^^^^ Successfully Decoded = [",m,nCP);
      for(i=0;i<8;i++) printf("%d",dec[i]);
      printf("]  = %d \n",decoded);
      
      // Requires function from ic_engine.h
//      foundIndex = myBinarySearch(decoded);
      break;
    }else{
      printf("[%d/%d] Decode unsuccessful.\n",m,nCP);
    }
       
    
  } // for m, each highly collinear points
  printf("Loop over collinear pointa: Time taken %f \n",CACurrentMediaTime()-clockTime0);
  return foundIndex;
}

void ut_connectedLabelling(unsigned char g1[],unsigned char g2[],unsigned char g3[]){
    int i,j,k,m,n;
    int foundIndex = -1;

    double clockTime0 = CACurrentMediaTime();
    
    // Original gray image (float[]), convolved image (float[] not used), binarized image;
    imageConv(g1,g2);
    printf("Convolutions: Time taken %f \n",CACurrentMediaTime()-clockTime0);

  // Create a huge connected region - can cause crash on dfs
//  for(i=00;i<512;i++)
//    for(j=00;j<300;j++) g2[j+i*IMW] = 100;
  
    // ===================================================================
    // **** Connected Component labelling (Working, but Inefficient! 4.4ms)
    clockTime0 = CACurrentMediaTime();

    int thres_size_min = 20;
    int thres_size_max = 1000;
    float centre[1000][3]; // maximum 10000 centres
    int ncentre = 0;
  //  test(g2, centre, &ncentre, thres_size_min,thres_size_max);
    connectedLabelling(g2, centre, &ncentre, thres_size_min,thres_size_max);
   printf("Connected labelling: Time taken %f \n",CACurrentMediaTime()-clockTime0);
  if(TC_VERBOSE){
    for(i=0;i<ncentre;i++){
      // Draw white dot at centres on image
      int x = centre[i][0]; // rounded down to int
      int y = centre[i][1]; // rounded down to int
      g2[x  + y   *IMW] = 255;
      g2[x+1+ y   *IMW] = 255;
      g2[x+1+(y+1)*IMW] = 255;
      g2[x  +(y+1)*IMW] = 255;
      
      printf("---- Centre %d/%d (x,y,n): %f %f %f\n",i,ncentre,centre[i][0],centre[i][1],centre[i][2]);
    }
  }
}





@end
