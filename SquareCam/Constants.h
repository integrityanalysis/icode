//
//  Constants.h
//  SquareCam 
//
//  Created by Zeke Chan on 27/05/16.
//
//

#ifndef Constants_h
#define Constants_h

#define use_planar        0
#define db_showFeatures   0
#define CB                34
#define db_testStabilize  1
#define db_printImage     0
#define db_noStabilize    0
#define db_saveCameraRoll 0
#define db_viewDebug      0
#define runSimulator      0
#define db_utInitVC       0  // Unit Test
#define db_useDemo        0  // testA
#define db_showPlot       1  // testA
#define db_applyRadLimit  1
#define db_demoRelease    0
#define db_tryMultiPeak   0
#define db_useOldFilter   0
#define db_testMode       0 // 0:normal,1:showPlot & no item menu
#define captureMode       1 // 0:640x480,RGB, 1:1280x720,YCbCr
#define deleteExpired     0 // Delete expired codes - set to 0 for testing

#define APP_STORE_ID 1039981052
#define DEBUG_BOX 0
#define STOP_CRASHLYTICS 0
#define STOP_GOOGLEANALYTICS 0
#define TEST_SUBMIT 0 // hide uneeded tabs
#define FOCUS_NEAR 0 // force focus range to near object

#define NORM2(a,b) sqrt(a*a + b*b)
#define	MAX(a, b)	((a) < (b) ? (b) : (a))
#define	MIN(a, b)	((a) > (b) ? (b) : (a))
#define wirth_median(a,n) kthsmallest((((n)&1)?((n)/2):(((n)/2)-1)),n,a)
#define modz(x,y) (x<0) ? (x+y) : (x>=y ? x-y : x) // Zeke's modulus

#define docDir [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]

#pragma mark ===== Device Classification ========
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_32BIT (sizeof(void*) == 4)

#define TRANSLATE_TO_DEST(xDest,yDest,object) CGAffineTransformMakeTranslation(xDest-object.center.x,yDest-object.center.y)
#define ROTATE_TRANSLATE(rotate,xDest,yDest,object) CGAffineTransformConcat(rotate,TRANSLATE_TO_DEST(xDest,yDest,object))

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define UA_rgba(r,g,b,a)			[UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define UA_rgb(r,g,b)					UA_rgba(r, g, b, 1.0f)
#define MY_LITEORANGE UA_rgb(255,204,102)

#define BUTTONDISABLE(button) button.enabled=NO; button.alpha=0.5;
#define BUTTONENABLE(button)  button.enabled=YES;button.alpha=1.0;
#define _LS(str) NSLocalizedString(str, str);
#define nls(key,str) NSLocalizedString(key, str)
#define subNil(str)  (str?str:@"")     // Substitute nil string with empty string
#define ptf(str,fmt) [NSString stringWithFormat:str,fmt] // Use it like printf in c

#define strdev(str) [str stringByReplacingOccurrencesOfString:@"xxx" withString:IS_IPAD?@"ipad":@"iphone"]
#define blanknil(field) (field?field:@"")  // if field is nil, put a blank.Prevent crashing.


#define LOGFRAME(view) NSLog(@"Frame of %s: %f %f %f %f", #view, view.frame.origin.x, view.frame.origin.y, view.frame.size.width, view.frame.size.height);

#define SCALE6(view) view.frame = CGRectMake(view.frame.origin.x * 375.0/320.0, view.frame.origin.y * 667.0/568.0, view.frame.size.width * 375.0/320.0, view.frame.size.height * 667.0/568.0);
#define SCALE6P(view) view.frame = CGRectMake(view.frame.origin.x * 414.0/320.0, view.frame.origin.y * 736.0/568.0, view.frame.size.width * 414.0/320.0, view.frame.size.height * 736.0/568.0);
#define SCALE6_44(view) view.frame = CGRectMake(view.frame.origin.x * 375.0/320.0, (view.frame.origin.y-44) * 667.0/568.0 + 44, view.frame.size.width * 375.0/320.0, view.frame.size.height * 667.0/568.0);
#define SCALE6P_44(view) view.frame = CGRectMake(view.frame.origin.x * 414.0/320.0, (view.frame.origin.y-44) * 736.0/568.0 + 44, view.frame.size.width * 414.0/320.0, view.frame.size.height * 736.0/568.0);


// Scaling for text font sizes
#define SC_TEXT_LABEL(view) UIFont *font = view.font; if (IS_IPHONE_6) { view.font = [UIFont fontWithName:font.fontName size:font.pointSize+1]; } else if (IS_IPHONE_6P) { view.font = [UIFont fontWithName:font.fontName size:font.pointSize+2]; }
#define SC_TEXT_BUTTON(view) UIFont *font = view.font; if (IS_IPHONE_6) { view.titleLabel.font = [UIFont fontWithName:font.fontName size:font.pointSize+1]; } else if (IS_IPHONE_6P) { view.titleLabel.font = [UIFont fontWithName:font.fontName size:font.pointSize+2]; }
#define SC_TEXT(view) if([view isKindOfClass:[UILabel class]]) { SC_TEXT_LABEL(view) } else if([view isKindOfClass:[UIButton class]]) { SC_TEXT_BUTTON(view) }

// Zeke
#define CGRects(x,y,w,h) NSStringFromCGRect(CGRectMake(x,y,w,h))
#define SC6(frame)  frame = CGRectMake(frame.origin.x * 375.0/320.0, frame.origin.y * 667.0/568.0, frame.size.width * 375.0/320.0, frame.size.height * 667.0/568.0);
#define SC6P(frame) frame = CGRectMake(frame.origin.x * 414.0/320.0, frame.origin.y * 736.0/568.0, frame.size.width * 414.0/320.0, frame.size.height * 736.0/568.0);
#define SC6_44(frame) frame = CGRectMake(frame.origin.x * 375.0/320.0, (frame.origin.y-44) * 667.0/568.0 + 44, frame.size.width * 375.0/320.0, frame.size.height * 667.0/568.0);
#define SC6P_44(frame) frame = CGRectMake(frame.origin.x * 414.0/320.0, (frame.origin.y-44) * 736.0/568.0 + 44, frame.size.width * 414.0/320.0, frame.size.height * 736.0/568.0);
#define SCF(frame)  if(IS_IPHONE_6){ SC6(frame); }else if(IS_IPHONE_6P){ SC6P(frame); }
#define SCF_44(frame) if(IS_IPHONE_6){ SC6_44(frame); }else if(IS_IPHONE_6P){ SC6P_44(frame); }



#define SPALERT(title,message) [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"00_ok", @"OK") otherButtonTitles: nil] show];
// Auto select Phone scaling - both view and all subviews
#define SCALEF(view)  if(IS_IPHONE_6){ SCALE6(view); }else if(IS_IPHONE_6P){ SCALE6P(view); }
#define SCALEFS(view) for(UIView *v in [view subviews]){ SCALEF(v); }

#define matchLowerCase(s1,s2) [[s1 lowercaseString] isEqual: [s2 lowercaseString]]
#define findLowcase(s1,s2)     [s1.lowercaseString containsString:s2.lowercaseString]
#define isCurrentUser(pfUser) [[(PFUser *)(pfUser) objectId] isEqual:[PFUser currentUser].objectId]
#define isCurrentUserId(pfUserId) [[PFUser currentUser].objectId isEqual:pfUserId]


#endif /* Constants_h */

