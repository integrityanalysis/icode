//
//  TCAlgo.h
//  SquareCam
//
//  Created by Zeke Chan on 9/01/20.
//

#ifndef TCAlgo_h
#define TCAlgo_h


#endif /* TCAlgo_h */

@interface TCAlgo : NSObject


+ (UIImage *) processFrameToImage : (unsigned char*) baseAddress
      setWidth : (int) width
     setHeight : (int) height
setBytesPerRow : (int) bytesPerRow
                    setImageColor : (BOOL) imageColor;

@end
