//

//  Code.h

//  SquareCam

//

//  Created by Eden Choi on 9/12/16.

//

//



#import <AWSCore/AWSCore.h>

#import <AWSDynamoDB/AWSDynamoDB.h>



@interface Code : AWSDynamoDBObjectModel <AWSDynamoDBModeling>


@property (nonatomic, strong) NSString *Code;
@property (nonatomic, strong) NSNumber *CreatedAt;
@property (nonatomic, strong) NSNumber *UpdatedAt;
@property (nonatomic, strong) NSNumber *ExpireIn; // Expire in num.days after CreatedAt
@property (nonatomic, strong) NSString *Desc;
@property (nonatomic, strong) NSString *Url;
@property (nonatomic, strong) NSString *ClientId;
@property (nonatomic, strong) NSString *ClientName;
@property (nonatomic, strong) NSString *CreatedAtS;
@property (nonatomic, strong) NSString *UpdatedAtS;
@property (nonatomic, strong) NSString *ScannedAt;
@property (nonatomic, strong) NSData *Thumbnail;


+ (NSString *)dynamoDBTableName;

+ (NSString *)hashKeyAttribute;

+ (NSString *)rangeKeyAttribute;

- (UIImage *) getImage;

@end
