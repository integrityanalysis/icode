//
//  UT5ViewController.m
//  
//
//  Created by Zeke Chan on 30/12/19.
//

#import "UT5ViewController.h"

#import "TCAlgo.h"
#include "tc_engine.h"

#import "plotView.h"
#include <stdint.h> // int 64

#include "nr.h"
#include "nrutil.h"
#include "math.h"

#import "ic_engine.h" // may need to remove this?
#include <Accelerate/Accelerate.h>
#import <AWSCore/AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "Code.h"
#import "Scan.h"
#import "ICG.h"
#import "Code.h"
#import "ICWebViewController.h"
#import "UIImage+Resize.h"
#import <QuartzCore/QuartzCore.h>
#import <ZXingObjC/ZXingObjC.h>
#import <Crashlytics/Crashlytics.h>
#import "Reachability.h"

#define IMW 512  // Image width
#define SQ512 512*512 // Square 512

@interface UT5ViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *iv1;
@property (strong, nonatomic) IBOutlet UIImageView *iv2;

@end

@implementation UT5ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self buttonUT3:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// https://www.geeksforgeeks.org/represent-given-set-points-best-possible-straight-line/
// function to calculate m and c that best fit points
// represented by x[] and y[]
//void linefit(float x[], float y[], int n, float *m, float *c)
//{
//    int i, j;
//    float sum_x = 0, sum_y = 0, sum_xy = 0, sum_x2 = 0;
//    for (i = 0; i < n; i++) {
//        sum_x += x[i];
//        sum_y += y[i];
//        sum_xy += x[i] * y[i];
//        sum_x2 += (x[i] * x[i]);
//    }
//  
//    *m = (n * sum_xy - sum_x * sum_y) / (n * sum_x2 - (sum_x * sum_x));
//    *c = (sum_y - *m * sum_x) / n;
//}


//float _tmpIm[500000];
//unsigned char _img1[500000];
//void loadCSVFile(char filename[], float tmpf[]){
//  // Read CSV file from bundle
//  int i,j;
//  NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithUTF8String:filename] ofType:@"csv"];
//  NSData *myData     = [NSData dataWithContentsOfFile:filePath];
//  if (myData) {
//    // Load CSV file elements, line by line
//    j = 0;
//    NSString *file= [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
//    NSArray  *allLines = [file componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
//    for (NSString* line in allLines) {
//      NSArray *elements = [line componentsSeparatedByString:@","];
//      int ne = [elements count];
//      for(i=0;i<ne;i++)  tmpf[j++] = [elements[i] floatValue];
//    }
//    NSLog(@"Load CSV: file found. (%d) elements loaded.",j);
//  }
//}

- (IBAction)buttonUT1:(id)sender {
  
  printf("\n***** Start test fitEllipse *****\n");
  // from tmp7. fprintf('%d,',[f.col; f.row])
  float data[384*2]={266,266,266,266,266,266,266,267,267,267,267,267,278,278,279,279,279,279,279,279,279,284,284,284,285,285,285,285,285,285,285,290,290,291,291,290,291,291,291,291,312,313,312,314,313,314,313,314,313,314,313,314,313,314,313,314,318,319,318,319,319,319,328,329,328,329,328,329,328,331,329,330,329,329,333,334,334,333,333,335,334,335,336,337,337,338,339,337,338,339,340,336,338,339,340,336,338,336,339,340,342,343,344,342,343,344,342,343,344,345,343,343,345,347,346,347,346,348,347,346,348,347,346,347,347,349,350,352,351,352,351,348,352,351,350,353,352,351,352,351,359,361,360,362,361,360,362,364,361,363,362,364,361,363,362,362,363,366,367,368,369,367,368,369,367,369,367,368,367,368,369,370,371,369,370,371,369,372,370,371,370,373,371,369,371,372,373,371,372,373,371,374,372,373,374,372,375,373,372,374,375,374,375,376,376,374,375,375,369,372,375,377,376,375,379,378,377,379,378,380,379,378,376,376,375,374,377,376,378,377,376,379,378,377,380,379,381,369,368,368,370,368,372,369,371,370,372,375,367,365,367,366,365,366,368,367,369,368,369,361,366,364,362,363,361,362,365,363,366,367,366,366,357,360,354,362,355,351,349,350,351,349,352,353,350,351,352,353,340,341,342,343,341,345,344,341,344,343,338,332,333,332,331,332,335,334,333,335,327,334,327,328,328,327,329,326,331,330,327,331,328,330,331,330,320,322,317,313,312,313,314,311,312,314,312,313,301,303,301,302,303,301,302,303,301,302,300,301,298,296,296,295,296,297,295,296,297,295,296,297,294,295,296,295,294,290,290,291,290,291,289,290,289,290,289,290,289,286,284,285,263,263,263,262,131,132,133,134,135,136,137,134,135,136,137,138,134,135,134,135,136,137,138,139,140,134,135,136,135,136,137,138,139,140,141,136,137,136,137,141,138,139,140,142,146,145,147,144,146,145,147,146,148,147,149,148,150,149,151,150,149,148,152,151,153,154,156,155,157,156,159,158,160,156,159,158,160,161,160,159,160,162,163,161,164,164,164,163,164,164,163,166,165,164,163,168,166,165,164,169,167,170,167,170,168,168,167,170,169,168,171,170,169,168,171,172,172,172,173,173,174,173,174,175,174,175,176,176,177,177,177,176,177,177,178,181,178,179,180,178,179,180,180,181,193,192,193,192,193,194,193,192,194,193,194,193,195,194,195,196,196,203,203,203,203,204,204,204,205,205,206,206,208,208,208,208,208,209,209,209,210,209,210,210,211,210,211,214,214,214,214,215,215,215,216,215,216,216,216,217,216,217,219,219,219,220,220,220,221,222,222,223,252,253,253,273,273,273,274,274,274,275,275,276,276,277,278,279,279,279,280,280,281,281,281,282,282,282,283,284,293,300,301,302,303,303,305,304,305,305,306,308,306,307,308,308,308,309,310,310,311,311,312,311,314,313,312,313,312,313,315,314,316,317,317,318,316,319,316,323,319,333,332,333,334,333,336,337,335,336,337,338,333,341,342,344,342,347,346,343,347,346,352,349,351,350,349,351,355,354,353,356,346,355,351,353,354,353,356,352,359,358,354,360,356,359,361,360,358,368,363,364,363,365,367,362,364,368,365,367,367,373,368,371,374,369,372,375,370,373,368,371,372,370,371,369,372,375,370,373,376,371,374,377,369,372,375,373,370,372,373,377,374,378,371,375,372,376,373,377,374,374,378,385,369,379,381,369};
  
  float ellipseParam[3];
  for(int i=0;i<384;i++){
    data[i] -= 256.5;
    data[384+i] -= 256.5;
    printf("%d  %f  %f \n",i,data[i] ,data[384+i]);
  }
  fitEllipse(&data[0],&data[384], 384, ellipseParam);
  printf("\n***** End test fitEllipse *****\n\n");
  
  
  
  
}
- (IBAction)buttonUT2:(id)sender {
  
//  loadCSVFile("Im512_2",_tmpIm);
//  for(int i=0;i<10;i++) printf("%f ",_tmpIm[i]);
//
//  loadCSVFile("Im512",_tmpIm);
//  for(int i=0;i<10;i++) printf("%f ",_tmpIm[i]);
}

// -----------------------------------------------------
// Simple but inefficient 8-Connected Component Labelling
// https://stackoverflow.com/questions/14465297/connected-component-labeling-implementation
// direction vectors
//const int dx[] = {+1, 0, -1, 0, +1, +1, -1, -1};
//const int dy[] = {0, +1, 0, -1, -1, +1, -1, +1};
//
//// the labels, 0 means unlabeled
//int label[SQ512];
//float centre[10000][3]; // maximum 10000 centres
//
//void dfs(int x, int y, int current_label, unsigned char m[]) {
//  // Important that for every new image, last_label is initialized
//  static int last_label = -99;
//  if(current_label==-1) last_label= -99;
//
//  if (x < 0 || x == IMW) return; // out of bounds
//  if (y < 0 || y == IMW) return; // out of bounds
//  if (label[x+y*IMW] || !m[x+y*IMW]) return; // already labeled or not marked with 1 in m
//
//  // mark the current cell
//  label[x+y*IMW] = current_label;
//
//  // --------- Zeke - summarize centre info
//  if(current_label!=last_label){
//    centre[current_label][0] = x;
//    centre[current_label][1] = y;
//    centre[current_label][2] = 1;
//  }else{
//    centre[current_label][0] += x;
//    centre[current_label][1] += y;
//    centre[current_label][2] += 1;
//  }
//  last_label = current_label;
//  // ----------
//
//  // recursively mark the neighbors
//  for (int direction = 0; direction < 8; ++direction)
//    dfs(x + dx[direction], y + dy[direction], current_label,m);
//}

//void find_components() {
//  int component = 0;
//  for (int i = 0; i < row_count; ++i)
//    for (int j = 0; j < col_count; ++j)
//      if (!label[i+j*IMW] && m[i+j*IMW]) dfs(i, j, ++component);
//}
// -----------------------------------------------------


float _tcIm1[SQ512], _tcIm2[SQ512];

unsigned char _g1[SQ512],_g2[SQ512],_g3[SQ512];
float _t1[SQ512],_t2[SQ512],_t3[SQ512];
- (IBAction)buttonUT3:(id)sender {
//  BOOL check = 1;
//  int i,j,k,m,n;
//  loadCSVFile("im_1912_06",_t1);
//  for(i=0;i<SQ512;i++) _g1[i]=_t1[i];
//
//
//
//
//  // 3x3 Gaussian Convolution
////  float B[9]={
////    0.011,0.084,0.011,
////    0.084,0.619,0.084,
////    0.011,0.084,0.011
////  };
////  vDSP_f3x3(_t1, IMW, IMW, B, _t2);
////
////  // Edge detection kernel
////  float E[9]={
////    -1 ,   -1  ,  -1,
////    -1 ,    8  ,  -1,
////    -1 ,   -1  ,  -1
////  };
////  vDSP_f3x3(_t2, IMW, IMW, E, _t2);
//
//  double clockTime0 = CACurrentMediaTime();
//
//
//  // ======================================================================
//  // Image Convolutions
//  // 2 Important parameters: (1)edge detetion threshold, (2)fattening 3x3 or 5x5
//  int thres_edge = -70; // edge detection parameter
//  int nfat       = 5;
//
//  // Combined 3x3 Gaussian and Edge Detection
//  float B[25]={
//     -0.011344,-0.095163,-0.106507,-0.095163,-0.011344,
//     -0.095163,-0.696236,-0.139117,-0.696236,-0.095163,
//     -0.106507,-0.139117,4.574123,-0.139117,-0.106507,
//     -0.095163,-0.696236,-0.139117,-0.696236,-0.095163,
//     -0.011344,-0.095163,-0.106507,-0.095163,-0.011344
//   };
//   vDSP_f5x5(_t1, IMW, IMW, B, _t2);
//
//  // Thresholding for edges
//  for(i=0;i<SQ512;i++) _t2[i]=(_t2[i]<thres_edge);
//
//  // Fattening
//  if(nfat==5){
//    // Fattens by 5x5 disk
//    float D[25]={
//      0.000000,0.017016,0.038115,0.017016,0.000000,
//      0.017016,0.078381,0.079577,0.078381,0.017016,
//      0.038115,0.079577,0.079577,0.079577,0.038115,
//      0.017016,0.078381,0.079577,0.078381,0.017016,
//      0.000000,0.017016,0.038115,0.017016,0.000000
//    };
//    vDSP_f5x5(_t2, IMW, IMW, D, _t3);
//  }else{
//    // Fatten by 3x3 Disk
//    float D[9]={
//      0.025079,0.145344,0.025079,
//      0.145344,0.318310,0.145344,
//      0.025079,0.145344,0.025079,
//    };
//    vDSP_f3x3(_t2, IMW, IMW, D, _t3);
//  }
//
//  printf("Convolutions: Time taken %f \n",CACurrentMediaTime()-clockTime0);
//
//  // Binarize
//  for(i=0;i<SQ512;i++) _g2[i]=128*(_t3[i]>0);
//
//  // ===================================================================
//  // **** Connected Component labelling (Working, but Inefficient! 4.4ms)
//  clockTime0 = CACurrentMediaTime();
//
//  dfs(0, 0, -1,_g2); // Reset static variable last_label: set current_Label=-1
//  int component = 0;
//  for (int i = 0; i < IMW; ++i)
//    for (int j = 0; j < IMW; ++j)
//      if (!label[i+j*IMW] && _g2[i+j*IMW]) dfs(i, j, ++component,_g2);
//
//  printf("Connected labelling: Time taken %f \n",CACurrentMediaTime()-clockTime0);
//
//  // Centre array: start from [1] and ends on [component], following variable [component]. No [0].
//  int thres_size_min = 20;
//  int thres_size_max = 1000;
//  float c[10000][3]; // maximum 10000 centres
//  int nc = 0;
//  for(i=1;i<=component;i++){
//    centre[i][0]/=centre[i][2]; // Get mean x
//    centre[i][1]/=centre[i][2]; // Get mean y
//
//    printf("Component %d (x,y,n): %f %f %f\n",i,centre[i][0],centre[i][1],centre[i][2]);
//
//    // Extract centres exceeding threshold size
//    // Should also remove large ones >1000 i think
//    if(centre[i][2]>=thres_size_min & centre[i][2]<=thres_size_max){
//      c[nc][0] = centre[i][0];
//      c[nc][1] = centre[i][1];
//      c[nc][2] = centre[i][2];
//      nc++;
//    }
//
//    // Draw white dot at centres on image
//    int x = centre[i][0]; // rounded down to int
//    int y = centre[i][1]; // rounded down to int
//    _g2[x  + y   *IMW] = 255;
//    _g2[x+1+ y   *IMW] = 255;
//    _g2[x+1+(y+1)*IMW] = 255;
//    _g2[x  +(y+1)*IMW] = 255;
//  }
//
//  for(i=0;i<nc;i++){
//    printf("---- Centre %d (x,y,n): %f %f %f\n",i,c[i][0],c[i][1],c[i][2]);
//  }
//
//  // ===================================================================
//  // **** Collinear Point Search
//  // Should not include small centres (<5)
//  // Can we speed up by using integer for c[][]?
//  int thres_proj = 10;
//  long M[200*200]; // if 1000x1000, crashes
//
//  long CP[1000];
//  int iCP[1000],jCP[1000]; // M count, idx number
//  unsigned long a[200*200];
//
//  // http://www.bowdoin.edu/~ltoma/teaching/cs3250-CompGeom/spring17/Lectures/ex-collineartriplets.pdf
//  // Much faster! Compute slopes, then sort slopes, then look for consecutive points with same slope
////  clockTime0 = CACurrentMediaTime();
////  float slope[200];
////  for(i=0;i<nc;i++){
////    for(j=0;j<nc;j++){
////      slope[j] = (c[i][1]-c[j][1]) / (c[i][0]-c[j][0]);
////    }
////  indexx(nc, slope, a);
////  }
////  printf("Sort method: Time taken %f \n",CACurrentMediaTime()-clockTime0);
//
//
//  clockTime0 = CACurrentMediaTime();
//
//  int nCP = 0;
//  for(i=0;i<nc;i++){
//    for(j=i+1;j<nc;j++){
//      // Pre-calc n1 and n2 for speed up of about 50%
//      M[i*nc + j]=-2;
//      float n1 = (c[i][1]-c[j][1]);
//      float n2 = (c[i][0]-c[j][0]);
//      for(k=0;k<nc;k++){
////        int d = (c[i][1]-c[j][1])*(c[i][0]-c[k][0]) - (c[i][1]-c[k][1])*(c[i][0]-c[j][0]); // original
//        float d = n1*(c[i][0]-c[k][0]) - n2*(c[i][1]-c[k][1]); // use pre-calc n1 and n2 - faster
//        if(fabs(d)<thres_proj) M[i*nc + j] += 1; //  fastest with fabs
////        M[i][j] += ((d>0) ? d:-d)<thres_proj; // works ok but slower
//      }
//      if(M[i*nc + j] > 3){
//        CP[ nCP ]  = M[i*nc + j];
//        iCP[ nCP ] = i;
//        jCP[ nCP ] = j;
//        nCP++;
//      }
//    }// for j
//  } // for i
//  printf("Projection matrix: Time taken %f \n",CACurrentMediaTime()-clockTime0);
//
//  printf("---------- Projection distance matrix\n");
//  for(i=0;i<nc;i++){
//    for(j=0;j<nc;j++){
//      if(M[i*nc + j]<0) printf("n ");
//      else if (M[i*nc + j]==0) printf(". ");
//      else printf("%d ",M[i*nc + j]);
//    }
//    printf("\n");
//  }
//
//  iindexx(nc*nc, M, a); // Start on element 1 or 0?
//
//  for(i=0;i<100;i++){
//    printf("(%d) %d %d\n",i,M[a[nc*nc-i]],a[nc*nc-i]);
//  }
//
//  for(i=0;i<nCP;i++){
//    printf("nCP original (%d) %d %d %d\n",i,CP[i],iCP[i],jCP[i]);
//  }
//
//  // iindexx - unit offset so must change to zero offset by array-1
//  // index is from [1,....len] so must do a[i]-1 to change to [0,...len-1]
//
//  iindexx(nCP, CP-1, a-1);
//  float uy[1000],ux[1000];
//
//
//  for(m=0;m<nCP;m++){
//    // Get highest collinear
//    n = a[nCP-m-1] -1; // changed to [nCP-1 to 0]+cvhange sort idx to zero offset by -1.
//    printf("%d (%d,%d) %d %d %d\n",a[m]-1,m,a[nCP-m-1]-1,CP[n],iCP[n],jCP[n]);
//    i = iCP[n];
//    j = jCP[n];
//
//    // ==================================
//    // Get collinear points with i, j
//    // Pre-calc n1 and n2 for speed up of about 50%
//    int npt = 0;
//    float n1 = (c[i][1]-c[j][1]);
//    float n2 = (c[i][0]-c[j][0]);
//    for(k=0;k<nc;k++){
//      float d = n1*(c[i][0]-c[k][0]) - n2*(c[i][1]-c[k][1]);
//      if(fabs(d)<thres_proj){
//        printf("%f %f\n",c[k][0],c[k][1]);
//        uy[npt] = c[k][1];
//        ux[npt] = c[k][0];
//        npt += 1; //  fastest with fabs
//
//        if(npt>=1000) printf("********** Problem");
//      }
//    }
////    sort2(10, r1,r2);
////    linefitTS(uy, ux, uw, npt, &beta1, &beta2, &mae);
//    // --------------------------
//    // 1. Line fitting
//    float m2,c2;
//    linefit(ux,uy,npt,&m2,&c2);
//    printf("npt = %d, m2=%f c2=%f\n",npt,m2,c2);
//    printf("\n");
//
//    // --------------------------
//    // 2. Generate gradient
//    float py[1000],px[1000];
//    int pb[1000]; // bitmap intensity
//    int np=0;
//    int marg = 20;
//    for(i=marg;i<IMW-marg;i++){
//      // check that y is inbound as x is already bounded
//      float y = i*m2 + c2;
//      if(y>marg && y<=IMW-marg){
//        px[np] = i;
//        py[np] = y;
//        pb[np] = _g2[i + (int)roundf(y)*IMW];
//
//        // draw line for check
//        _g2[i + (int)roundf(y)*IMW] = 220;
//        if(np>=1000) printf("********** Problem");
//        if((i + (int)roundf(y)*IMW)>=SQ512) printf("********** Problem");
//
////        _g2[(int)roundf(i + 100*IMW)] = 255;
//        printf("gradient: %d %f %f %d\n",np,px[np],py[np],pb[np]);
//        np++;
//      }else continue;
//
//    }
//
//    // --------------------------
//    // 3. Detect long unbroken segment
//    int thres_gap = 50, thres_width = 120, thres_density = 0.6;
//    int ilast1 = 0, count0 = 0, istart,iend,len;
//
//    // Initialization
//    pb[0]    =128; // easier on algo - help initialize ilast1
//    printf("m %d np %d\n",m,np);
//
////    pb[np-1] =128; // one last check on last bit - this one causes crash!!!
////
////    // Main loop
////    for(i=0;i<np;i++){
////      if(pb[i]==0) count0++;
////      else{
////        // '1' is detected
////        if(count0>50 || i==np-1){
////          printf("Long break detected\n");
////          istart = ilast1;   // first occurrence of 1 after long break
////          iend   = i-count0; // last occurrence of 1 after long break
////          len    = iend - istart ;
////
////          // Get longest minor breaks of 0
////          int lbreak =0, c0=0, n1=0;
////          for(j=istart;j<iend;j++){
////            if(pb[j]==0) c0++;
////            else{
////              lbreak = MAX(c0,lbreak);
////              c0 = 0;
////              n1++;
////            }
////          }
////
////          float den = len>0? (float)n1/len:0;
////          printf("len=%d, den=%f,lbreak=%d\n",len,den,lbreak);
////
////
////          // first occurrnece of 1 after a long break
////          ilast1 = i;
////        }
////        count0 = 0;
////      }
////
////    } //np
//
//
//
//    // ==================================
////    break; // for testing only - no loop
//  }
//  float m2,c2;
//  linefit(r1,r2,50,&m2,&c2);
//  float sigx,sigy,a2,b,siga,sigb,chi2,q;
//  fitexy(r1, r2, 10, &sigx, &sigy, &a2, &b, &siga, &sigb, &chi2, &q);
//  sort2(nc, M, a);
  
  
  
  
  
  
  
  
  
  // Pixel repositioning to Rotate - efficient - must use 2 different buffers, otherwise overwrite.
//  vImage_Buffer src  = { _g2, IMW, IMW, IMW };
//  vImage_Buffer dest = { _g3, IMW, IMW, IMW };
//  vImage_Error err = vImageRotate90_Planar8(&src, &dest, 3, 0, kvImageNoError); // 1-rotate 90, 3-rotate 270
  
//  if(check){
//    NSLog(@"Check Image");
//    UIImage *uiImg1 = [ICG processFrameToImage:_g1 setWidth:IMW setHeight:IMW setBytesPerRow:IMW setImageColor:NO];
//    [_iv1 initWithImage:uiImg1];
//
//    UIImage *uiImg2 = [ICG processFrameToImage:_g2 setWidth:IMW setHeight:IMW setBytesPerRow:IMW setImageColor:NO];
//    [_iv2 initWithImage:uiImg2];
//  }
  
}
- (IBAction)buttonUT4:(id)sender {
}
- (IBAction)buttonUT5:(id)sender {
}
- (IBAction)buttonUT6:(id)sender {
}
- (IBAction)buttonUT7:(id)sender {
}
- (IBAction)buttonUT8:(id)sender {
}
- (IBAction)buttonUT9:(id)sender {
}

@end
