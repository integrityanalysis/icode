/*
     File: SquareCamViewController.m
 Abstract: Dmonstrates iOS 5 features of the AVCaptureStillImageOutput class
  Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */

#import "SquareCamViewController.h"
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import <AssertMacros.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Constants.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ICG.h"
#import "imViewController.h"
#import "ICWebViewController.h"
#import "UIImage+Resize.h"
#include "ic_engine.h"
#pragma mark-

// used for KVO observation of the @"capturingStillImage" property to perform flash bulb animation
static const NSString *AVCaptureStillImageIsCapturingStillImageContext = @"AVCaptureStillImageIsCapturingStillImageContext";

static CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

static void ReleaseCVPixelBuffer(void *pixel, const void *data, size_t size);
static void ReleaseCVPixelBuffer(void *pixel, const void *data, size_t size) 
{	
	CVPixelBufferRef pixelBuffer = (CVPixelBufferRef)pixel;
	CVPixelBufferUnlockBaseAddress( pixelBuffer, 0 );
	CVPixelBufferRelease( pixelBuffer );
}

// create a CGImage with provided pixel buffer, pixel buffer must be uncompressed kCVPixelFormatType_32ARGB or kCVPixelFormatType_32BGRA
static OSStatus CreateCGImageFromCVPixelBuffer(CVPixelBufferRef pixelBuffer, CGImageRef *imageOut);
static OSStatus CreateCGImageFromCVPixelBuffer(CVPixelBufferRef pixelBuffer, CGImageRef *imageOut) 
{	
	OSStatus err = noErr;
	OSType sourcePixelFormat;
	size_t width, height, sourceRowBytes;
	void *sourceBaseAddr = NULL;
	CGBitmapInfo bitmapInfo;
	CGColorSpaceRef colorspace = NULL;
	CGDataProviderRef provider = NULL;
	CGImageRef image = NULL;
	
	sourcePixelFormat = CVPixelBufferGetPixelFormatType( pixelBuffer );
	if ( kCVPixelFormatType_32ARGB == sourcePixelFormat )
		bitmapInfo = kCGBitmapByteOrder32Big | kCGImageAlphaNoneSkipFirst;
	else if ( kCVPixelFormatType_32BGRA == sourcePixelFormat )
		bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaNoneSkipFirst;
	else
		return -95014; // only uncompressed pixel formats
	
	sourceRowBytes = CVPixelBufferGetBytesPerRow( pixelBuffer );
	width = CVPixelBufferGetWidth( pixelBuffer );
	height = CVPixelBufferGetHeight( pixelBuffer );
	
	CVPixelBufferLockBaseAddress( pixelBuffer, 0 );
	sourceBaseAddr = CVPixelBufferGetBaseAddress( pixelBuffer );
	
	colorspace = CGColorSpaceCreateDeviceRGB();
    
	CVPixelBufferRetain( pixelBuffer );
	provider = CGDataProviderCreateWithData( (void *)pixelBuffer, sourceBaseAddr, sourceRowBytes * height, ReleaseCVPixelBuffer);
	image = CGImageCreate(width, height, 8, 32, sourceRowBytes, colorspace, bitmapInfo, provider, NULL, true, kCGRenderingIntentDefault);
	
bail:
  {
	if ( err && image ) {
		CGImageRelease( image );
		image = NULL;
	}
	if ( provider ) CGDataProviderRelease( provider );
	if ( colorspace ) CGColorSpaceRelease( colorspace );
	*imageOut = image;
	return err;
  }
}

// utility used by newSquareOverlayedImageForFeatures for 
static CGContextRef CreateCGBitmapContextForSize(CGSize size);
static CGContextRef CreateCGBitmapContextForSize(CGSize size)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    int             bitmapBytesPerRow;
	
    bitmapBytesPerRow = (size.width * 4);
	
    colorSpace = CGColorSpaceCreateDeviceRGB();
    context = CGBitmapContextCreate (NULL,
									 size.width,
									 size.height,
									 8,      // bits per component
									 bitmapBytesPerRow,
									 colorSpace,
									 kCGImageAlphaPremultipliedLast);
	CGContextSetAllowsAntialiasing(context, NO);
    CGColorSpaceRelease( colorSpace );
    return context;
}

#pragma mark-

@interface UIImage (RotationMethods)
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
@end

@implementation UIImage (RotationMethods)

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees 
{   
	// calculate the size of the rotated view's containing box for our drawing space
	UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.size.width, self.size.height)];
	CGAffineTransform t = CGAffineTransformMakeRotation(DegreesToRadians(degrees));
	rotatedViewBox.transform = t;
	CGSize rotatedSize = rotatedViewBox.frame.size;
//	[rotatedViewBox release];
	
	// Create the bitmap context
	UIGraphicsBeginImageContext(rotatedSize);
	CGContextRef bitmap = UIGraphicsGetCurrentContext();
	
	// Move the origin to the middle of the image so we will rotate and scale around the center.
	CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
	
	//   // Rotate the image context
	CGContextRotateCTM(bitmap, DegreesToRadians(degrees));
	
	// Now, draw the rotated/scaled image into the context
	CGContextScaleCTM(bitmap, 1.0, -1.0);
	CGContextDrawImage(bitmap, CGRectMake(-self.size.width / 2, -self.size.height / 2, self.size.width, self.size.height), [self CGImage]);
	
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
	
}

@end

#pragma mark-

@interface SquareCamViewController (InternalMethods)
- (void)setupAVCapture;
- (void)teardownAVCapture;
- (void)drawFaceBoxesForFeatures:(NSArray *)features forVideoBox:(CGRect)clap orientation:(UIDeviceOrientation)orientation;
@end

@implementation SquareCamViewController

- (void)setupAVCapture
{
	NSError *error = nil;
	
	AVCaptureSession *session = [AVCaptureSession new];
//  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
//	    [session setSessionPreset:AVCaptureSessionPreset640x480];
////      [session setSessionPreset:AVCaptureSessionPresetPhoto];
//
//  }else{
//	    [session setSessionPreset:AVCaptureSessionPresetPhoto];
//  }
  // Always 1280x720, back camera only - supported since iPhone 4S.
//  [session setSessionPreset:AVCaptureSessionPreset1280x720];
  NSNumber* formatValue;
  if(THINCODE==0){
    if(captureMode==0){
      // BGRA, 640x480
      [session setSessionPreset:AVCaptureSessionPreset640x480];
      formatValue = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    }else{
      // YCBCR, 1280x720
      [session setSessionPreset:AVCaptureSessionPreset1280x720];
      formatValue = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange];
    }
  }else{
    // Thin code uses full HD - 1920x1080, also available 3840x2160
    [session setSessionPreset:AVCaptureSessionPreset1920x1080]; // Full HD
//    [session setSessionPreset:AVCaptureSessionPreset3840x2160]; // 4k
    formatValue = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_420YpCbCr8BiPlanarFullRange];
  }

    // Select a video device, make an input
	AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
//  require( error == nil, bail );
  if (error == nil){
    
  isUsingFrontFacingCamera = NO;
	if([session canAddInput:deviceInput]) [session addInput:deviceInput];
	
    // Make a video data output
	videoDataOutput = [AVCaptureVideoDataOutput new];
  
  // we want BGRA, both CoreGraphics and OpenGL work well with 'BGRA'
	NSDictionary *videoSettings = [NSDictionary dictionaryWithObject: formatValue
                                                            forKey: (id)kCVPixelBufferPixelFormatTypeKey];
  
  // YUV format works too, even with Face detection.
  // Not sure about JPEG export speed
	[videoDataOutput setVideoSettings:videoSettings];
	[videoDataOutput setAlwaysDiscardsLateVideoFrames:YES]; // discard if the data output queue is blocked (as we process the still image)
    
    // create a serial dispatch queue used for the sample buffer delegate as well as when a still image is captured
    // a serial dispatch queue must be used to guarantee that video frames will be delivered in order
    // see the header doc for setSampleBufferDelegate:queue: for more information
	videoDataOutputQueue = dispatch_queue_create("VideoDataOutputQueue", DISPATCH_QUEUE_SERIAL);
	[videoDataOutput setSampleBufferDelegate:self queue:videoDataOutputQueue];
	
    if ( [session canAddOutput:videoDataOutput] )
		[session addOutput:videoDataOutput];
	[[videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:NO];
	
	effectiveScale = 1.0;
	previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
	[previewLayer setBackgroundColor:[[UIColor blackColor] CGColor]];
	[previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
	CALayer *rootLayer = [previewView layer];
	[rootLayer setMasksToBounds:YES];
	[previewLayer setFrame:[rootLayer bounds]];
	[rootLayer addSublayer:previewLayer];
  
  // ----------
  [self getCam];
  // ----------

	[session startRunning];
  } else
//bail:
  {
//	[session release];
	if (error) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Failed with error %d", (int)[error code]]
															message:[error localizedDescription]
														   delegate:nil 
												  cancelButtonTitle:@"Dismiss" 
												  otherButtonTitles:nil];
		[alertView show];
//		[alertView release];
		[self teardownAVCapture];
	}
  }
}

// clean up capture setup
- (void)teardownAVCapture
{
//	[videoDataOutput release];
	if (videoDataOutputQueue)
//		dispatch_release(videoDataOutputQueue);
	[stillImageOutput removeObserver:self forKeyPath:@"isCapturingStillImage"];
//	[stillImageOutput release];
	[previewLayer removeFromSuperlayer];
//	[previewLayer release];
}

// perform a flash bulb animation using KVO to monitor the value of the capturingStillImage property of the AVCaptureStillImageOutput class
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  
  if( [keyPath isEqualToString:@"adjustingFocus"] ){
    BOOL adjustingFocus = [ [change objectForKey:NSKeyValueChangeNewKey] isEqualToNumber:[NSNumber numberWithInt:1] ];
    NSLog(@"keypath ============ Is adjusting focus? %@", adjustingFocus ? @"YES" : @"NO" );
    if(!adjustingFocus){
      [self forceFocus];
//      [self spinLogo: 1]; // fast spin
    }
  }
  
  static float lensPositionLast = 0;
  if ([keyPath isEqualToString:@"lensPosition"]) {
    // Use change in lens position to determine spin.
    float lensPosition = [change[@"new"] floatValue];
    float lensChange   = lensPosition - lensPositionLast;
    
    // Big change - fast spin, else slow spin.
    if(fabs(lensChange)>0.05) [self spinLogo: 1]; // fast spin
    
//    NSLog(@"keypath lens position: %f, change %f", lensPosition,lensChange);
    lensPositionLast = lensPosition;
  }
  
//	if ( context == AVCaptureStillImageIsCapturingStillImageContext ) {
//		BOOL isCapturingStillImage = [[change objectForKey:NSKeyValueChangeNewKey] boolValue];
//		
//		if ( isCapturingStillImage ) {
//			// do flash bulb like animation
//			flashView = [[UIView alloc] initWithFrame:[previewView frame]];
//			[flashView setBackgroundColor:[UIColor whiteColor]];
//			[flashView setAlpha:0.f];
//			[[[self view] window] addSubview:flashView];
//			
//			[UIView animateWithDuration:.4f
//							 animations:^{
//								 [flashView setAlpha:1.f];
//							 }
//			 ];
//		}
//		else {
//			[UIView animateWithDuration:.4f
//							 animations:^{
//								 [flashView setAlpha:0.f];
//							 }
//							 completion:^(BOOL finished){
//								 [flashView removeFromSuperview];
//								 [flashView release];
//								 flashView = nil;
//							 }
//			 ];
//		}
//	}
}

// utility routing used during image capture to set up capture orientation
- (AVCaptureVideoOrientation)avOrientationForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
	AVCaptureVideoOrientation result = deviceOrientation;
	if ( deviceOrientation == UIDeviceOrientationLandscapeLeft )
		result = AVCaptureVideoOrientationLandscapeRight;
	else if ( deviceOrientation == UIDeviceOrientationLandscapeRight )
		result = AVCaptureVideoOrientationLandscapeLeft;
	return result;
}

// utility routine to display error aleart if takePicture fails
- (void)displayErrorOnMainQueue:(NSError *)error withMessage:(NSString *)message
{
	dispatch_async(dispatch_get_main_queue(), ^(void) {
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%@ (%d)", message, (int)[error code]]
															message:[error localizedDescription]
														   delegate:nil 
												  cancelButtonTitle:@"Dismiss" 
												  otherButtonTitles:nil];
		[alertView show];
//		[alertView release];
	});
}



// find where the video box is positioned within the preview layer based on the video size and gravity
+ (CGRect)videoPreviewBoxForGravity:(NSString *)gravity frameSize:(CGSize)frameSize apertureSize:(CGSize)apertureSize
{
    CGFloat apertureRatio = apertureSize.height / apertureSize.width;
    CGFloat viewRatio = frameSize.width / frameSize.height;
    
    CGSize size = CGSizeZero;
    if ([gravity isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
        if (viewRatio > apertureRatio) {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        } else {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        }
    } else if ([gravity isEqualToString:AVLayerVideoGravityResizeAspect]) {
        if (viewRatio > apertureRatio) {
            size.width = apertureSize.height * (frameSize.height / apertureSize.width);
            size.height = frameSize.height;
        } else {
            size.width = frameSize.width;
            size.height = apertureSize.width * (frameSize.width / apertureSize.height);
        }
    } else if ([gravity isEqualToString:AVLayerVideoGravityResize]) {
        size.width = frameSize.width;
        size.height = frameSize.height;
    }
	
	CGRect videoBox;
	videoBox.size = size;
	if (size.width < frameSize.width)
		videoBox.origin.x = (frameSize.width - size.width) / 2;
	else
		videoBox.origin.x = (size.width - frameSize.width) / 2;
	
	if ( size.height < frameSize.height )
		videoBox.origin.y = (frameSize.height - size.height) / 2;
	else
		videoBox.origin.y = (size.height - frameSize.height) / 2;
    
	return videoBox;
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
       fromConnection:(AVCaptureConnection *)connection
{
  
  // *** Note *** Use tabBarController selection to skip image operation.
  // "Scan" view is index 0.
  // Still energy intensive. Probably can put it somewhere else to save energy.
 //  printf("selected index %d\n",self.tabBarController.selectedIndex);
  if(self.tabBarController.selectedIndex!=0 || self.recordOn==NO) return;
  
  
	// got an image
	CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CFDictionaryRef attachments  = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
	CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
	if (attachments)
		CFRelease(attachments);
	NSDictionary *imageOptions = nil;
	UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
	int exifOrientation;
  
  
	
    /* kCGImagePropertyOrientation values
        The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
        by the TIFF and EXIF specifications -- see enumeration of integer constants. 
        The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
        
        used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
        If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
        
	enum {
		PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
		PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.  
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.  
		PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.  
		PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.  
		PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.  
		PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.  
		PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.  
	};
	
	switch (curDeviceOrientation) {
		case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
			exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
			break;
		case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			break;
		case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
			if (isUsingFrontFacingCamera)
				exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
			else
				exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
			break;
		case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
		default:
			exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
			break;
	}

  // Don't run Engine if not _recordOn and minTimeLapse have passed
  double minTimeLapse = 1.0;
  if(_recordOn == NO || (CACurrentMediaTime() - _recordOnAt)<minTimeLapse) return;
  
  algo.exifOrientation = exifOrientation;
  // Moved to here so Face features can be passed too.
  int found = [algo captureOutput : captureOutput
                 didOutputSampleBuffer : sampleBuffer
                        fromConnection : connection
                              features : nil];
  
  static int myCount=0;
  myCount++;
  if(db_showPlot){
    // **************************
    // Radius plot
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    dict[@"x"]    = @[@(myCount%5),@(2),@(3),@(4),@(5)];
//    dict[@"y"]    = @[@(10),@(20),@(30),@(40),@(50)];
    
    NSMutableArray *x = [NSMutableArray array];
    NSMutableArray *y = [NSMutableArray array];
    for(int i=0;i<LK.nPlotPoint;i++){
      x[i] = @(LK.x[i]);
      y[i] = @(LK.y[i]);
    }
    dict[@"x"]    = x;
    dict[@"y"]    = y;
    
    
    dict[@"xlim"] = @[@(0),@(NX)];
    dict[@"ylim"] = @[@(0),@(100)];
    dict[@"grid"] = @[@(20),@(20)];
    dict[@"title"] = [NSString stringWithFormat:@"%s",LK.plotComment];
    _vwPlot.d = dict;
 
    // **************************
    // FFT heatmap
//    UIImage *heatmap = [ICG heatmap:LK.heatmap setWidth:RF/2+1 setHeight:RF setMinValue:0 setMaxValue:100];
    UIImage *heatmap = [ICG heatmap:LK.heatmap setWidth:NX setHeight:NX2 setMinValue:0 setMaxValue:100];
    
    // Drawing code - refresh
    dispatch_async(dispatch_get_main_queue(), ^(void) {
      [_vwPlot setNeedsDisplay];
      [_ivHeatmap setImage:heatmap];
    });
  
  }
  
  // Progress view update - very simple - just (peak-average)/100
  static float pv=0;
  int iRadBest = LK.iRadBest[0];
  float a = 0.5;
  float sum=0;
  for(int i=0;i<LK.nPlotPoint;i++) sum+=LK.y[i];
  pv      = a*pv + (1-a)*(LK.y[iRadBest]-sum/LK.nPlotPoint)/50;
  dispatch_async(dispatch_get_main_queue(), ^(void) {
    [_pvSignalIndicator setProgress:pv animated:NO ];

  });
  
  if(found>=0 || found==-2){
    [self updateUI : found
             image : algo.latestCaptureImage]; // pass code index
  }else{
    if([ICG sharedICG].testMode){
      dispatch_async(dispatch_get_main_queue(), ^(void) {
        _lbSignal.text = [NSString stringWithFormat:@"%019llu",LK.codeLatest];
      });
    }
  }
  
    // get the clean aperture
    // the clean aperture is a rectangle that defines the portion of the encoded pixel dimensions
    // that represents image data valid for display.
//	CMFormatDescriptionRef fdesc = CMSampleBufferGetFormatDescription(sampleBuffer);
//	CGRect clap = CMVideoFormatDescriptionGetCleanAperture(fdesc, false /*originIsTopLeft == false*/);
//	
//	dispatch_async(dispatch_get_main_queue(), ^(void) {
//		[self drawFaceBoxesForFeatures:features forVideoBox:clap orientation:curDeviceOrientation];
//	});
}




- (void) getCam {
  
  AVCaptureDevicePosition desiredPosition = AVCaptureDevicePositionBack;
  
  for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
    if ([d position] == desiredPosition) {
      
      [[previewLayer session] beginConfiguration];
      AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
      for (AVCaptureInput *oldInput in [[previewLayer session] inputs]) {
        [[previewLayer session] removeInput:oldInput];
      }
      [[previewLayer session] addInput:input];
      [[previewLayer session] commitConfiguration];
      
      // Right place to adjust Frame Rate
//      [self configureCameraForHighestFrameRate:d];
      
      NSLog(@"device active format %@",d.activeFormat);
      if ( [d lockForConfiguration:NULL] == YES ) {
        
        // If not fixed frame rate, iPhone will vary between 16-30fps according to lighting
        int fps = 20;
        d.activeVideoMinFrameDuration = CMTimeMake(1, fps);
        d.activeVideoMaxFrameDuration = CMTimeMake(1, fps);
        
        // http://stackoverflow.com/questions/15782083/set-an-initial-focal-distance-on-ios
        // If we are on an iOS version that supports AutoFocusRangeRestriction and the device supports it
        // Set the focus range to "near"
        if ([d respondsToSelector:@selector(isAutoFocusRangeRestrictionSupported)] && d.autoFocusRangeRestrictionSupported && FOCUS_NEAR) {
          d.autoFocusRangeRestriction = AVCaptureAutoFocusRangeRestrictionNear;
        }
        [d unlockForConfiguration];
      }
      
      break;
    }
  }
  
  
}

- (void)handlePinchZoom:(UIPinchGestureRecognizer *)pinchRecognizer
{
  
  // https://stackoverflow.com/questions/35841390/zoom-in-and-zoom-out-on-camera-view-using-objective-c
  // AVCaptureDevice *device =[AVCaptureDevicedefaultDeviceWithMediaType:AVMediaTypeVideo];
  AVCaptureDevice *device ;
  AVCaptureDevicePosition desiredPosition = AVCaptureDevicePositionBack;
    
  for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
    if ([d position] == desiredPosition) {
      device = d;
      break;
    }
  }
  
  AVCaptureDeviceFormat *format = device.activeFormat;
  CGFloat maxZoomFactor = format.videoMaxZoomFactor;
  NSArray *formats = device.formats;
  const CGFloat pinchVelocityDividerFactor = 20.0f;
  if (pinchRecognizer.state == UIGestureRecognizerStateChanged || pinchRecognizer.state ==UIGestureRecognizerStateBegan)
  {
     NSError *error = nil;
     if ([device lockForConfiguration:&error])
     {
        CGFloat desiredZoomFactor = device.videoZoomFactor +
        atan2f(pinchRecognizer.velocity, pinchVelocityDividerFactor);

        device.videoZoomFactor = MAX(1.0, MIN(desiredZoomFactor,
                                       device.activeFormat.videoMaxZoomFactor));
        [device unlockForConfiguration];
     }
    else
    {
      NSLog(@"error: %@", error);
    }
  }
 }

- (void) setzoom: (float) zoomFactor
{
 
 // https://stackoverflow.com/questions/35841390/zoom-in-and-zoom-out-on-camera-view-using-objective-c
 // AVCaptureDevice *device =[AVCaptureDevicedefaultDeviceWithMediaType:AVMediaTypeVideo];
 AVCaptureDevice *device ;
 AVCaptureDevicePosition desiredPosition = AVCaptureDevicePositionBack;
   
 for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
   if ([d position] == desiredPosition) {
     device = d;
     break;
   }
 }

  
  NSError *error = nil;
  if ([device lockForConfiguration:&error])
  {
    device.videoZoomFactor = zoomFactor;
    [device unlockForConfiguration];
  }
 else
 {
   NSLog(@"error: %@", error);
 }

}




// Force Focus is called when (1) viewDidAppear, (2) autoFocus stops.
- (void) forceFocus {
//  if(THINCODE) return;
  if([ICG sharedICG].noForceFocus>0) return;
  
  AVCaptureDevicePosition desiredPosition = AVCaptureDevicePositionBack;
  for (AVCaptureDevice *d in [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo]) {
    if ([d position] == desiredPosition) {
      NSLog(@"Force Focus: device active format %@",d.activeFormat);
      if ( [d lockForConfiguration:NULL] == YES ) {
        
        // Force re-focus at the center - great reference
        // http://stackoverflow.com/questions/15782083/set-an-initial-focal-distance-on-ios
        if ([d isFocusPointOfInterestSupported]){
          [d setFocusPointOfInterest:CGPointMake(0.5f,0.5f)];
          [d setFocusMode:AVCaptureFocusModeAutoFocus];
//          [d setFocusMode: AVCaptureFocusModeContinuousAutoFocus]; // only for iPhone 11 (may be X)
        }
      break;
      }
    }
  }
}

- (void) spinLogo: (int) action {
  // Parameters - starting and ending alpha, rotation period, imageView to be rotated.
  float alphaLow = 0.1,alphaHi = 0.5, rotSpeedFast =4;
  UIImageView *imRotate = _ivSpin;
  
  // https://www.shareicon.net/dotted-circle-pattern-shapes-dots-dot-earth-icons-circles-symbol-circular-675443
  
  float rotSpeedSlow = .25*rotSpeedFast;
  UIViewAnimationOptions option;
  float rotSpeed;
  float alphaChange=0;
  static float alpha0,alpha1;
  static int speedup = 0;
  static int spinInProgress=0;
  
  // action 6: Fast - set speed up flag. Doesn't proceed to call animation.
  option = UIViewAnimationOptionCurveLinear;
  if(action==1){
    speedup     = 1;
    return;
  }
  // Forbid calling spin animation when already spinning
  if(spinInProgress==1){
    return;
  }
  
  // Calling animation: 0 (start or continue in slow), 1 (speed up)
  if(speedup){
    rotSpeed    = rotSpeedFast ;
    alphaChange = +0.05;
    speedup     = 0; // only place to reset speedup to 0
  }else{
    rotSpeed    = rotSpeedSlow;
    alphaChange = -0.4;
  }
  
  // Apply animation
  spinInProgress = 1; // do not allow another spin animation until this one is over.
  imRotate.alpha = alpha0;
  alpha0         = MAX(MIN(alpha0+alphaChange,alphaHi),alphaLow);
  dispatch_async(dispatch_get_main_queue(), ^{
    [UIView animateWithDuration: 0.2f
                          delay: 0.0f
                        options: option
                     animations: ^{
                       imRotate.transform = CGAffineTransformRotate(imRotate.transform, rotSpeed*3.14/8.0f);
                       imRotate.alpha = alpha0;
                     }
                     completion:^(BOOL finished) {
                       spinInProgress = 0; // reset spinInProgress - allow call again
                       [self spinLogo:0];
                     }];
    
  });

}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
  
}
- (IBAction)buttonLight:(id)sender {
  // Flash Light
  AVCaptureDevice *flashLight = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
  if ([flashLight isTorchAvailable] && [flashLight isTorchModeSupported:AVCaptureTorchModeOn]) {
    BOOL success = [flashLight lockForConfiguration:nil];
    if (success) {
      if ([flashLight isTorchActive]) {
        [flashLight setTorchMode:AVCaptureTorchModeOff];
      } else {
        [flashLight setTorchMode:AVCaptureTorchModeOn];
      }
      [flashLight unlockForConfiguration];
    }
  }
}
#pragma mark ========== Unit Tests =====
- (IBAction)buttonUT1:(id)sender {

  // Play a sound
  AudioServicesPlaySystemSound(1109);
  
  // Set viThumbnail to an image
//  NSString *workSpacePath=[docDir stringByAppendingPathComponent:@"ztmp_8305537995105307236.jpg"];
////  UIImageView *myimage=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,20,20)];
//  UIImage *image=[UIImage imageWithData:[NSData dataWithContentsOfFile:workSpacePath]];
//  
//  dispatch_async(dispatch_get_main_queue(), ^(void) {
//    [_ivThumbnail setImage: image];
//  });
//  
//  // Change to index 3 tabBarController
//  self.tabBarController.selectedIndex = 3;
  
  

}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
  // not equivalent to image.size (which depends on the imageOrientation)!
  double refWidth  = CGImageGetWidth(image.CGImage);
  double refHeight = CGImageGetHeight(image.CGImage);
  
  double x = (refWidth - size.width) / 2.0;
  double y = (refHeight - size.height) / 2.0;
  
  CGRect cropRect = CGRectMake(x, y, size.height, size.width);
  CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
  
  UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:0.0 orientation:image.imageOrientation];
  CGImageRelease(imageRef);
  
  return cropped;
}

- (void) updateUI : (int) index
            image : (UIImage *) latestCaptureImage {
  if(_recordOn == NO) return;
  _recordOn = NO;
  Code *code;
  if(index>=0){
    // iCode
    code = [[ICG sharedICG].arrayCode[index] copy]; // must use "copy" for deep copy
  }else{
    // Bar code
    code = [[Code alloc] init];
    code.Code     = 0;
    code.ClientId = @"";
    code.Desc = @"";//algo.latestBarcode[@"format"];
    NSString *url = algo.latestBarcode[@"contents"];
    if(![url.lowercaseString hasPrefix:@"http"]){
      url = [NSString stringWithFormat:@"https://www.google.com/search?q=%@",url];
    }
    code.Url  = url;
  }
  code.ScannedAt = [[ICG sharedICG] getNowDateString];
  [[ICG sharedICG].arrayScan insertObject:code atIndex:0];
  
  // Define "active client id"
  [ICG sharedICG].clientId = code.ClientId;
//  for(int i=0;i<100000;i++){
//  NSDictionary *dict = @{@"code": code.Code,
//                      @"address": code.Url,
//                        @"title": code.Desc,
//                   @"thumbnail" : code.Thumbnail};
//  [[ICG sharedICG].arrayScan insertObject:dict atIndex:0];
//  if([ICG sharedICG].arrayScan.count>5) [[ICG sharedICG].arrayScan removeLastObject];
  
//  }
  printf("Array length %d \n", (int)[ICG sharedICG].arrayScan.count );
//    [[ICG sharedICG].arrayScan addObject:dict];
  
  
  // For testing only
  if([ICG sharedICG].testMode){
//    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfFile:workSpacePath]];
    UIImage *image = latestCaptureImage; // [UIImage imageNamed:@"refresh.png"];
    
    dispatch_async(dispatch_get_main_queue(), ^(void) {
      // Show Image, make sound, display code
      [_ivThumbnail setImage: image];
      AudioServicesPlaySystemSound(1011); // vibration only
      _lbCode.text   = [NSString stringWithFormat:@"%@.jpg",code.Code]; // show code
      _lbSignal.text = code.Desc;
      
      if(THINCODE==1) _lbCode.text = [NSString stringWithFormat:@"%d",[ICG sharedICG].tc_code ];
    });
    
    // We need set recordOn here because we do not leave the viewController.
    _recordOn = YES; // do we need this?????
    _recordOnAt = CACurrentMediaTime();// do we need this?????
  }else{

    dispatch_async(dispatch_get_main_queue(), ^(void) {
      
      // ----- Display Image Only - imViewController
//      imViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"imViewController"];
////      wvc.address  = file;
//      wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
//      [self.navigationController pushViewController:wvc animated:YES];
      
      ICWebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
      wvc.code      = code;
      
      // Crop Image into Square, then save to JPEG.
      // Name file as picXXX.jpg where XXX is code.ScannedAt
      UIImage *imSq = [latestCaptureImage thumbnailImage:256
                                              transparentBorder:0
                                                   cornerRadius:10
                                           interpolationQuality:kCGInterpolationHigh];
      
      NSString *f = [docDir stringByAppendingPathComponent:ptf(@"pic%@.jpg",code.ScannedAt)];
      [UIImageJPEGRepresentation(imSq,0.9f) writeToFile:f atomically:YES];
       
//      wvc.thumbnail = imSq;//latestCaptureImage;
      wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
      [self.navigationController pushViewController:wvc animated:YES];
      
      
      // Save to user default
      [[ICG sharedICG] saveUserDefaults : @"scan"];
      
      // Play a sound
      AudioServicesPlaySystemSound(1110);
      
      
//      [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"test.jpg"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
    });

  }
  
//  NSLog(@"arrayScan %@",[ICG sharedICG].arrayScan);
 

}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  
  // If using Simulator, don't go further.
  if(runSimulator) return;
  
  if(THINCODE){
    UIImage *im = [UIImage imageNamed:@"crosshair01.png"];
    [_ivBracket setImage:im];
    
  }
  
  // TEMPORARY: hide unneeded tab items
  if (TEST_SUBMIT) {
    NSMutableArray *tabs = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
    [tabs removeObjectAtIndex:4];
    [tabs removeObjectAtIndex:2];
    [self.tabBarController setViewControllers:tabs];
  }

  
  // Setup AV Capture, Feature Detector, and algo object.
	[self setupAVCapture];
  
  // ----------
  algo = [[SCAlgo alloc] init];
  algo.arrPhoto     = [[NSMutableArray alloc] init];
  algo.arrScore     = [[NSMutableArray alloc] init];
  algo.arrThumbnail = [[NSMutableArray alloc] init];
  algo.arrSelected  = [[NSMutableArray alloc] init];
  algo.photoId      = -1;
  _readyForStillImage = YES;
  
  self.title = @"Scan";
  
  // If demo release, hide the "Help"
  if(db_demoRelease){
    NSMutableArray *tbViewControllers = [NSMutableArray arrayWithArray:[self.tabBarController viewControllers]];
    [tbViewControllers removeObjectAtIndex:3];
    [self.tabBarController setViewControllers:tbViewControllers];
  }
  
  // TestMode=0: normal, 1: showPlot and do not go to Item menu
  if(db_testMode==1){
    [ICG sharedICG].showPlot = 1;
    [ICG sharedICG].testMode = 1;
  }
//  for(int i=0;i<100;i++){
//  NSDictionary *dict = @{@"address":@"https://www.swingprofile.com",@"title":@"Swing Profile"};
//  
//  [[ICG sharedICG].arrayScan insertObject:dict atIndex:0];
////  NSLog(@"ICG data %@",[ICG sharedICG].arrayScan);
//  }
  
  // Add Pinch gesture recognizer
  UIPinchGestureRecognizer *pinch =[[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchZoom:)];
  [pinch setDelegate:self];
  [self.view addGestureRecognizer:pinch];
  
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
  
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
  [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
  
  // Only place to switch on _recordOn (except for test above)
  _recordOn   = YES;
  _recordOnAt = CACurrentMediaTime(); // timer
  
  // Register focus KVO observer
  AVCaptureDevice *camDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
  int flags = NSKeyValueObservingOptionNew;
  [camDevice addObserver:self
              forKeyPath:@"adjustingFocus"
                 options:flags
                 context:nil];
  [camDevice addObserver:self
              forKeyPath:@"lensPosition"
                 options:NSKeyValueObservingOptionNew
                 context:nil];
  
  // Change navigtionBar according to Client Id
  if([[ICG sharedICG].clientId isEqual:@"JBH"]){
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navbar_jb.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
  }else{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
  }
  
  
  

}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  if(THINCODE){
//  dispatch_async(dispatch_get_main_queue(), ^(void){
    // Preview Enlarge, if required
//    float sc=1.5;
//    previewView.transform = CGAffineTransformMakeScale(sc, sc);
//  });
    // Manually set zoom
    [self setzoom:1.5];
  }
  _viewUnitTest.hidden = YES;
  [previewView bringSubviewToFront:_ivBracket];
  
  _buttonLight.hidden = NO;
  [previewView bringSubviewToFront:_buttonLight];
  

  [previewView bringSubviewToFront:_ivSpin];
  
  [previewView bringSubviewToFront:_pvSignalIndicator];
  
  _vwPlot.hidden    = YES;
  _ivHeatmap.hidden = YES;
  
  // Zeke: force start video
	[[videoDataOutput connectionWithMediaType:AVMediaTypeVideo] setEnabled:YES];
  if(db_viewDebug || [ICG sharedICG].testMode){
    [previewView bringSubviewToFront:_viewUnitTest];
    _viewUnitTest.hidden = NO;
  }
  if([ICG sharedICG].showPlot){
    [previewView bringSubviewToFront:_vwPlot];
    [previewView bringSubviewToFront:_ivHeatmap];
    _vwPlot.hidden    = NO;
    _ivHeatmap.hidden = NO;
  }
  
  // Must start the auto-focus going right in the beginning.
  // Then KVO will pick up focus status from there.
  // Only 1 of 2 places to force focus
  [self forceFocus];
  [self spinLogo: 0];
  
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
  _recordOn   = NO;
  
  // unregister focus KVO observer
  AVCaptureDevice *camDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
  [camDevice removeObserver:self forKeyPath:@"adjustingFocus"];
  [camDevice removeObserver:self forKeyPath:@"lensPosition"];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
