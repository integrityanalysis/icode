//
//  Code.m
//  SquareCam
//
//  Created by Eden Choi on 9/12/16.
//
//

#import <Foundation/Foundation.h>
#import "Code.h"
#import "Constants.h"

@interface Code()

@end

@implementation Code

+ (NSString *)dynamoDBTableName {
  return @"Codes7";
}

+ (NSString *)hashKeyAttribute {
  return @"IcRegion";
}

+ (NSString *)rangeKeyAttribute {
  return @"Code";
}

- (UIImage *) getImage {
  // Set thumbnail, if any
  //  _thumbnail = nil;
  //  if(_code.Thumbnail.length>0){
  //    NSData *decoded = [[NSData alloc] initWithBase64EncodedData:_code.Thumbnail options:NSDataBase64DecodingIgnoreUnknownCharacters];
  //    if(decoded.length>0){
  //      _thumbnail = [UIImage imageWithData:decoded];
  //    }
  //  }
  
  NSString *f = [docDir stringByAppendingPathComponent:ptf(@"pic%@.jpg",_ScannedAt)];
  UIImage *image    = [UIImage imageWithContentsOfFile:f];
  if(!image) image  = [UIImage imageNamed:@"refresh.png"];
  return image;
}

@end
