//
//  ICWebViewController.h
//  SquareCam 
//
//  Created by Eden Choi on 13/09/16.
//
//
#import "Code.h"
//#import <WebKit/WebKit.h> // not added wkwebview yet...

@interface ICWebViewController : UIViewController <UIWebViewDelegate>

@property (strong, nonatomic) NSString *websiteAddress;
@property (strong, nonatomic) NSString *websiteTitle;
@property (strong, nonatomic) UIImage  *thumbnail;
@property (strong, nonatomic) Code *code;
@property (assign, nonatomic) BOOL noAnimate;
@end
