/*
     File: SquareCamAppDelegate.m
 Abstract: Dmonstrates iOS 5 features of the AVCaptureStillImageOutput class
  Version: 1.0
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */

#import "SquareCamAppDelegate.h"
#import "Constants.h"
#import <AWSCore/AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import "ICG.h"
#import "Branch.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>



@implementation SquareCamAppDelegate

@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  [self.window setTintColor:[UIColor redColor]];
  
  // Initialise Branch.io
  Branch *branch = [Branch getInstance];
  [branch initSessionWithLaunchOptions:launchOptions andRegisterDeepLinkHandler:^(NSDictionary *params, NSError *error) {
    if (!error && params) {
      // params are the deep linked params associated with the link that the user clicked -> was re-directed to this app
      // params will be empty if no data found
      // ... insert custom logic here ...
      NSLog(@"params: %@", params.description);
    }
  }];
  
  // Initialise the Amazon Cognito credentials provider
  AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc]
                                                        initWithRegionType:AWSRegionUSWest2
                                                        identityPoolId:@"us-west-2:d097a0fd-5a23-4b6d-af32-6082375e21b1"];
  // Initialise the default service configuration
  AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSWest2 credentialsProvider:credentialsProvider];
  // Initialise the AWSDynamoDBObjectMapper service configuration to use AP-Southeast-2
  AWSServiceConfiguration *dynamoDBConfiguration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionAPSoutheast2 credentialsProvider:credentialsProvider];
  // Initialise AWSDynamoDBObjectMapperConfiguration
  AWSDynamoDBObjectMapperConfiguration *objMapperConfiguration = [[AWSDynamoDBObjectMapperConfiguration alloc] init];
  // Initialise AWSDynamoDBObjectMapper to use the right configuration
  [AWSDynamoDBObjectMapper registerDynamoDBObjectMapperWithConfiguration:dynamoDBConfiguration objectMapperConfiguration:objMapperConfiguration forKey:@"APSoutheast2DynamoDB"];

  
  [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;

  // Crashlytics
  if (!STOP_CRASHLYTICS) {
    [Fabric with:@[[Crashlytics class], [Branch class]]];
  }
  
  // Google Analytics
  if (!STOP_GOOGLEANALYTICS) {
    // Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    // Optional: configure GAI options.
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
  }

  
  // Setup ICG and shared arrays
  [[ICG sharedICG] setup];
  
  // Initiate with a different VC
  if(db_utInitVC){
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSString *vid = THINCODE==0? @"ut1VC" : @"ut5VC";
    UIViewController *vc = [storyboard instantiateViewControllerWithIdentifier: vid];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
  }

  

  // Override point for customization after application launch.
  [self.window makeKeyAndVisible];

  
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
  NSLog(@"***** App going into background!!!!");
  [[ICG sharedICG] downloadCodesFromCloud];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
  NSLog(@"***** App going into foreground!!!!");
  [[ICG sharedICG] downloadCodesFromCloud];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	/*
	 Called when the application is about to terminate.
	 Save data if appropriate.
	 See also applicationDidEnterBackground:.
	 */
}

// Respond to URI scheme links
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
  // pass the url to the handle deep link call
  [[Branch getInstance] handleDeepLink:url];
  
  // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
  return YES;
}

// Respond to Universal Links
- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray *restorableObjects))restorationHandler {
  BOOL handledByBranch = [[Branch getInstance] continueUserActivity:userActivity];
  
  // --------------
  // Use ABC to open iCode - not working well but will do so now.
  [ICG sharedICG].clientId = @"JBH";
  UIStoryboard *storyboard = self.window.rootViewController.storyboard;
  UITabBarController *tabBarController = [storyboard instantiateViewControllerWithIdentifier:@"mainTabBarController"];
  
 
//  tabBarController.selectedIndex = 1;
  self.window.rootViewController = tabBarController;
  [self.window makeKeyAndVisible];
  // --------------
  
  return handledByBranch;
}

@end
