//
//  ICActivityViewController.h
//  SquareCam 
//
//  Created by Eden Choi on 12/09/16.
//
//

@interface ICActivityViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end