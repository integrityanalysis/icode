//
//  TC_preproc.m
//  SquareCam
//
//  Created by Zeke Chan on 30/12/19.
//

#import <Foundation/Foundation.h>
#include "Constants.h"
#include "tc_engine.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Accelerate/Accelerate.h>



//float _signal1[SQPX], _edge[SQPX], _absF1[SQPX];
void TC_preproc(float *Signal, float *absF2){
//  if(db_useOldFilter){
//    getFilteredFFT_old(Signal, absF2);
//    return;
//  }
//
//  int r,c,i,j,k;
//  int displayOn = 0;
//
//  // Disk-donut Kernal (instead of Gaussian)
//  float B[9]={
//    0.0368 ,   0.2132 ,   0.0368,
//    0.2132 ,        0 ,   0.2132,
//    0.0368 ,   0.2132 ,   0.0368
//  };
//  vDSP_f3x3(Signal, CF, RF, B, _signal1);
//
//  // Edge detection kernel
//  float E[9]={
//    -1 ,   -1  ,  -1,
//    -1 ,    8  ,  -1,
//    -1 ,   -1  ,  -1
//  };
//  vDSP_f3x3(Signal, CF, RF, E, _edge);
//
//  if(displayOn){
//    printf("Conv Signal\n");
//    for (r = 0; r < RF; ++r){
//      for (c = 0; c < CF; ++c){
//        printf("%3.0f ", _signal1[r*CF+c]);
//      }
//      printf("\n");
//    }
//  }
//
//  // Alternative 3 - Adaptive binary based on Flatness
//  float bthres   = 0.1; // minimum pixel threshold
//  float seg[3]   = { 0.0 , 10.0  , 100.0   };
//  float sgain[3] = { 0.025,  0.15,   0.0125 };
//  for(i=0;i<NF;i++){
//    float Id  = Signal[i] - _signal1[i]; // difference
//    float Id1 = +(Id>bthres)  -(Id<-bthres);   // binarize
//
//    // Adaptive gain based on Edge
//    float absEdge = fabs(_edge[i]);
//    float gain;
//    if(absEdge<seg[1])       gain=sgain[0]; // low edge: high noise content
//    else if (absEdge<seg[2]) gain=sgain[1]; // med edge: best signal
//    else                     gain=sgain[2]; // hi edge: possibly false
//
//    _signal1[i] = Id1 * gain;
//  }
//
//
//  // Alternative 2 - exponentially increase near pixel 0 and 255.
//  // Plus edge removal.
//  // The lower ethres, the more noise removed.
////  float ethres = 50; // matlab use 100. Here 80 a stronger filter for clearer image
////  for(i=0;i<NF;i++){
////    if(_signal1[i]!=0 & fabs(_edge[i])<ethres){
////      float Id  = Signal[i] - _signal1[i]; // difference
////      float den = 10 + (_signal1[i]<128) *_signal1[i]
////                     + (_signal1[i]>=128)*(255-_signal1[i]);
////      _signal1[i] = Id  / den; // edge removal
////    }else{
////      _signal1[i] = 0;
////    }
////  }
//
//  // Alternative 1 - binarize. Good result too!
//  // (f-f'), binarize, edge removal+scaling
////  float bthres = 2;
////  float ethres = 80;
////  for(i=0;i<NF;i++){
////    float Id  = Signal[i] - _signal1[i]; // difference
////    float Id1 = +(Id>bthres)  -(Id<-bthres);   // binarize
////    _signal1[i] = 0.1 * Id1 * (fabs(_edge[i])<ethres); // edge removal
////  }
//
//  // Original plus edge detector. Better but in text situation, code not strong.
//  // f/f' - 1. if f' is zero, don't do the division. Keep it to zero.
////  for(i=0;i<NF;i++){
////    if(_signal1[i]!=0){
////      _signal1[i] = (Signal[i] / _signal1[i] - 1)*(fabs(_edge[i])<ethres);
////    }
////  }
//
//  if(displayOn){
//    printf("Normalized Convolved Signal\n");
//    for (r = 0; r < RF; ++r){
//      for (c = 0; c < CF; ++c){
//        printf("%7.3f ", _signal1[r*CF+c]);
//      }
//      printf("\n");
//    }
//  }
//
//  // Zero-out area outside circle
//  for(i=0;i<LK.nOutCircle;i++) _signal1[ LK.outCircle[i] ] = 0;
//
//  // unfortunately, can't see if the circle-zero is working!
////  printimagef(_signal1,256,256,5,5,1,1);
//
//
//  //  float *absF;
//  getFFT(_signal1,_absF1);
//  int C1 = CF/2+1;
//
//  if(displayOn){
//    printf("Abs F (not shifted)\n");
//    k = 0;
//    for (r = 0; r < RF; ++r){
//      for (c = 0; c < C1; ++c){
//        printf("%7.3f ", _absF1[k++]);
//      }
//      printf("\n");
//    }
//  }
//
//  // Perform FFT Shift
//  memcpy(absF2,&_absF1[(RF/2)*C1],4*(RF/2)*C1); // float=4x word.
//  memcpy(&absF2[(RF/2)*C1],_absF1,4*(RF/2+1)*C1);
//  // printf("RF %d C1 %d NF %d\n",RF,C1,NF); //512,257
//
//  if(displayOn){
//    printf("\n\nShuffled Abs F\n");
//    k = 0;
//    for (r = 0; r < RF+1; ++r){
//      for (c = 0; c < C1; ++c){
//        printf("%7.3f ", absF2[k++]);
//      }
//      printf("\n");
//    }
//  }
}
