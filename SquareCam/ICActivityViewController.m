//
//  ICActivityViewController.m
//  SquareCam 
//
//  Created by Eden Choi on 12/09/16.
//
//

#import <Foundation/Foundation.h>
#import "ICActivityViewController.h"
#import "ICWebViewController.h"
#import "imViewController.h"
#import "ICG.h"
#import <Google/Analytics.h>

@interface ICActivityViewController()

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewDebug;

@end

@implementation ICActivityViewController 

- (void)viewDidLoad {
  [super viewDidLoad];

  if (db_viewDebug) {
    _viewDebug.hidden   = NO;
  }
}

- (void)viewWillAppear:(BOOL)animated {
  // Set title
  if([[ICG sharedICG].clientId isEqual:@"JBH"]){
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navbar_jb.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
  }else{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
  }

   [_tableView reloadData]; // called when tap on tabBar
  
  id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
  [tracker set:kGAIScreenName value:@"ICActivityViewController"];
  [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  NSLog(@"%lu", (unsigned long)[[ICG sharedICG].arrayScan count]);
  return [[ICG sharedICG].arrayScan count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"activityTableView"];
  if (cell==nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"activityTableView"];
  }
  Code *code = [ICG sharedICG].arrayScan[indexPath.row];
  
  cell.imageView.image      = code.getImage;
  cell.textLabel.text       = code.Desc;
  cell.detailTextLabel.text = code.Url;
  cell.detailTextLabel.textColor = [UIColor grayColor];
  cell.selectionStyle         = UITableViewCellSelectionStyleNone;
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  Code *code = [ICG sharedICG].arrayScan[indexPath.row];
  
  if(db_useDemo){//([code.Url hasPrefix:@"ztmp_"]){
    // ----- Display Image Only - imViewController
    imViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"imViewController"];
    wvc.code  = code;
    wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
    [self.navigationController pushViewController:wvc animated:YES];
    
  }else{
    // ----- Visit Webpage -
    ICWebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
    wvc.code  = code;
    wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
    [self.navigationController pushViewController:wvc animated:YES];
  }

}

#pragma mark - Debug box unit tests
// Load data
- (IBAction)UT01:(id)sender {
  [[ICG sharedICG] loadData];
  [_tableView reloadData];
}

// Save data
- (IBAction)UT02:(id)sender {
  [ICG sharedICG].arrayScan = [NSMutableArray arrayWithObjects:@{@"address":@"https://www.swingprofile.com",@"title":@"Swing Profile"}, @{@"address":@"https://www.google.com",@"title":@"Google"}, @{@"address":@"https://www.trademe.co.nz",@"title":@"Trade Me"}, nil];
  [[ICG sharedICG] saveData];
}

@end
