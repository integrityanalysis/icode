//
//  ICWebViewController.m
//  SquareCam 
//
//  Created by Eden Choi on 13/09/16.
//
//

#import <Foundation/Foundation.h>
#import "ICWebViewController.h"
#import "ICG.h"

@interface ICWebViewController()

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *viewAddress;
@property (strong, nonatomic) IBOutlet UITextField *textFieldAddress;
@property (strong, nonatomic) IBOutlet UIView *buttonRefresh;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintViewAddressTop;
@property (assign, nonatomic) BOOL showThumbnail;
@property (strong, nonatomic) UIImageView *loadingView;

@end

@implementation ICWebViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  if(!_code){
    _code    = [[Code alloc] init];
    _code.Code     = 0;
    _code.ClientId = @"";
    _code.Desc     = @"";
    _code.Url      = @"https://www.integrityanalysis.com";
    _noAnimate     = YES;
  }
  
  // 1. Start loading and put the email in the address bar
  if ([_code.Url.lowercaseString hasPrefix:@"http"]) {
    _websiteAddress = _code.Url;
  } else {
    _websiteAddress = [NSString stringWithFormat:@"http://%@",_code.Url];
  }
  
//  _websiteAddress = [NSString stringWithFormat:@"http://%@",_code.Url];
  [_webView setMediaPlaybackRequiresUserAction:NO]; // enables youtube autoplay.Side effect?
  [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteAddress]]];
  _textFieldAddress.text = _websiteAddress;
  self.title             = _code.Desc;// _websiteTitle;
  

  
//    _loadingView.translatesAutoresizingMaskIntoConstraints = YES;
  _thumbnail = _code.getImage;
  
  // 2. Add loading view and activity spinner
  float w = SCREEN_WIDTH * 0.6;
  _loadingView = [[UIImageView alloc] initWithFrame:CGRectMake((SCREEN_WIDTH-w)/2, (SCREEN_HEIGHT-w)/2, w, w)];
  _loadingView.image              = _thumbnail;
  _loadingView.backgroundColor    = [UIColor colorWithWhite:0. alpha:0.6];
  _loadingView.layer.cornerRadius = 5;

  
  UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
  activityView.tag    = 1;
  activityView.center = CGPointMake(w/2, w/2);

  [_loadingView addSubview:activityView];
  
  UILabel* lblLoading = [[UILabel alloc] initWithFrame:CGRectMake(0,w-48,w,48)];
  lblLoading.tag  = 2;
  lblLoading.textColor = [UIColor whiteColor];
  lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
  lblLoading.textAlignment = NSTextAlignmentCenter;
  

  
  [_loadingView addSubview:lblLoading];
  
  [self.view addSubview:_loadingView];

//  SCALEF(_loadingView);
//  SCALEFS(_loadingView);
  [_loadingView setHidden:NO];
  _showThumbnail = YES;
  
  if(_noAnimate){
    _viewAddress.hidden = YES;
    _constraintViewAddressTop.constant    = -40;
  }
 
//  if([[ICG sharedICG].clientId isEqual:@"jbhifi"]){
//    [self.navigationController.navigationBar setBackgroundImage:[[UIImage imageNamed:@"navbar_jb.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)] forBarMetrics:UIBarMetricsDefault];
//  }else{
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//  }
}
-(void)viewWillDisappear:(BOOL)animated
{
  // This seems to work well to stop loading youTube video.
  // But if we don't want to delete page (in case of default webpage
  // use _noAnimate to control
  if(!_noAnimate){
    [_webView loadHTMLString:nil baseURL:nil];
  }
  
//  if ([_webView isLoading]) [_webView stopLoading];
}

#pragma mark - Buttons

- (IBAction)buttonRefresh:(id)sender {
  _showThumbnail = YES;
  [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_websiteAddress]]];
  // where is the button?
  // Need to display the loadingView again and activityView.
}

#pragma mark - UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView {
  
  // Hide Web address - call this always
  if (!webView.isLoading && !_noAnimate) {
    NSLog(@"Finished loading");
    [self.view layoutIfNeeded];
    _constraintViewAddressTop.constant    = -40;
    [UIView animateWithDuration:0.3 animations:^{
      [self.view layoutIfNeeded];
    }];
  }
  
  // Don't proceed if showThumbnail is OFF
  if(!_showThumbnail) return;
  
  // Hide loadingView but with fade out animation
  _loadingView.alpha = 1.0;
  [UIView animateWithDuration:0.5 animations:^{
    _loadingView.alpha = 0.0;
  } completion:^(BOOL finished) {
    // alpha 0 should be = hidden.
  }];
 
  NSLog(@"webView did finish load");
  _showThumbnail = NO;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
  
  [_loadingView setHidden:NO];
  for (UIView *v in _loadingView.subviews){
    if(v.tag == 1){
      // Activity View
      UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)v;
      [activityView startAnimating];
    }
    if(v.tag == 2){
      // label Loading
      UILabel *lblLoading = (UILabel *)v;
      
      // Attributed String - allow highlighting only text background
      NSMutableAttributedString *s = [[NSMutableAttributedString alloc] initWithString:@"Loading..."];
      [s addAttribute:NSBackgroundColorAttributeName
                value:UA_rgba(0, 0, 0, 0.5)
                range:NSMakeRange(0, s.length)];
      lblLoading.attributedText = s;
    }
  }
  
  NSLog(@"webView did start load");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
  [_loadingView setHidden:NO];
  
  for (UIView *v in _loadingView.subviews){
    if(v.tag == 1){
      // Activity View
      UIActivityIndicatorView *activityView = (UIActivityIndicatorView *)v;
      [activityView stopAnimating];
    }
    if(v.tag == 2){
      // label Loading
      UILabel *lblLoading = (UILabel *)v;
      
      // Attributed String - allow highlighting only text background
      NSMutableAttributedString *s = [[NSMutableAttributedString alloc] initWithString:@"Loading Failed"];
      [s addAttribute:NSBackgroundColorAttributeName
                value:UA_rgba(0, 0, 0, 0.5)
                range:NSMakeRange(0, s.length)];
      lblLoading.attributedText = s;
    }
  }
  
  NSLog(@"Problem: fail with error %@",error);
}

@end
