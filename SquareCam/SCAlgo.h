//
//  SCAlgo.h
//  SquareCam 
//
//  Created by Zeke Chan on 27/05/16.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SCAlgo : NSObject
@property (assign, nonatomic) long int photoId;
@property (strong, nonatomic) NSMutableArray *arrPhoto;
@property (strong, nonatomic) NSMutableArray *arrScore;
@property (strong, nonatomic) NSMutableArray *arrThumbnail;
@property (strong, nonatomic) NSMutableArray *arrSelected;

@property (assign, nonatomic) double effectiveScale;
@property (assign, nonatomic) double vidWdith;
@property (assign, nonatomic) double vidHeight;
@property (assign, nonatomic) double effWdith;
@property (assign, nonatomic) double effHeight;
@property (assign, nonatomic) int exifOrientation;

@property (strong, nonatomic) NSString *detectMode;
@property (assign, nonatomic) double detectStartAt;
@property (assign, nonatomic) double detectEndAt;
@property (assign, nonatomic) unsigned long long int code;

@property (strong, nonatomic) UIImage *latestCaptureImage;
@property (strong, nonatomic) NSDictionary *latestBarcode;

- (int)captureOutput : (AVCaptureOutput *)captureOutput
didOutputSampleBuffer : (CMSampleBufferRef)sampleBuffer
       fromConnection : (AVCaptureConnection *)connection
             features : (NSArray *) features ;

- (void) getFrameRate;

@end
