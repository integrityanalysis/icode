//
//  ICG.m
//  SquareCam 
//
//  Created by Eden Choi on 14/09/16.
//
//

#import <Foundation/Foundation.h>
#import "ICG.h"
#import "ic_engine.h"

@interface ICG()
@end

@implementation ICG

// Make ICG a singleton
+ (id)sharedICG {
  static id sharedICG = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedICG = [[self alloc] init];
  });
  return sharedICG;
}
//+ (ICG *)sharedICG {
//  static dispatch_once_t pred;
//  __strong static ICG * sharedICG = nil;
//  dispatch_once( &pred, ^{
//    sharedICG = [[self alloc] init];
//  });
//  return sharedICG;
//}

- (id)init {
  self = [super init];
  if (self) {
    _arrayScan = [[NSMutableArray alloc] init];
    _clientId  = @"";
    _showPlot  = 0;
    _testMode  = 0 || THINCODE; // For now, thin code defaults to test mode
    _fScan     = [docDir stringByAppendingPathComponent:@"scan.plist"];
    _fCode     = [docDir stringByAppendingPathComponent:@"code.plist"];
    _fSettings = [docDir stringByAppendingPathComponent:@"settings.plist"];
  }
  return self;
}

// Load the data
- (void)loadData {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"data"];
  
  _arrayScan = [NSMutableArray arrayWithArray:[NSArray arrayWithContentsOfFile:filePath]];
  
  // For testing fill with dummy data
  if ([_arrayScan count] == 0) {
//    _arrayScan = [NSMutableArray arrayWithObjects:@{@"address":@"https://www.swingprofile.com",@"title":@"Swing Profile"}, @{@"address":@"https://www.google.com",@"title":@"Google"}, nil];
    NSLog(@"No ICG array data found");
  } else {
    NSLog(@"ICG array data loaded");
  }
}

// Save the data
- (void)saveData {
  // For testing:
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  
  if ([paths count] > 0) {
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"data"];
    [_arrayScan writeToFile:filePath atomically:YES];
    NSLog(@"ICG Array data saved");
  }
}

- (NSString *) getNowDateString{
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  
  [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
  //  NSDate *dateNow    = [dateFormatter dateFromString:[NSDate date]];
  NSString *dateNow = [dateFormatter stringFromDate:[NSDate date]];
  return dateNow;
  
}

- (void) setup {
  // Load data from user defaults
  [self loadUserDefaults];
  [self updateLookupCode];
  [self downloadCodesFromCloud];
}

- (void) loadUserDefaults {
  
  // If there is already loaded userDefault, indicated by _latestUpdatedAt,
  // then don't reload from userDefault, coz it is buggy and often loads nothing.
  if(_latestUpdatedAt){
    NSLog(@"Not loading userDefaults because value at latestUpdatedAt:%f",_latestUpdatedAt);
    return;
  }
  // Initialize if empty.
//  _arrayScan       = [NSMutableArray array];
//  _arrayCode       = [NSMutableArray array];
//  _latestUpdatedAt = 0;

  // Settings
  _settings = [NSDictionary dictionaryWithContentsOfFile: _fSettings];
  _latestUpdatedAt = [_settings[@"latestUpdatedAt"] doubleValue] ;
  
  // Load Code and Scan
  NSData *data1 = [[NSData alloc] initWithContentsOfFile: _fScan];
  _arrayScan    = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
//  NSAssert(_arrayScan, @"Load arrayScan failed");
  
  NSData *data2 = [[NSData alloc] initWithContentsOfFile: _fCode];
  _arrayCode  = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
//  NSAssert(_arrayCode, @"Load arrayCode failed");
  
  // Initialize if empty.
  if(!_arrayScan)       _arrayScan       = [NSMutableArray array];
  if(!_arrayCode)       _arrayCode       = [NSMutableArray array];
  if(!_latestUpdatedAt) _latestUpdatedAt = 0;
  
  
//  // Array from UserDefaults are immutable. So must assign first to NSArray,
//  // Then copy to MutableArray.
//  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//  
//  // Note array elements are Code object. Can't say as array as usual.
//  NSData *data1 = [defaults objectForKey:@"arrayScan"];
//  NSArray *arr1 = [NSKeyedUnarchiver unarchiveObjectWithData:data1];
//  NSData *data2 = [defaults objectForKey:@"arrayCode"];
//  NSArray *arr2 = [NSKeyedUnarchiver unarchiveObjectWithData:data2];
//
//  _latestUpdatedAt = [[defaults objectForKey:@"latestUpdatedAt"] doubleValue] ;
//  _arrayScan = [NSMutableArray arrayWithArray:arr1];
//  _arrayCode = [NSMutableArray arrayWithArray:arr2];
//
//  
//  // Initialize if empty.
//  if(!_arrayScan)       _arrayScan       = [NSMutableArray array];
//  if(!_arrayCode)       _arrayCode       = [NSMutableArray array];
//  if(!_latestUpdatedAt) _latestUpdatedAt = 0;
  
  NSLog(@"Load scan and codes from UserDefaults");
}

- (void) saveUserDefaults : (NSString *) option{
  // Save to fScan
  if ([option rangeOfString:@"scan"].location != NSNotFound) {
    NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:_arrayScan];
    BOOL success  = [data1 writeToFile:[ICG sharedICG].fScan atomically:YES];
    NSAssert(success, @"Write fScan failed");
    
    NSLog(@"Save scans to UserDefaults.");
    
    
    // For checking: show what is in the library
    NSArray* dirs = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:docDir
                                                                        error:NULL];
    [dirs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
      NSString *filename  = (NSString *)obj;
      NSString *extension = [[filename pathExtension] lowercaseString];
      NSLog(@"%@",filename);
    }];
    
    // For checking: the values are stored correctly
    for(int i=0;i<_arrayScan.count;i++){
      Code *code = _arrayScan[i];
      NSLog(@"Image ScannedAt on array %i = %@",i,code.ScannedAt);
    }
    
    // Keep only "historySize" number of items in history.
    // Delete older images and arrayScan entry.
    int historySize = 10;
    if(_arrayScan.count>historySize){
      for(int i=_arrayScan.count-1;i>=historySize;i--){
        // Remove image, then remove oldest array entry
        Code *code = _arrayScan[i];
        NSString *filePath = [docDir stringByAppendingPathComponent:ptf(@"pic%@.jpg",code.ScannedAt)];
        NSError *error;
        BOOL successRemove = [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        [_arrayScan removeObjectAtIndex:i];
        NSLog(@"Old image %i %@ remove success %d array length %d",i,filePath,successRemove,_arrayScan.count);
      }
    }
  }
  
  // Save to fCode and fSettings
  if ([option rangeOfString:@"code"].location != NSNotFound) {
    NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:_arrayCode];
    BOOL success  = [data2 writeToFile:[ICG sharedICG].fCode atomically:YES];
    NSAssert(success, @"Write fCode failed");
    
    NSDictionary *d3 = @{@"latestUpdatedAt" : @(_latestUpdatedAt),
                        @"key2" : @"Hops",
                        @"key3" : @"Malt",
                        @"key4" : @"Yeast" };
    BOOL success3 = [d3 writeToFile: [ICG sharedICG].fSettings atomically:YES];
    NSAssert(success3, @"Write fSettings failed");
    
    NSLog(@"Save codes to UserDefaults.");
  }
 

  
  // Store the data - special retrival from achived data for array of Code.
//  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//  if ([option rangeOfString:@"scan"].location != NSNotFound) {
//    NSData *data1 = [NSKeyedArchiver archivedDataWithRootObject:_arrayScan];
//    [defaults setObject:data1 forKey:@"arrayScan"];
//    NSLog(@"Save scans to UserDefaults.");
//  }
//  if ([option rangeOfString:@"code"].location != NSNotFound) {
//    NSData *data2 = [NSKeyedArchiver archivedDataWithRootObject:_arrayCode];
//    [defaults setObject:data2 forKey:@"arrayCode"];
//    [defaults setObject:@(_latestUpdatedAt) forKey:@"latestUpdatedAt"];
//    NSLog(@"Save codes to UserDefaults.");
//  }
//  [defaults synchronize];
  
}

- (void) updateArrayCode : (Code *) code {
  
  // 1. Binary Search for new code on arrayCode
  // Get array insert index
  // Do one more match to see whether code already existed
  NSMutableArray *sortedArray = _arrayCode;
  NSUInteger findIndex = 0;
  int match            = 0;
  
  Code *searchObject      = code;
//  NSNumber *searchObject = code.Code;
  if(sortedArray.count>0){
    // Binary NSArray search
    NSRange searchRange  = NSMakeRange(0, [sortedArray count]);
    findIndex = [sortedArray indexOfObject : searchObject
                             inSortedRange : searchRange
                                   options : NSBinarySearchingInsertionIndex
                           usingComparator : ^(Code *obj1, Code *obj2) {
                              return [obj1.Code compare:obj2.Code];
                            }];
    NSLog(@"findIndex %tu",findIndex);
    
    // As long as not greater than whole array (not matched),
    // Test matching (crash if findIndex==sortedArray.count)
    if(findIndex<sortedArray.count){
      Code *c1 = sortedArray[findIndex];
      Code *c2 = searchObject;
      if([c1.Code isEqual:c2.Code]) match = 1;
    }
  }
  
  // 2. If not match, insert element; otherwise overwrite.
  if(match==0){
    // Insert
    [sortedArray insertObject:searchObject atIndex:findIndex];
  }else{
    // Overwrite
    sortedArray[findIndex] = searchObject;
  }
  
//  NSLog(@"Code %@ matched? %d findIndex %tu",code.Code,match,findIndex);
//  NSLog(@"array %@",sortedArray);
}

// Search and delete expired item in arrayCode, then copy to LK.code
- (void) updateLookupCode {
  // 1. Search for expired code and delete - looping backward for delete
//  BOOL changeMade = NO;
  double unixNow = [[NSDate date] timeIntervalSince1970];
  for(int i=[ICG sharedICG].arrayCode.count-1;i>=0;i--){
    Code *c = [ICG sharedICG].arrayCode[i];
    
    printf("%d Created at %f / now %f \n",i,c.CreatedAt.doubleValue,unixNow);
    NSLog(@"Created At String %@",c.CreatedAtS);
    
    // Check expiry - c.ExpireIn is in DAYS.
    BOOL expired = unixNow > (c.CreatedAt.doubleValue + c.ExpireIn.doubleValue*3600*24);
    if(expired && deleteExpired){
      printf("%d *** deleted!! \n",i);
      [[ICG sharedICG].arrayCode removeObjectAtIndex:i];
//      changeMade = YES;
    }
  }
  // Save only if any change has been made.
//  if(changeMade) [[ICG sharedICG] saveUserDefaults : @"code" ];
  [[ICG sharedICG] saveUserDefaults : @"code" ];
  
  // 2. Add Code from arrayCode to LK.code - much faster search.
  LK.nCode    = 0; // In between now and finish copying, nCode=0 lock out search.
  for(int i=0;i<[ICG sharedICG].arrayCode.count;i++){
    Code *c = [ICG sharedICG].arrayCode[i];
    LK.code[i] = c.Code.longLongValue;//[c.Code unsignedLongLongValue];
    printf("%d LK.code %llu / %lu\n",i,LK.code[i],[ICG sharedICG].arrayCode.count);
  }
  LK.nCode = [ICG sharedICG].arrayCode.count;
  
}
- (void) downloadCodesFromCloud {
  AWSDynamoDBObjectMapper *objMapper = [AWSDynamoDBObjectMapper DynamoDBObjectMapperForKey:@"APSoutheast2DynamoDB"];
  AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
  
  queryExpression.indexName = @"IcRegion-UpdatedAt-index";
  queryExpression.keyConditionExpression    = @"IcRegion = :reg AND UpdatedAt > :val";
//  queryExpression.keyConditionExpression    = @"IcRegion = :reg";
  
  // Use Different Region for iCode and thin code
  queryExpression.expressionAttributeValues = @{@":reg":(THINCODE==0) ? @"NZL" : @"NZL_TC",
                                                @":val":@([ICG sharedICG].latestUpdatedAt)};
  
  printf("Before call: latest Updated At %f\n",[ICG sharedICG].latestUpdatedAt);

  [[objMapper query:[Code class]
         expression:queryExpression]
   continueWithBlock:^id(AWSTask *task) {
     
     if (task.error) {
       NSLog(@"The request failed. Error: [%@]", task.error);
     }
     // This is no longer available in version 2.5.0 of the AWS SDK
//     if (task.exception) {
//       NSLog(@"The request failed. Exception: [%@]", task.exception);
//     }
     if (task.result) {
       AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
       for (Code *code in paginatedOutput.items) {
         //Do something with book.
         NSLog(@"Download Code %@ desc %@ client %@ createdAt %@", code.Code,code.Desc,code.ClientName,code.CreatedAt);
         
         [[ICG sharedICG] updateArrayCode:code];
         
         // Update latestUpdatedAt if higher value exists
         if( code.UpdatedAt.doubleValue > [ICG sharedICG].latestUpdatedAt ) {
           [ICG sharedICG].latestUpdatedAt = code.UpdatedAt.doubleValue;
         }
       }
       // Only updateLookupCode if there had been any change from Cloud
       if(paginatedOutput.items.count>0){
         [[ICG sharedICG] updateLookupCode];
       }
       printf("After call:latest Updated At %f\n",[ICG sharedICG].latestUpdatedAt);
     }
     return nil;
   }];
  
}


- (void) resetUserDefaults {
  _arrayScan       = [NSMutableArray array];
  _arrayCode       = [NSMutableArray array];
  _latestUpdatedAt = 0;
}

// ********************************************************************************
// * Process Frame in uint8 array into a UIImage format
// ********************************************************************************
+ (UIImage *) processFrameToImage : (unsigned char*) baseAddress
                         setWidth : (int) width
                        setHeight : (int) height
                   setBytesPerRow : (int) bytesPerRow
                    setImageColor : (BOOL) imageColor
{
  CGColorSpaceRef colorSpace;
  CGBitmapInfo bitmapInfo;
  
  if (imageColor == YES){
    colorSpace = CGColorSpaceCreateDeviceRGB();
    bitmapInfo = kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst;
  } else {
    colorSpace = CGColorSpaceCreateDeviceGray();
    bitmapInfo = kCGImageAlphaNone;
  }
  
  /*Create a CGImageRef from the CVImageBufferRef*/
  CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, bitmapInfo);
  CGImageRef newImage = CGBitmapContextCreateImage(newContext);
  
  /*We release some components*/
  CGContextRelease(newContext);
  CGColorSpaceRelease(colorSpace);
  
  
  /*We display the result on the image view (We need to change the orientation of the image so that the video is displayed correctly).
   Same thing as for the CALayer we are not in the main thread so ...*/
  UIImage *image = [UIImage imageWithCGImage:newImage
                                       scale:1.0f
                                 orientation:nil];
  
  /*We relase the CGImageRef*/
  CGImageRelease(newImage);
  return image;
}

unsigned char _tmpImage[262144];
+ (UIImage *) heatmap : (float *) src
             setWidth : (int) width
            setHeight : (int) height
          setMinValue : (float) minValue
          setMaxValue : (float) maxValue {
  
  // change range of src to [0,255], and assign to _tmpImage (unsigned char)
  float range = (maxValue-minValue);
  int k=0;
  for(int i=0;i<width;i++){
    for(int j=0;j<height;j++){
      _tmpImage[k] = (unsigned char)(255.0*MAX(MIN((src[k] - minValue)/range,1),0));
      k++;
    }
  }
  // create UIImage from _tmpImage
  UIImage *im = [self processFrameToImage:_tmpImage
                                 setWidth:width
                                setHeight:height
                           setBytesPerRow:width setImageColor:NO];
  
  return im;
}







@end
