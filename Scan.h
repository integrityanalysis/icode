//
//  Scan.h
//  SquareCam
//
//  Created by Zeke Chan on 29/10/18.
//


#import <AWSCore/AWSCore.h>
#import <AWSDynamoDB/AWSDynamoDB.h>



@interface Scan : AWSDynamoDBObjectModel <AWSDynamoDBModeling>


@property (nonatomic, strong) NSString *UserId;
@property (nonatomic, strong) NSString *Note;
@property (nonatomic, strong) NSNumber *TimeStamp;

+ (NSString *)dynamoDBTableName;

+ (NSString *)hashKeyAttribute;




@end

//NS_ASSUME_NONNULL_END
