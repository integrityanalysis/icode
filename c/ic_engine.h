//
//  ic_engine.h
//  SquareCam 
//
//  Created by Zeke Chan on 10/11/16.
//
//

#ifndef ic_engine_h
#define ic_engine_h


#endif /* ic_engine_h */

#define Log2R	8u		// Base-two logarithm of number of rows (5 bit-32).
#define Log2C	8u		// Base-two logarithm of number of columns (6-bit-64).


#define	RF	(1u<<Log2R)	// Number of rows.
#define	CF	(1u<<Log2C)	// Number of columns.
#define	NF	(RF*CF)		// Total number of elements.
#define SQW (1u<<Log2R) // Used in SCAlgo.m to define image width
#define SQPX SQW*SQW  // Image area or num elements

#define NX   180   // Number of columns extended.
#define NX2  (NX*2-1)   // Number of rows extended 2*nx-1.
#define SX2  NX2*NX   // extended half circle num elements

#define NE SQW*100   // buffer for storing radius indices
#define PI 3.14159265358979323846264338

// ----------
#define printReset sprintf(_str,"")
#define printAppend(...) sprintf(_str+strlen(_str),__VA_ARGS__)
#define printOut LOGD("%s",_str)

// ----------
#ifdef __ANDROID__

#include "camera_jni.h"
#define LOG_TAG	"engine"
//#undef LOG
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

#else

#define LOGD(...) printf(__VA_ARGS__);printf("\n")

#endif



// Return the maximum of two numbers.

char _str[10000];

// ------------------------------------------------
struct LOOKUP{
  double tframe0;
  int bp   ;//  = 0;
  int iframes; //debugging
  int iframecur;
  int sqpx2;
  
  float cc;
  float rc;
  float radius[SX2];
  float angle[SX2];
  
  int  nOutCircle;
  int  outCircle[NE*10];

  int nRadiusBin;
  int radiusBinValue[NE];
  int radiusBinCount[NE];
  int radiusBinIdx[NE*200];//NE * number of radius bins
  
  // Read from Ellipse
  int nDiv; // divisions between 2 bits
  int nBit; // nbit per PI
  int npt;
  float cosa[5000];
  float sina[5000];
  float div;
  
  float r[SX2];
  float c[SX2];
  
  int map[SX2]; // FFT image to extended image mapping
  int icentre;
  //  double dmin[NB+1];
  //  double dmax[NB+1];
  //  double pA[NB+1];
  //  double pB[NB+1];
  //  double pC[NB+1];
  //  double cpA[NB+1];
  //  double cpB[NB+1];
  //  double cpC[NB+1];
  
  unsigned long long int code[1000000];
  int nCode;
  
  
  int iRadBest[100];
  int nRadBest;
  
  // for general plotting
  float x[5000],y[5000],y2[5000];
  int nPlotPoint;
  char plotComment[1000];
  int showPlot;
  
  // for heatmap
  float heatmap[NF*4];
  
  unsigned long long int codeLatest;
  
};
struct LOOKUP LK;

struct SPARSE{
  int n;  // number of pixels in the half square
  float r[NE];
  float c[NE];
  float v[NE];
  float rad[NE];
  float angle[NE];
  int   use[NE];
  float p95;
  float p50;
};
struct SPARSE gp;


void initializeLookup();

void printimage(unsigned char *Im, int row, int col, int rowskip, int colskip, int bytesperpixel, int bound);
void printimagef(float *Im, int row, int col, int rowskip, int colskip, int bytesperpixel, float bound);
float kthsmallest(int k, int n, float arr[]);
void prtvec(float *v,int m);
void prtmat(float **a,int m,int n);
void inv_svd(float **a, float *b, int m, int n, float *x);
void matrixInversion(float **A, int N,float **y);


void initialize();

void fitEllipse(float *data1, float *data2, int ndata, float *ellipseParam);
void readPeaks(float *lf, float *ellipseParam, float *conf);
void readPeaksOnly(float *lf, float *ellipseParam);
void getConfidence(float *conf, int iMax, float ptile);
// BCH
//extern void gf_poly(void);
//extern void generate_gf(void);
//extern void gen_poly(void);
//extern int decode(void);
extern int bchdec(int m_in, int length_in, int t_in, unsigned char *recd_in);
int myDecoder(unsigned char *rx, unsigned char *ucRx, int *nCorrected);
int randomDecode(float *p,int maxTrial);

// binary to decimal for 63 bits.
unsigned long long int b2d63(unsigned char *ucRx);
unsigned long long int decMax63(unsigned long long int dec);

void getFilteredFFT(float *Signal, float *absF2);
void getFilteredFFT_old(float *Signal, float *absF2);
void getFFT(float *Signal, float *mag1);
void test01();
void getMeanRadius(float *af0);
int myEngine(float *Image);
int myEnginex(float *absF);
void test_Image();

int cmpfunc(const void * a, const void * b);
int myBinarySearch(unsigned long long int target);

void copyIntoLKCode(long long *data, int ndata);
unsigned char *loadMatlabData();
