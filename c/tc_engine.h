//
//  tc_engine.h
//  SquareCam
//
//  Created by Zeke Chan on 30/12/19.
//

#ifndef tc_engine_h
#define tc_engine_h

#include <stdio.h>

#endif /* tc_engine_h */

#define TC_VERBOSE 0
#define IMW 512  // Image width
#define SQ512 512*512 // Square 512

#define codechk(x,m) if(!(x)){ pass=0;printf("*** Failed: %s\n",m); goto decodeNext;}

int myTCEngine(unsigned char *Image);
void TC_preproc(float *Signal, float *absF2);

void loadCSVFile(char filename[], float tmpf[]);


void imageConv(unsigned char g1[],unsigned char bin[]);
void dfs(int x, int y, int current_label, unsigned char m[], int label[], float centre[][3]);
void connectedLabelling(unsigned char in[], float c[][3],int *count, int thres_min,int thres_max);
void linefit(float x[], float y[], int n, float *m, float *c);
void collinearSearch(float c[][3],int nc, int thres_proj, long M[], long CP[], int *nCP2, int iCP[], int jCP[]);
void collinearLineParam(float c[][3],int nc,int i,int j,int thres_proj,
                        float *mx2,float *my2,float *yint2, float *deg);
void collinearGradientPoints(unsigned char g2[],float mx, float my,float c2,
                             float px[],float py[],int pb[],int *np2);
void findLongSegments(int pb[], int np, int seg[][2],int *nseg,
                      int thres_gap, int thres_len,int thres_den,int thres_lbreak);
void getIntercepts(unsigned char g1[], float px[],float py[],int istart, int iend,float intc[][2],int *nintc2);
void tc_decode(float gap[][2],int ngap,int *pass,int dec[]);
void test(unsigned char in[], float c[][3],int *count, int thres_min,int thres_max);
int tc_engine0(unsigned char g1[],unsigned char g2[],unsigned char g3[]);
int tc_fast(unsigned char g1[],unsigned char g2[],unsigned char g3[]);

// Unit tests
void ut_connectedLabelling(unsigned char g1[],unsigned char g2[],unsigned char g3[]);

int tc_connected(unsigned char g1[],unsigned char g2[],unsigned char g3[]);
