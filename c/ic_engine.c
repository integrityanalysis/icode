//
//  ic_engine.c
//  SquareCam
//
//  Created by Zeke Chan on 2/12/16.
//
//
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "nr.h"
#include "nrutil.h"
#include "math.h"
#include "ic_engine.h"
#include "Constants.h"


// #####################################################################
// Binary search over LK.code - super fast
// #####################################################################
//  //https://www.codingunit.com/c-reference-stdlib-h-function-bsearch
//  //https://www.tutorialspoint.com/c_standard_library/c_function_bsearch.htm
int cmpfunc(const void * a, const void * b)
{
  if ( *(unsigned long long int*)a > *(unsigned long long int*)b ) return 1;
  else if ( *(unsigned long long int*)a == *(unsigned long long int*)b ) return 0;
  return -1;
}

int myBinarySearch(unsigned long long int target){
  
  unsigned long long int key = target;
  unsigned long long int *item;
  item =  bsearch(&key, LK.code, LK.nCode, sizeof(unsigned long long int), cmpfunc);
  
  int index = -1;
  if( item != NULL )  {
    index = item-LK.code; // index is address difference from base
  }
  
  return index;
}

// #####################################################################
// Fit Ellipse
// #####################################################################
void fitEllipse(float *data1, float *data2, int ndata, float *ellipseParam){
  int displayOn = 0;
  float c;
  float *r  =vector(1,ndata);
  float **X =matrix(1,2,1,ndata);
  float **y =matrix(1,2,1,ndata);
  //  float *c=vector(1,ndata);
  float *v  =vector(1,3);
  float **B =matrix(1,ndata,1,3);
  
  float **A =matrix(1,2,1,2);
  float *bv =vector(1,2);
  float *D  =vector(1,2);
  float **Q =matrix(1,2,1,2);
  float **iQ=matrix(1,2,1,2);
  
  int i,j,k;
  for(i=1;i<=ndata;i++){
    float x1 = data1[i-1];//       - 256.5;
    float x2 = data2[i-1];// - 256.5;
    X[1][i] = x1;
    X[2][i] = x2;
    //    printf("%f %f\n",x1,x2);
    B[i][1] = 2 * x1 * x2;
    B[i][2] = x2*x2 - x1*x1;
    B[i][3] = 1.0f;
    r[i]    = -(x1*x1);
  }
  
  // 1. Least Square Fitting
  inv_svd(B, r, ndata, 3, v);
  
  
  // 2. Conic to Parametric (bv=0)
  A[1][1] = 1 - v[2];
  A[1][2] = v[1];
  A[2][1] = v[1];
  A[2][2] = v[2];
  c       = v[3];
  
  float l1,l2,v1x,v1y,v2x,v2y;
  float a,b,alpha;
  
  // 2a. Eigen value and Vector by Jacobi - A is destroyed. But no worries.
  int nrot;
  jacobi(A, 2, D, Q, &nrot);
  eigsrt(D, Q, 2); // sort eigenvalues from largest to smallest
  
  if(displayOn){
    printf ("** solution x\n");
    prtvec(v,3);
    printf ("** solution Q\n");
    prtmat(Q, 2, 2);
    printf ("** solution D\n");
    prtvec(D,  2);
  }
  
  
  // Matlab sort from lowest to largest Eigenvalue.
  // NR sorts from largest to lowest. So we work on 2nd column, not 1st column..
  // Note we did not tranpose Q,
  a     = sqrtf(-c / D[2]); // matlab: a = sqrt(-c / D(1,1)); lowest eigenvalue
  b     = sqrtf(-c / D[1]); // matlab: b = sqrt(-c / D(2,2)); highest eigenvalue
  alpha = atan2f(Q[2][2], Q[1][2]); // work on 2nd column. may be different by PI from matlab
  
  A[1][1] =  cos(alpha);
  A[1][2] = -sin(alpha);
  A[2][1] =  sin(alpha);
  A[2][2] =  cos(alpha);
  
  
  
  
  
  //  % ==========================
  //  % Compute Unit Circle Error to all data
  //  % (not just the filtered data)
  //  iQ = inv([cos(alphag), -sin(alphag); sin(alphag) cos(alphag)]);
  //  t1 = xh(1,:) - zg(1); // zg is 0
  //  t2 = xh(2,:) - zg(2);
  //  y  = [1/ag 0;0 1/bg]*(iQ*[t1 ; t2]); % unit circle representation
  //  (transpose if requred: y' = [t1 ; t2]'*iQ'*[1/ag 0; 0 1/bg])
  //
  //  % Error from unit circle
  //  r    = sqrt(sum(y.*y,1));
  //  e1   = r-1;
  
  //  iQ = inv([cos(alphag), -sin(alphag); sin(alphag) cos(alphag)]);
  matrixInversion(A, 2, iQ);
  
  LOGD("a,b,alpha  (%10.8f,%10.8f,%10.8f) iQ",a,b,alpha);
  if(displayOn){
    printf("a,b,alpha  (%10.8f,%10.8f,%10.8f) iQ\n",a,b,alpha);
    prtmat(iQ, 2, 2);
  }
  
  if(0){
    // Compute unit circle representation and error
    // y  = [1/ag 0;0 1/bg]*(iQ*[t1 ; t2]);
    // r  = sqrt(sum(y.*y,1));
    // e1 = r-1;
    
    // A = [1/ag 0;0 1/bg]*iQ
    float errAve = 0;
    A[1][1] = iQ[1][1]/a;
    A[1][2] = iQ[1][2]/a;
    A[2][1] = iQ[2][1]/b;
    A[2][2] = iQ[2][2]/b;
    prtmat(A, 2, 2);
    for(i=1;i<=ndata;i++) {
      float y1,y2,eu;
      y1 = A[1][1]*X[1][i] + A[1][2]*X[2][i];
      y2 = A[2][1]*X[1][i] + A[2][2]*X[2][i];
      y[1][i] = y1;
      y[2][i] = y2;
      
      // Error from unit circle
      eu     = fabsf( sqrtf(y1*y1 + y2*y2) - 1 );
      r[i]   =  eu;
      errAve += eu;
    }
    errAve /= ndata;
    
    
    for(i=1;i<=20;i++) {
      printf("%f  %f  Error %f\n",y[1][i],y[2][i],r[i]);
    }
    
    float p95 = kthsmallest(ndata*0.95, ndata, r);
    
    printf("errAve %f p95 %f\n",errAve,p95);
  }
  
  ellipseParam[0] = a;
  ellipseParam[1] = b;
  ellipseParam[2] = alpha;
  
  free_vector(r,1,ndata);
  free_matrix(X,1,2,1,ndata);
  free_matrix(y,1,2,1,ndata);
  //  free_vector(c,1,ndata);
  free_vector(v,1,3);
  free_matrix(B,1,ndata,1,3);
  
  free_vector(bv,1,2);
  free_vector(D,1,2);
  free_matrix(A,1,2,1,2);
  free_matrix(Q,1,2,1,2);
  free_matrix(iQ,1,2,1,2);
}

// #####################################################################
// Read Peaks from F
// #####################################################################
float _Mt[30000],_tmp[30000],_mmk[63],_magPk[5000];
int _iPk[5000];
int _nPk,_iOptShift;

void readPeaksOnly(float *lf, float *ellipseParam){
  int displayOn = 0;
  int i,j,k;
  
  float a,b,alpha;
  a     = ellipseParam[0];
  b     = ellipseParam[1];
  alpha = ellipseParam[2];
  
  //  % --------- 1. Extract Magnitude around the ring.
  //  % Rotation matrix
  //  Q = [cos(G.alpha), -sin(G.alpha); sin(G.alpha) cos(G.alpha)] + repmat(G.z, 1, length(angles))
  //
  //  % Ellipse points
  //  X = Q * [G.a * cos(angles); G.b * sin(angles)]
  float q11 = cosf(alpha), q12 = -sinf(alpha),
  q21 = sinf(alpha), q22 =  cosf(alpha);
  
  // We want angle to start at -G.alpha -pi/2
  //  float ap = -(alpha+PI/2);
  //  if(ap<0){
  //    ap += PI * (int)(-ap/PI+1);
  //  }
  //  j = roundf( ap /  (M_PI/(LK.npt+1)) );
  //  a = -a;  // somehow the angle shift caused -ve effect
  //  b = -b;
  //  printf("Shifted: -G.alpha -pi/2 = %f    index div: %d \n",ap,j);
  
  float rad[7],stp=0.02;
  for(i=0;i<7;i++) rad[i] = 1 - stp + i*stp/(6/2);
  
  
  for(i=0;i<LK.npt;i++){
    float angle = i*LK.div - alpha - PI/2;
    float c1 = q11 * a * cosf(angle) + q12 * b * sinf(angle) ;
    float r1 = q21 * a * cosf(angle) + q22 * b * sinf(angle) ;
    
    for(j=0;j<7;j++) {
      // Should be able to speed up this step by:
      // 1) shifting pre-calculated cos and sin at LK.
      // 2) pre-calculate a whole lot of multiplications
      int ci = rad[j]*c1 + 0;//1; //LK.cc = 256
//      int ri = rad[j]*r1 + RF/2; // be careful about RF/2 - as long as end result is integer should be ok
      int ri = rad[j]*r1 + (NX-1); // be careful about RF/2 - as long as end result is integer should be ok
      
      // Should double check this: RF+1 rows, RF/2+1 columns.
      // Prevent OB - this could cause crash
//      int r = MAX(MIN(RF+1,ri),0);
//      int c = MAX(MIN(RF/2+1,ci),0);
      int r = MAX(MIN(NX2,ri),0);
      int c = MAX(MIN(NX,ci),0);
      
//      if(j==0){
//        printf("%f %f %d %d %d %d\n",r1,c1,ri,ci,r,c);
//      }
      
      //    k = (i+j)%LK.npt;
      
      //    int c = q11 * a * LK.cosa[k] + q12 * b * LK.sina[k] + 1; //LK.cc = 256
      //    int r =  q21 * a * LK.cosa[k] + q22 * b * LK.sina[k] + 257;
      
      //    X[1][i] = q11 * a * LK.cosa[k] + q12 * b * LK.sina[k] + 1; //LK.cc = 256
      //    X[2][i] = q21 * a * LK.cosa[k] + q22 * b * LK.sina[k] + 257;
      
      // This could crash when c+r*(RF/2+1) has boundary problem
      //_Mt[i + j*LK.npt] = lf[c + r*(RF/2+1)]; // again careful about RF/2 - as long as end result is integer should be ok
      _Mt[i + j*LK.npt] = lf[c + r*NX]; // again careful about RF/2 - as long as end result is integer should be ok
      
      //      if(i<50){
      //        printf("%d _Mt %f c r [%d,%d]\n",i, _Mt[i],c,r);
      //      }
      //      printf("%f ")
      
    }
    
    //    if(displayOn>0 && i<20){
    //      printf("[%d] ",i);
    //      for(j=0;j<7;j++) printf("%8.3f ",_Mt[i + j*LK.npt] );
    //      printf("\n");
    //    }
  }
  
  //  % --------- 2a. Compute Average - could have been done at the same time at last step
  //  why j=1 and not j=0? j=0 is the "base" series.
  for(i=0;i<LK.npt;i++){
    for(j=1;j<7;j++) _Mt[i] += _Mt[i + j*LK.npt];
    _Mt[i] /= 7;
  }
  
  // Text plot
  if(0){
    float ylevel =0;
    for(j=5;j>=0;j--){
      ylevel=j*20;
      printf("%3.0f ",ylevel);
      for(i=0;i<LK.npt;i+=8){
        printf("%s",_Mt[i]>ylevel?"|":" ");
        
      }
      printf("\n");
    }
  }
//    printf("\n******** Before Smoothed _Mt\n");
//    for(i=0;i<LK.npt;i++){
//      // Plot in Matlab
//      printf("%8.5f ",_Mt[i]);
//    }
//    printf("\n _Mt end\n");
  
  // =====================
  // Seem to work up to this point _Mt seems correct
  //  LOGD("\n******** Before Smoothed _Mt\n");
//    printReset;
//    for(i=0;i<LK.npt;i++){
//      // Plot in Matlab
//      printAppend("%8.5f ",_Mt[i]);
////      if(i%100==99){
////        printOut;
////        printReset;
////      }
//    }
//    printOut;
  // =====================
  
  
  //  % --------- 2b. Smoothing 11-point Gaussian! Ensure no equal values
  // My version of convolution - room for improvement!
  int cw = 4;
  float gw[9] = {.01, .02, .07, .2, .4, .2, .07, .02, .01};
  for(i=0;i<LK.npt;i++){
    _tmp[i]=0;
    for(j=0;j<2*cw+1;j++){
      int k=i+j-cw;
      // Substitution for modulus: modz. Same as
      //      if(k<0) k+=LK.npt;
      //      else if(k>=LK.npt) k-=LK.npt;
      k = modz(k,LK.npt);
      
      _tmp[i]+=gw[j]*_Mt[k];
    }
  }
  for(i=0;i<LK.npt;i++) _Mt[i] = _tmp[i]; // copy _tmp back to _Mt
  
  // Matlab plot
//  printf("\n******** Smoothed _Mt for Matlab plot\n");
//  for(i=0;i<LK.npt;i++){
//    printf("%5.3f ",_Mt[i]);
//  }
//  printf("\n _Mt end\n");
  
  //  % --------- 3. Compute optimal shift.
  int   iMax = 0, nPk=0;
  float fMax = 0, ftmp = 0;
  for(i=0;i<LK.nDiv;i++){
    ftmp = 0;
    for(j=0;j<LK.npt;j+=LK.nDiv) ftmp += _Mt[j+i];
    if(ftmp>fMax){
      iMax = i;
      fMax = ftmp;
    }
    // Plot in Matlab
    // printf("shift %d f %f\n",i,ftmp);
  }
  _iOptShift = iMax;
  if(displayOn) LOGD("fMax %f  iMax %d\n",fMax,iMax);
  
  //  % --------- 4. Extract Single Peak2.
  
  
  // b2. Peak finding
  int w=LK.nDiv/4; // window 1-side width
  _nPk = 0;
  for(i=0;i<LK.npt;i++){
    // ----- a. Determine if _Mt[i] is maximum
    int isMax = 1;
    if(i>w && i<LK.npt-w){
      // Normal Fast Comparison
      for(j=1;j<w;j++){
        if(_Mt[i]<=_Mt[i+j] || _Mt[i]<=_Mt[i-j]){
          isMax = 0;
          break;
        }
      }
      
    }else{
      // More Computation involving "MOD" - clumsy coding can be improved
      for(j=i-w;j<i+w;j++){
        k = modz(j,LK.npt);
        //        if(j<0)       k=j + LK.npt;
        //        if(j>=LK.npt) k=j - LK.npt;
        if(i!=k && _Mt[i]<=_Mt[k]){
          isMax = 0;
          break;
        }
      }
    }
    
    // Final output: Peak location (iPk), Peak magnitude (magPk), num.Peak (nPeak)
    if(isMax>0){
      _iPk[_nPk]   = i;
      _magPk[_nPk] = _Mt[i];
      _nPk++;
    }
  } // For i loop
}


void getConfidence(float *conf, int iMax, float ptile){
  int i,j,k;
  int displayOn=0;
  // b. Get 75 percentile - will destroy sorted array. Copy into _tmp first.
  for(i=0;i<LK.npt;i++) _tmp[i] = _Mt[i];
  float p75 = kthsmallest(LK.npt*ptile, LK.npt, _tmp);
  
  // a. Reset Confidence array
  for(i=0;i<LK.nBit;i++){
    _mmk[i] = 0; // magnitude of marker
    conf[i] = 0;
  }
  for(j=0;j<_nPk;j++){
    // ----- b. if Maximum, Compute
    // (a) nearest marker, prob.marker,confidence,bitPos
    if(_magPk[j]>p75){
      // imk - nearest bit position
      // pmk - normalized distance from nearest marker
      // dv     = round((i - iMax) / nDiv); % divider
      // imk(k) = mod1(dv,63); - bitPos
      // pmk(k) = abs(i-iMax - dv*nDiv)/(nDiv*0.5);
      float dv   = roundf((float)(_iPk[j] - iMax) / LK.nDiv);
      int bitPos = modz(dv,LK.nBit);
      float pmk  = fabsf(_iPk[j]  - iMax - dv*LK.nDiv) / (LK.nDiv*.5);
      float cf   = 1 - fabsf(pmk);
      
      // New: Update conf[bitPos] only if the peak has higher magnitude than previous.
      if(_mmk[bitPos]<_magPk[j]){
        conf[bitPos] = cf;
        _mmk[bitPos] = _magPk[j];
      }
      
      if(displayOn){
        //        LOGD("bitPos must be 0-62: [%d - %d]",bitPos0, bitPos);
        LOGD("ReadPk %4d mt:%6.1f mmk:%6.1f nmk:%4d pmk:%5.4f cf:%5.4f conf:%5.4f bitpos:%3d (isMax:%d p75:%f)",_iPk[j],_magPk[j],_mmk[bitPos],99,pmk,cf,conf[bitPos],bitPos,99,p75);
      }
    }
    //    LOGD("- ReadPk %4d mt:%6.1f mmk:%6.1f nmk:%4d pmk:%5.4f cf:%5.4f conf:%5.4f bitpos:%3d (isMax:%d p75:%f)",j,_Mt[j],0.0f,0,0.0f,0.0f,0.0f,0,isMax,p75);
    
  }
  
  LOGD("***** readPeaks Conf");
  printReset;
  for(int i=0;i<LK.nBit;i++){
    printAppend("%d",conf[i]>0.0);
  }
  printOut;
  
  //  int nPk2 = 0;
  //  for(i=0;i<63;i++){
  //    nPk2 += (_mmk[i]>0); // magnitude of marker
  //  }
  //
  //  // ---------- 5. Tune the Confidence probability here.
  //  float gradConf = 1.5; // confidence gradient tuninng parameter. Was 1.5
  //  float minConf = 0.01, maxConf = 1-minConf;
  //  for (i=0;i<LK.nBit;i++){
  //    conf[i] = MAX( MIN( conf[i]*gradConf, maxConf) ,  minConf);
  //    if(displayOn){
  //      printf("Confidence for each 63 bits %i %f\n",i,conf[i]);
  //    }
  //  }
  //
  //  // High number of spikes>120 - signal of randomness.
  //  printf("bitCertainty nPk %d  \n",nPk);
  
}



void readPeaks(float *lf, float *ellipseParam, float *conf){
  int displayOn = 0;
  int i,j,k;
  
  float a,b,alpha;
  a     = ellipseParam[0];
  b     = ellipseParam[1];
  alpha = ellipseParam[2];
  
  //  % --------- 1. Extract Magnitude around the ring.
  //  % Rotation matrix
  //  Q = [cos(G.alpha), -sin(G.alpha); sin(G.alpha) cos(G.alpha)] + repmat(G.z, 1, length(angles))
  //
  //  % Ellipse points
  //  X = Q * [G.a * cos(angles); G.b * sin(angles)]
  float q11 = cosf(alpha), q12 = -sinf(alpha),
  q21 = sinf(alpha), q22 =  cosf(alpha);
  
  // We want angle to start at -G.alpha -pi/2
  //  float ap = -(alpha+PI/2);
  //  if(ap<0){
  //    ap += PI * (int)(-ap/PI+1);
  //  }
  //  j = roundf( ap /  (M_PI/(LK.npt+1)) );
  //  a = -a;  // somehow the angle shift caused -ve effect
  //  b = -b;
  //  printf("Shifted: -G.alpha -pi/2 = %f    index div: %d \n",ap,j);
  
  float rad[7],stp=0.02;
  for(i=0;i<7;i++) rad[i] = 1 - stp + i*stp/(6/2);
  
  
  for(i=0;i<LK.npt;i++){
    float angle = i*LK.div - alpha - PI/2;
    float c1 = q11 * a * cosf(angle) + q12 * b * sinf(angle) ;
    float r1 = q21 * a * cosf(angle) + q22 * b * sinf(angle) ;
    
    for(j=0;j<7;j++) {
      // Should be able to speed up this step by:
      // 1) shifting pre-calculated cos and sin at LK.
      // 2) pre-calculate a whole lot of multiplications
      int ci = rad[j]*c1 + 0;//1; //LK.cc = 256
      int ri = rad[j]*r1 + RF/2; // be careful about RF/2 - as long as end result is integer should be ok
      
      // Should double check this: RF+1 rows, RF/2+1 columns.
      // Prevent OB - this could cause crash
      int r = MAX(MIN(RF+1,ri),0);
      int c = MAX(MIN(RF/2+1,ci),0);
      
      //    k = (i+j)%LK.npt;
      
      //    int c = q11 * a * LK.cosa[k] + q12 * b * LK.sina[k] + 1; //LK.cc = 256
      //    int r =  q21 * a * LK.cosa[k] + q22 * b * LK.sina[k] + 257;
      
      //    X[1][i] = q11 * a * LK.cosa[k] + q12 * b * LK.sina[k] + 1; //LK.cc = 256
      //    X[2][i] = q21 * a * LK.cosa[k] + q22 * b * LK.sina[k] + 257;
      
      // This could crash when c+r*(RF/2+1) has boundary problem
      _Mt[i + j*LK.npt] = lf[c + r*(RF/2+1)]; // again careful about RF/2 - as long as end result is integer should be ok
      
      //      if(i<50){
      //        printf("%d _Mt %f c r [%d,%d]\n",i, _Mt[i],c,r);
      //      }
      //      printf("%f ")
      
    }
    
    //    if(displayOn>0 && i<20){
    //      printf("[%d] ",i);
    //      for(j=0;j<7;j++) printf("%8.3f ",_Mt[i + j*LK.npt] );
    //      printf("\n");
    //    }
  }
  
  //  % --------- 2a. Compute Average - could have been done at the same time at last step
  //  why j=1 and not j=0? j=0 is the "base" series.
  for(i=0;i<LK.npt;i++){
    for(j=1;j<7;j++) _Mt[i] += _Mt[i + j*LK.npt];
    _Mt[i] /= 7;
  }
  
  // Text plot
  if(0){
    float ylevel =0;
    for(j=5;j>=0;j--){
      ylevel=j*20;
      printf("%3.0f ",ylevel);
      for(i=0;i<LK.npt;i+=8){
        printf("%s",_Mt[i]>ylevel?"|":" ");
        
      }
      printf("\n");
    }
  }
  //  printf("\n******** Before Smoothed _Mt\n");
  //  for(i=0;i<LK.npt;i++){
  //    // Plot in Matlab
  //    printf("%8.5f ",_Mt[i]);
  //  }
  //  printf("\n _Mt end\n");
  
  // =====================
  // Seem to work up to this point _Mt seems correct
  //  LOGD("\n******** Before Smoothed _Mt\n");
  //  printReset;
  //  for(i=0;i<LK.npt;i++){
  //    // Plot in Matlab
  //    printAppend("%8.5f ",_Mt[i]);
  //    if(i%100==99){
  //      printOut;
  //      printReset;
  //    }
  //  }
  //  printOut;
  // =====================
  
  
  //  % --------- 2b. Smoothing 11-point Gaussian! Ensure no equal values
  // My version of convolution - room for improvement!
  int cw = 4;
  float gw[9] = {.01, .02, .07, .2, .4, .2, .07, .02, .01};
  for(i=0;i<LK.npt;i++){
    _tmp[i]=0;
    for(j=0;j<2*cw+1;j++){
      int k=i+j-cw;
      // Substitution for modulus: modz. Same as
      //      if(k<0) k+=LK.npt;
      //      else if(k>=LK.npt) k-=LK.npt;
      k = modz(k,LK.npt);
      
      _tmp[i]+=gw[j]*_Mt[k];
    }
  }
  for(i=0;i<LK.npt;i++) _Mt[i] = _tmp[i]; // copy _tmp back to _Mt
  
  // Matlab plot
  //  printf("\n******** Smoothed _Mt\n");
  //  for(i=0;i<LK.npt;i++){
  //    printf("%5.3f ",_Mt[i]);
  //  }
  //  printf("\n _Mt end\n");
  
  //  % --------- 3. Compute optimal shift.
  int   iMax = 0, nPk=0;
  float fMax = 0, ftmp = 0;
  for(i=0;i<LK.nDiv;i++){
    ftmp = 0;
    for(j=0;j<LK.npt;j+=LK.nDiv) ftmp += _Mt[j+i];
    if(ftmp>fMax){
      iMax = i;
      fMax = ftmp;
    }
    // Plot in Matlab
    // printf("shift %d f %f\n",i,ftmp);
  }
  if(displayOn) LOGD("fMax %f  iMax %d\n",fMax,iMax);
  
  //  % --------- 4. Extract Single Peak2.
  
  
  // b. Get 75 percentile - will destroy sorted array. Copy into _tmp first.
  for(i=0;i<LK.npt;i++) _tmp[i] = _Mt[i];
  float p75 = kthsmallest(LK.npt*.75, LK.npt, _tmp);
  
  // b2. Peak finding
  int w=LK.nDiv/4; // window 1-side width
  _nPk = 0;
  for(i=0;i<LK.npt;i++){
    // ----- a. Determine if _Mt[i] is maximum
    int isMax = 1;
    if(i>w && i<LK.npt-w){
      // Normal Fast Comparison
      for(j=1;j<w;j++){
        if(_Mt[i]<=_Mt[i+j] || _Mt[i]<=_Mt[i-j]){
          isMax = 0;
          break;
        }
      }
      
    }else{
      // More Computation involving "MOD" - clumsy coding can be improved
      for(j=i-w;j<i+w;j++){
        k = modz(j,LK.npt);
        //        if(j<0)       k=j + LK.npt;
        //        if(j>=LK.npt) k=j - LK.npt;
        if(i!=k && _Mt[i]<=_Mt[k]){
          isMax = 0;
          break;
        }
      }
    }
    
    // Record Peak location and magnitude
    if(isMax>0){
      _iPk[_nPk]   = i;
      _magPk[_nPk] = _Mt[i];
      _nPk++;
    }
  }
  
  
  // a. Reset Confidence array
  for(i=0;i<LK.nBit;i++){
    _mmk[i] = 0; // magnitude of marker
    conf[i] = 0;
  }
  for(j=0;j<_nPk;j++){
    // ----- b. if Maximum, Compute
    // (a) nearest marker, prob.marker,confidence,bitPos
    if(_magPk[j]>p75){
      // imk - nearest bit position
      // pmk - normalized distance from nearest marker
      // dv     = round((i - iMax) / nDiv); % divider
      // imk(k) = mod1(dv,63); - bitPos
      // pmk(k) = abs(i-iMax - dv*nDiv)/(nDiv*0.5);
      float dv   = roundf((float)(_iPk[j] - iMax) / LK.nDiv);
      int bitPos = modz(dv,LK.nBit);
      float pmk  = fabsf(_iPk[j]  - iMax - dv*LK.nDiv) / (LK.nDiv*.5);
      float cf   = 1 - fabsf(pmk);
      
      // New: Update conf[bitPos] only if the peak has higher magnitude than previous.
      // Not sure how useful this is.
      if(_mmk[bitPos]<_magPk[j]){
        conf[bitPos] = cf;
        _mmk[bitPos] = _magPk[j];
      }
//      if(conf[bitPos]<cf){
//        conf[bitPos] = cf;
//      }
      
      if(displayOn){
        //        LOGD("bitPos must be 0-62: [%d - %d]",bitPos0, bitPos);
        LOGD("ReadPk %4d mt:%6.1f mmk:%6.1f nmk:%4d pmk:%5.4f cf:%5.4f conf:%5.4f bitpos:%3d (isMax:%d p75:%f)",_iPk[j],_magPk[j],_mmk[bitPos],99,pmk,cf,conf[bitPos],bitPos,99,p75);
      }
    }
    //    LOGD("- ReadPk %4d mt:%6.1f mmk:%6.1f nmk:%4d pmk:%5.4f cf:%5.4f conf:%5.4f bitpos:%3d (isMax:%d p75:%f)",j,_Mt[j],0.0f,0,0.0f,0.0f,0.0f,0,isMax,p75);
    
  }
  
  LOGD("***** readPeaks Conf");
  printReset;
  for(int i=0;i<LK.nBit;i++){
    printAppend("%d",conf[i]>0.0);
  }
  printOut;
  
  //  int nPk2 = 0;
  //  for(i=0;i<63;i++){
  //    nPk2 += (_mmk[i]>0); // magnitude of marker
  //  }
  //
  //  // ---------- 5. Tune the Confidence probability here.
  //  float gradConf = 1.5; // confidence gradient tuninng parameter. Was 1.5
  //  float minConf = 0.01, maxConf = 1-minConf;
  //  for (i=0;i<LK.nBit;i++){
  //    conf[i] = MAX( MIN( conf[i]*gradConf, maxConf) ,  minConf);
  //    if(displayOn){
  //      printf("Confidence for each 63 bits %i %f\n",i,conf[i]);
  //    }
  //  }
  //
  //  // High number of spikes>120 - signal of randomness.
  //  printf("bitCertainty nPk %d  \n",nPk);
  
}




// #####################################################################
// Random Decode
// #####################################################################
unsigned char _seq[63],_seqCor[63];
unsigned long long int _arr[100];
int randomDecode(float *p,int maxTrial){
  
  LOGD("Show original sequence");
  printReset;
  for(int i=0;i<LK.nBit;i++){
    printAppend("%d",p[i]>0.5);
  }
  printOut;
  
  int show=0;
  
  int i,j,k;
  for(j=0;j<maxTrial;j++){
    
    // Create new sequence
    for(i=0;i<63;i++){
      _seq[i] = p[i] > (float)rand()/RAND_MAX;
    }
    
    // Doesn't seem to be any repeats...
    if(0){ // check for repeats
      _arr[i] = b2d63(_seq); // Save the binary version
      for(k=0;k<i;k++){
        if(_arr[i] ==_arr[k] ) printf("Repeat found!!!!!\n");
      }
    }
    
    //    printf("run:%3d  \n",j);
    //    for(int i=0;i<63;i++){
    //      printf("%d",_seq[i]);
    //    }
    //    printf("\n");
    //    printReset;
    //    for(int i=0;i<63;i++){
    //      printAppend("%d",_seq[i]);
    //    }
    //    printOut;
    
    
    
    // Correction
    int nCorrected;
    int flag = myDecoder(_seq,_seqCor,&nCorrected);
    if((flag>0) & show){
      LOGD("%d flag %d nCorrected %d",j,flag,nCorrected);
    }
    
    if(flag==0 || flag==1){
      
      //      for(int i=0;i<63;i++){
      //        printf("%d",_seqCor[i]);
      //      }
      //      printf("\n");
      
      // a. Convert binary to dec, and get max dec through rotation.
      unsigned long long int decMax = decMax63(b2d63(_seqCor));
      if(show){
        printf("Decode success %d corrected %d decMax %lu\n",flag,nCorrected,decMax);
        LOGD("Decode success %d corrected %d decMax %lu\n",flag,nCorrected,decMax);
      }
      LK.codeLatest = decMax;
      
      // b. Search on LK.code array. If found, return. Otherwise, loop again.
      int foundIndex = myBinarySearch(decMax);
      if(foundIndex>=0){
        if(show){
          printf("Code %llu found at index %d \n",decMax,foundIndex);
          LOGD("Code %llu found at index %d \n",decMax,foundIndex);
        }
        return foundIndex;
      }else{
        if(show){
          printf("Code %llu not found on array \n",decMax);
          LOGD("Code %llu not found on array \n",decMax);
        }
      }
      
    }else{
      //      printf("Decode error %d\n",flag);
      continue;
    }
    
    
  }
  return -1;
}

// #####################################################################
// Get Mean Radius
// #####################################################################

float _radPxMean[NE],_tmpx[20*NE];
void getMeanRadius(float *af0){
  int r,c,i,j,k,l,m;
  int displayOn = 0;
  
  if(displayOn) printf("***** getMeanRadius *****\n");
  
  
  // 1. Operate on half square only
  //    Compute pixel mean for each radius
  for(i=0;i<LK.nRadiusBin;i++){
    _radPxMean[i] = 0;
    for(j=0;j<LK.radiusBinCount[i];j++){
      float v = af0[ LK.radiusBinIdx[i*NE + j] ];
      v = (v>.001 && v<1000) ? v : 0; // Remove too big & too small
      _radPxMean[i]+= v ;
    }
    _radPxMean[i] /= LK.radiusBinCount[i];
    
    // Collect info for test
    //    if(db_showPlot){
    LK.x[i] = LK.radiusBinValue[i];
    LK.y[i] = _radPxMean[i] ;
    LK.nPlotPoint = LK.nRadiusBin;
    //    }
    if(displayOn){
      //      printf("%d %3f \n",LK.radiusBinValue[i],_radPxMean[i]);
      LOGD("%d %3f ",LK.radiusBinValue[i],_radPxMean[i]);
    }
  }
  
  if(db_showPlot){
    // For Plot heatmap
//    memcpy(LK.heatmap,af0,sizeof(float)*NF); // why NF should be just half circle?? need to change
    memcpy(LK.heatmap,af0,sizeof(float)*SX2); // why NF should be just half circle?? need to change
  }
  
  // 2. Peak detection and
  int w   = 5;
  int nPk = 0;
  //  float x[20];
  int iRadBest = 0;
  float pkBest = 0;
  LK.nRadBest = 0;
  //   pkBest   = 0;
  //   iRadBest = 0;
  for(i=w;i<LK.nRadiusBin-w;i++){
    
    // If radius limit is applied, radius must be within range.
    //    if(db_applyRadLimit){
    float radMin=40, radMax=NX; // changed from 128?
    if(LK.radiusBinValue[i]<radMin || LK.radiusBinValue[i]>radMax){
      continue;
    }
    //    }
    
    // 2.a Simple Peak Detection
    int isPeak = 1;
    for(j=i-w;j<i+w;j++){
      if(_radPxMean[i]<_radPxMean[j]){
        isPeak = 0;
        break;
      }
    }
    //     printf("[%d] is Peak %d\n",LK.radiusBinValue[i],isPeak);
    //    LOGD("[%d] is Peak %d\n",LK.radiusBinValue[i],isPeak);
    
    // 2.b If Peak, compute median, then median-difference
    if(isPeak==1){
      k=0;
      for(j=i-w;j<i+w;j++) _tmpx[k++] = _radPxMean[j];
      //      float median = quick_select_median(x,w*2+1); // Compute median
      float median = wirth_median(_tmpx,w*2+1); // Compute median
      //      float median = kthsmallest(w, w*2+1, _tmpx);
      float pk     = _radPxMean[i]-median;
      //       double median = wirth_median(x,w*2+1);
      //      printf("%3d Median %f diff %f \n",LK.radiusBinValue[i],median,pk);
      
      if(0){//(db_tryMultiPeak){
        
        LOGD("1 Peak detected %d",i);
        // Save to Lookup all peaks
        LK.iRadBest[LK.nRadBest] = i;
        LK.nRadBest++;
        
        // printf("LK.nRadBest(%d)=%d \n",LK.nRadBest,LK.radiusBinValue[i]);
      }else{
        // Save single best peak - highest pk only
        if(pk>pkBest){
          
          LOGD("2 Peak detected %d",i);
          pkBest   = pk;
          iRadBest = i;
          
          // LK.nRadBest always 1 only
          LK.iRadBest[0] = iRadBest; // error fixed!
          LK.nRadBest=1;
        }
      }
    }
  }// loop LK.nRadiusBin
  
  if(db_showPlot){
    sprintf(LK.plotComment,"Radius %d",LK.radiusBinValue[iRadBest]);
  }
  
  if(displayOn){
    //printf("idx %3d radius %d pkBest %f \n",iRadBest,LK.radiusBinValue[iRadBest],pkBest);
    LOGD("idx %3d radius %d pkBest %f LK.nRadBest %d \n",iRadBest,LK.radiusBinValue[iRadBest],pkBest,LK.nRadBest);
  }
  LOGD("idx %3d radius %d pkBest %f LK.nRadBest %d \n",iRadBest,LK.radiusBinValue[iRadBest],pkBest,LK.nRadBest);
  //  for(i=0;i<LK.nRadiusBin;i++){
  //    printf("(%4.1f,%4.1f) ",LK.x[i],LK.y[i]);
  //  }
  //  printf("\n");
  
}

void getRadiusStats(float *af0,int index){
  int iRadBest = LK.iRadBest[index];
  float pkBest = LK.radiusBinValue[iRadBest];
  
  LOGD("index %d iRadBest %d pkBest %f",index,iRadBest,pkBest);
  
  // 3. Eligible peak found.
  //    a) Find 95 percentile of the ring.
  //    b) Put into sparse format, only use values about 95-tile
  int displayOn = 1;
  int w,i,j,k,m;
  gp.n = 0;
  gp.p95 = 0;
  gp.p50 = 0;
  if(pkBest>0.5){
    w = 5;
    int iRadMin = MAX(0,iRadBest-w);
    int iRadMax = MIN(LK.nRadiusBin,iRadBest+w);
    k       = 0;
    // Go from iRadMin to iRadMax. At each Rad, add pixel to g.
    for(i= iRadMin; i<iRadMax; i++){
      // Go through each radius bin
      for(j=0;j<LK.radiusBinCount[i];j++){
        //        printf("i %d j %d, m %d k %d\n",i,j,m,k);
        m         = LK.radiusBinIdx[i*NE + j];
        gp.v[k]   = af0[m];
        gp.r[k]   = LK.r[m];
        gp.c[k]   = LK.c[m];
        gp.rad[k] = LK.radius[m];
        gp.use[k] = 1;
        _tmpx[k]  = af0[m];
        k++;
      }
    }// i loop
    gp.n = k;
    
    // Compute 95-percentile - note it will shuffle the array inplace.
    // So use a temporary array instead of actual gp.v.
    gp.p95 = kthsmallest(0.95*gp.n, gp.n, _tmpx);
    
    // P50 not used here. But later for reject unlikely candidate later.
    gp.p50 = kthsmallest(0.50*gp.n, gp.n, _tmpx);
    
    if(displayOn) printf("95-tile = %f, k=%d\n",gp.p95,gp.n);
    
    // Only use above 90-tile
    for(i=0;i<gp.n;i++) gp.use[i] = gp.v[i]>gp.p95;
    
  }
  
}



// #####################################################################
// Full Engine code
// #####################################################################
float _absF[NF],_absFX[SX2],_dataRing1[NE*50],_dataRing2[NE*50],_iRadBest[100];
int myEngine(float *Image){
  int i,j,k,r,c;
  
  // Dynamic memory allocation
  float *ellipseParam = (float *)malloc(3 *sizeof(float));
  float *conf         = (float *)malloc(63*sizeof(float));
  
  // ----- 1. Get filtered FFT
  //  for(i=0;i<20;i++) printf("%4.0f ",Image[i]);
  getFilteredFFT(Image, _absF);
  
  // Copy into Extended half circle
  _absF[LK.icentre] = 0; // set centre point of small circle to be 0
  for(i=0;i<SX2;i++) _absFX[i] = _absF[LK.map[i]];
  
//  printf("\n\nShuffled Abs F\n");
//   int C1 = CF/2+1;
//  k = 0;
//  for (r = 0; r < RF+1; ++r){
//    for (c = 0; c < C1; ++c){
//      printf("%7.3f ", _absF[k++]);
//    }
//    printf("\n");
//  }
  
//  if(1){
//    printf("\n\nExtended F\n");
//    k = 0;
//    for (r = 0; r < NX2; ++r){
//      for (c = 0; c < NX; ++c){
//        printf("%7.3f ", _absFX[k++]);
//      }
//      printf("\n");
//    }
//  }

  
  // ----- 2. Get mean Radius
  // Get mean radius - output is stored in structure "gp"
  getMeanRadius(_absFX);
  
  // ================================================
  // Loop over all radius peaks.
  int foundIndex = -1;
  
  for(int ir=0;ir<LK.nRadBest;ir++){
    
    // Get Stats about this radius position
    getRadiusStats(_absFX,ir);
    
    // Skip if insufficient stats on this peak
    if(gp.n==0) continue;
    
    // ----- 3. Fit into Ellipse
    k = 0;
    for(i=0;i<gp.n;i++){
      if(gp.use[i]==1){
        _dataRing1[k] = gp.c[i];
        _dataRing2[k] = gp.r[i];
        //       printf("%d  %f  %f \n",k,_dataRing1[k] ,_dataRing2[k]);
        k++;
      }
    }
    // Column (0 to 256), row (from -256 to +256]
    fitEllipse(_dataRing1,_dataRing2, k, ellipseParam);
    
    // ----- 4. Read Peaks
    // Output is Peak location (_iPk), Peak magnitude (_magPk), num.Peak (_nPk)
    //    readPeaks(_absF, ellipseParam, conf);
    readPeaksOnly(_absFX, ellipseParam);
    
    // Try over selected percentiles, and produce random sequence on each percentile.
    float ptile[4] = {.0,.25,.50,0.75};
    for(j=0;j<4;j++){
//      for(int k1=0;k1<1;k1++){ // using 32 causes crash
    
        //      getConfidence(conf, _iOptShift, ptile[j]);
        getConfidence(conf, _iOptShift, ptile[j]);
        //        getConfidence(conf, k, .75);
        
        // ----- 4a. Check Bit Certainty - normalized closeness to bit 1 only.
        /*
         float bitCertainty = 0;
         float ones = 0;
         for(i=0;i<63;i++){
         ones         += conf[i]>0.0;
         bitCertainty += conf[i]>0.0 ? conf[i] : 0;
         }
         bitCertainty /= ones;
         //    printf("ring p95-p50 diff %f bitCertainty %f ones %.2f\n",gp.p95-gp.p50,bitCertainty,ones);
         if(bitCertainty<0.6) return -1;
         */
        
        // ----- 4b. Tune Confidence Probability.
        float gradConf = 1.5; // confidence gradient tuninng parameter. Was 1.5
        float minConf = 0.01, maxConf = 1-minConf;
        for (i=0;i<LK.nBit;i++){
          conf[i] = MAX( MIN( conf[i]*gradConf, maxConf) ,  minConf);
          if(0){
            printf("Prob Confidence for each 63 bits %i %f\n",i,conf[i]);
          }
        }
        
        // ----- 5. Random decode
        foundIndex   = randomDecode(conf,12);
        
        
        // Only Point to exit: when foundIndex>=0
        if(foundIndex>=0){
          printf(" ##### decoded %llu index %d \n",LK.code[foundIndex],foundIndex);
          break;
        }else{
          printf(" ##### Decode Index %d \n",foundIndex);
        }
//      }
//      if(foundIndex>=0) break; // this causes crash
    }
    if(foundIndex>=0) break; // 2nd break just in case;
    
  }
  // Deallocate memory
  free(ellipseParam);
  free(conf);
  
  // ================================================
  
  return foundIndex;
}
int myEngine_old(float *Image){
  int i,j,k;
  
  // Dynamic memory allocation
  float *ellipseParam = (float *)malloc(3 *sizeof(float));
  float *conf         = (float *)malloc(63*sizeof(float));
  
  // ----- 1. Get filtered FFT
  //  for(i=0;i<20;i++) printf("%4.0f ",Image[i]);
  getFilteredFFT(Image, _absF);
  
  // ----- 2. Get mean Radius
  // Get mean radius - output is stored in structure "gp"
  getMeanRadius(_absF);
  
  // ================================================
  // Loop over all radius peaks.
  int foundIndex = -1;
  
  for(int ir=0;ir<LK.nRadBest;ir++){
    
    // Get Stats about this radius position
    getRadiusStats(_absF,ir);
    
    // Skip if insufficient stats on this peak
    if(gp.n==0) continue;
    
    // ----- 3. Fit into Ellipse
    k = 0;
    for(i=0;i<gp.n;i++){
      if(gp.use[i]==1){
        _dataRing1[k] = gp.c[i];
        _dataRing2[k] = gp.r[i];
        //       printf("%d  %f  %f \n",k,_dataRing1[k] ,_dataRing2[k]);
        k++;
      }
    }
    // Column (0 to 256), row (from -256 to +256]
    fitEllipse(_dataRing1,_dataRing2, k, ellipseParam);
    
    // ----- 4. Read Peaks
    readPeaks(_absF, ellipseParam, conf);
    
    // ----- 4a. Check Bit Certainty - normalized closeness to bit 1 only.
    float bitCertainty = 0;
    float ones = 0;
    for(i=0;i<63;i++){
      ones         += conf[i]>0.0;
      bitCertainty += conf[i]>0.0 ? conf[i] : 0;
    }
    bitCertainty /= ones;
    //    printf("ring p95-p50 diff %f bitCertainty %f ones %.2f\n",gp.p95-gp.p50,bitCertainty,ones);
    if(bitCertainty<0.6) return -1;
    
    // ----- 4b. Tune Confidence Probability.
    float gradConf = 1.5; // confidence gradient tuninng parameter. Was 1.5
    float minConf = 0.01, maxConf = 1-minConf;
    for (i=0;i<LK.nBit;i++){
      conf[i] = MAX( MIN( conf[i]*gradConf, maxConf) ,  minConf);
      if(0){
        printf("Prob Confidence for each 63 bits %i %f\n",i,conf[i]);
      }
    }
    
    // ----- 5. Random decode
    foundIndex   = randomDecode(conf,50);
    
    
    // Only Point to exit: when foundIndex>=0
    if(foundIndex>=0){
      printf(" ##### decoded %llu index %d \n",LK.code[foundIndex],foundIndex);
      break;
    }else{
      printf(" ##### Decode Index %d \n",foundIndex);
    }
  }
  // Deallocate memory
  free(ellipseParam);
  free(conf);
  
  // ================================================
  
  return foundIndex;
}
//float ellipseParam[3];
//float conf[63];
int myEnginex(float *aF){
  int i,j,k;
  
  //  printReset;
  //  for(int i=0;i<256*256;i+=700){
  //    printAppend("%4.2f,",aF[i]);
  //  }
  //  printOut;
  
  // Dynamic memory allocation
  float *ellipseParam = (float *)malloc(3 *sizeof(float));
  float *conf         = (float *)malloc(63*sizeof(float));
  
  // ----- 1. Get filtered FFT
  //  for(i=0;i<20;i++) printf("%4.0f ",Image[i]);
  //  getFilteredFFT(Image, _absF);
  //  k=0;
  //  for(j=0;j<256;j++){
  //    sprintf(_str,"");
  //    for(i=0;i<129;i++) sprintf(_str+strlen(_str),"%4.0f ",aF[k++]);
  //    LOGD("%s",_str);
  //  }
  
  
  // ----- 2. Get mean Radius
  // Get mean radius - output is stored in structure "gp"
  getMeanRadius(aF);
  
  // ================================================
  // Loop over all radius peaks.
  int foundIndex = -1;
  
  for(int ir=0;ir<LK.nRadBest;ir++){
    
    LOGD("getRadiusStats");
    // Get Stats about this radius position
    getRadiusStats(aF,ir);
    
    // Skip if insufficient stats on this peak
    if(gp.n==0) continue;
    
    // ----- 3. Fit into Ellipse
    LOGD("fitEllipse");
    k = 0;
    for(i=0;i<gp.n;i++){
      if(gp.use[i]==1){
        _dataRing1[k] = gp.c[i];
        _dataRing2[k] = gp.r[i];
        //       printf("%d  %f  %f \n",k,_dataRing1[k] ,_dataRing2[k]);
        //        sprintf(_str,"%s%d,%f,%f;",_str,k,_dataRing1[k] ,_dataRing2[k]);
        //        LOGD("%d  %f  %f",k,_dataRing1[k] ,_dataRing2[k]);
        
        k++;
      }
    }
    //    LOGD("%s",_str);
    // Column (0 to 256), row (from -256 to +256]
    fitEllipse(_dataRing1,_dataRing2, k, ellipseParam);
    
    // ----- 4. Read Peaks
    //    LOGD("readPeaks");
    readPeaks(aF, ellipseParam, conf);
    
    // ----- 4a. Check Bit Certainty - normalized closeness to bit 1 only.
    float bitCertainty = 0;
    float ones = 0;
    for(i=0;i<LK.nBit;i++){
      ones         += conf[i]>0.0;
      bitCertainty += conf[i]>0.0 ? conf[i] : 0;
    }
    bitCertainty /= ones;
    LOGD("bitCertainty");
    LOGD("ring p95-p50 diff %f bitCertainty %f ones %.2f",gp.p95-gp.p50,bitCertainty,ones);
    //    printf("ring p95-p50 diff %f bitCertainty %f ones %.2f\n",gp.p95-gp.p50,bitCertainty,ones);
    if(bitCertainty<0.6){
      //      free(ellipseParam);
      //      free(conf);
      //      return -1;//0.6
      LOGD("Break due to low BitCertainty");
      break;
    }
    
    // ----- 4b. Tune Confidence Probability.
    LOGD("Tune confidence Probability");
    float gradConf = 1.5; // confidence gradient tuninng parameter. Was 1.5
    float minConf = 0.01, maxConf = 1-minConf;
    for (i=0;i<LK.nBit;i++){
      conf[i] = MAX( MIN( conf[i]*gradConf, maxConf) ,  minConf);
      if(1){
        //        printf("Prob Confidence for each 63 bits %i %f\n",i,conf[i]);
        //        LOGD("Prob Confidence for each 63 bits %i %f",i,conf[i]);
      }
    }
    
    // ----- 5. Random decode
    LOGD("Random decode");
    foundIndex   = randomDecode(conf,500);
    
    LOGD("foundIndex %d",foundIndex);
    
    
    // Only Point to exit: when foundIndex>=0
    if(foundIndex>=0){
      printf(" ##### decoded %llu index %d \n",LK.code[foundIndex],foundIndex);
      LOGD(" ##### decoded %llu index %d \n",LK.code[foundIndex],foundIndex);
      break;
    }else{
      printf(" ##### Decode Index %d \n",foundIndex);
    }
    LOGD("Iteration %d (%d) end",ir,LK.nRadBest);
  }
  // Deallocate memory
  free(ellipseParam);
  free(conf);
  
  // ================================================
  
  return foundIndex;
}




