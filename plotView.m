//
//  plotView.m
//  SquareCam 
//
//  Created by Zeke Chan on 25/02/17.
//
//

#import "plotView.h"

@implementation plotView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)drawRect:(CGRect)rect
{
  // https://www.sitepoint.com/creating-a-graph-with-quartz-2d/
  float H = self.frame.size.height;
  float W = self.frame.size.width;
  // Drawing code - refresh [self.view setNeedsDisplay];
//  CGRect rectangle = CGRectMake(0, 100, 30, 10);
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetLineWidth(context, 0.6);
  CGContextSetRGBFillColor(context, 0.0, 0.0, 0.0, 0.5);
  CGContextSetRGBStrokeColor(context, 0.5, 0.5, 0.5, 0.1);
  CGContextSetStrokeColorWithColor(context, [[UIColor lightGrayColor] CGColor]);
  
  NSArray *ax = _d[@"x"];
  NSArray *ay = _d[@"y"];
  NSArray *xlim = _d[@"xlim"];
  NSArray *ylim = _d[@"ylim"];
  NSArray *grid = _d[@"grid"];
  float xrange = [xlim[1] floatValue] - [xlim[0] floatValue];
  float yrange = [ylim[1] floatValue] - [ylim[0] floatValue];
  
  float markersize = _d[@"markersize"] ? [_d[@"markersize"] floatValue] : 4;
  
  
  // Points
  int npoint = ax.count;
  for(int i=0;i<npoint;i++){
    float x = W*([ax[i] floatValue] - [xlim[0] floatValue]) / xrange;
    float y = H - H*([ay[i] floatValue] - [ylim[0] floatValue]) / yrange;
    
    CGRect rectangle = CGRectMake(x,y, markersize, markersize);
    CGContextFillRect(context, rectangle);
  }
  
  // Grid
  for(int v = [ylim[0] integerValue]; v < [ylim[1] integerValue]; v++){
    if( v%[grid[1] integerValue] == 0){
      float y = H - H*(v - [ylim[0] floatValue]) / yrange;
      CGContextMoveToPoint(context, 0, y);
      CGContextAddLineToPoint(context, W, y);
      
      // axis number label
      NSString *txt = [NSString stringWithFormat:@"%d", v];
      [txt drawAtPoint:CGPointMake(0, y) withAttributes:nil];
    }
  }
  for(int v = [xlim[0] integerValue]; v < [xlim[1] integerValue]; v++){
    if( v%[grid[0] integerValue] == 0){
      float x = W*(v - [xlim[0] floatValue]) / xrange;
      CGContextMoveToPoint(context, x, 0);
      CGContextAddLineToPoint(context, x, H);
      
      // axis number label
      NSString *txt = [NSString stringWithFormat:@"%d", v];
      [txt drawAtPoint:CGPointMake(x, 0) withAttributes:nil];
    }
  }
  
  //
  NSString *txt = _d[@"title"]? _d[@"title"] : @"";
  [txt drawAtPoint:CGPointMake(0, 10) withAttributes:nil];
  CGContextStrokePath(context);
  
  
//  CGContextFillRect(context, rectangle);
}

@end
