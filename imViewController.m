//
//  imViewController.m
//  SquareCam 
//
//  Created by Zeke Chan on 12/12/16.
//
//

#import "imViewController.h"

@interface imViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *ivPicture;
@property (strong, nonatomic) IBOutlet UILabel *lbTitle;

@end

@implementation imViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
  // Extract base64 thumbnail
  UIImage *thumbnail = nil;
//  Code *code = [ICG sharedICG].arrayScan[0];
  if(_code.Thumbnail){
    NSData *decoded = [[NSData alloc] initWithBase64EncodedData:_code.Thumbnail options:NSDataBase64DecodingIgnoreUnknownCharacters];
    if(decoded.length>0){
      thumbnail = [UIImage imageWithData:decoded];
    }
  }
  
  // Display image on main thread
  dispatch_async(dispatch_get_main_queue(), ^(void) {
    _lbTitle.text = [NSString stringWithFormat:@"%@\n%@",_code.Desc,_code.Url];
    [_ivPicture setImage: thumbnail];
  });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
