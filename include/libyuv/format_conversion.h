/*
 *  Copyright 2015 The LibYuv Project Authors. All rights reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree. An additional intellectual property rights grant can be found
 *  in the file PATENTS. All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */

/* format_conversion.h has been deprecated. */
/* Functionality for bayer formats (BGGR) removed. */

#ifndef INCLUDE_LIBYUV_FORMAT_CONVERSION_H_  // NOLINT
#define INCLUDE_LIBYUV_FORMAT_CONVERSION_H_
#endif  // INCLUDE_LIBYUV_FORMAT_CONVERSION_H_

