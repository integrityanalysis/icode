//
//  imViewController.h
//  SquareCam 
//
//  Created by Zeke Chan on 12/12/16.
//
//

#import <UIKit/UIKit.h>
#import "ICG.h"

@interface imViewController : UIViewController

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) Code *code;
@end
