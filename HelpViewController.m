//
//  HelpViewController.m
//  SquareCam
//
//  Created by Zeke Chan on 5/05/17.
//
//

#import "HelpViewController.h"
#import "Code.h"
#import "ICG.h"
#import "Code.h"
#import "ICWebViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  
  // Help Page - re-use
  Code *code    = [[Code alloc] init];
  code.Code     = 0;
  code.ClientId = @"";
  code.Desc     = @"";
  code.Url      = @"https://www.integrityanalysis.com";
  
  ICWebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"webViewController"];
  wvc.code      = code;
  wvc.noAnimate = YES; // indicates this is a help page that doesn't need animations.
  
  // Crop Image into Square, then save to JPEG.
  // Name file as picXXX.jpg where XXX is code.ScannedAt
  //  UIImage *imSq = [latestCaptureImage thumbnailImage:256
  //                                   transparentBorder:0
  //                                        cornerRadius:10
  //                                interpolationQuality:kCGInterpolationHigh];
  //
  //  NSString *f = [docDir stringByAppendingPathComponent:ptf(@"pic%@.jpg",code.ScannedAt)];
  //  [UIImageJPEGRepresentation(imSq,0.9f) writeToFile:f atomically:YES];
  
  //      wvc.thumbnail = imSq;//latestCaptureImage;
//  wvc.modalPresentationStyle  = UIModalPresentationFormSheet;
  [self.navigationController pushViewController:wvc animated:YES];
  
//    [self presentViewController:wvc animated:YES completion:nil];
//  [self.tabBarController presentViewController:wvc animated:YES completion:nil];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
